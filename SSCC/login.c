
#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;
local Inet *net;
// local Iship *ship;

int resx=0;
int resy=0;

local void login(void)
{

	//hyperspace=208.122.59.226:5005
	//trench wars=66.36.241.110:5400
	//dist=69.164.220.203:12000
	//swz=66.235.184.102:6000
	// DO_CBS(CB_CONNECTIONCONNECT,ConnectionConnectFunc,(DEST_ZONE,"127.0.0.1",12345));
	DO_CBS(CB_CONNECTIONCONNECT,ConnectionConnectFunc,(DEST_ZONE,"69.164.220.203",12000));
	// DO_CBS(CB_CONNECTIONCONNECT,ConnectionConnectFunc,(DEST_ZONE,"66.36.241.110",5400));
	// DO_CBS(CB_CONNECTIONCONNECT,ConnectionConnectFunc,("66.235.184.102",6000));
	
}

local void logout(void)
{
	CoreDisconnect cd;
	cd.type=CORE_PACKET;
	cd.subtype=CORE_DISCONNECT;
	
	net->SendPacket(DEST_ZONE,&cd,sizeof(CoreDisconnect));
	
	DO_CBS(CB_LOCATIONCHANGE,LocationChangeFunc,(LR_ZONEEXIT));
}

local void resolution(int x, int y)
{
	resx=x;
	resy=y;
}

local void ConnectionReady(int dest)
{
	if(dest != DEST_ZONE) return;
	
	C2SLoginRequest lr;
	int size=sizeof(C2SLoginRequest);
	memset(&lr,0,size);
			
	lr.type=C2S_LOGINREQUEST;
	sprintf(lr.name,"SSCclient");
	// sprintf(lr.name,"SSCclient%d",(current_ticks() % 1000));
	sprintf(lr.password,"ssc");
	lr.macid=1193577244; // my regards to arry
	lr.version=134;
	
	net->SendPacket(dest,&lr,size);
}

local void zonedisconnect(void)
{
	// DO_CBS(CB_LOCATIONCHANGE,LocationChangeFunc,(LR_ZONEEXIT));
}

local const char *loginmsg(int code)
{
#define AUTH_OK              0x00   /* success */
#define AUTH_NEWNAME         0x01   /* fail */
#define AUTH_BADPASSWORD     0x02   /* fail */
#define AUTH_ARENAFULL       0x03   /* fail */
#define AUTH_LOCKEDOUT       0x04   /* fail */
#define AUTH_NOPERMISSION    0x05   /* fail */
#define AUTH_SPECONLY        0x06   /* success */
#define AUTH_TOOMANYPOINTS   0x07   /* fail */
#define AUTH_TOOSLOW         0x08   /* fail */
#define AUTH_NOPERMISSION2   0x09   /* fail */
#define AUTH_NONEWCONN       0x0A   /* fail */
#define AUTH_BADNAME         0x0B   /* fail */
#define AUTH_OFFENSIVENAME   0x0C   /* fail */
#define AUTH_NOSCORES        0x0D   /* success */
#define AUTH_SERVERBUSY      0x0E   /* fail */
#define AUTH_TOOLOWUSAGE     0x0F   /* fail */
#define AUTH_ASKDEMOGRAPHICS 0x10   /* success */
#define AUTH_TOOMANYDEMO     0x11   /* fail */
#define AUTH_NODEMO          0x12   /* fail */
#define AUTH_CUSTOMTEXT      0x13   /* fail */      /* contonly */
	switch(code)
	{
		case AUTH_OK: return "ok";
		case AUTH_NEWNAME: return "new user";
		case AUTH_BADPASSWORD: return "incorrect password";
		case AUTH_ARENAFULL: return "arena full";
		case AUTH_LOCKEDOUT: return "you have been locked out";
		case AUTH_NOPERMISSION: return "no permission";
		case AUTH_SPECONLY: return "you can spec only";
		case AUTH_TOOMANYPOINTS: return "you have too many points";
		case AUTH_TOOSLOW: return "too slow (?)";
		case AUTH_NOPERMISSION2: return "no permission (2)";
		case AUTH_NONEWCONN: return "the server is not accepting new connections";
		case AUTH_BADNAME: return "bad player name";
		case AUTH_OFFENSIVENAME: return "offensive player name";
		case AUTH_NOSCORES: return "the server is not recordng scores";
		case AUTH_SERVERBUSY: return "the server is busy";
		case AUTH_TOOLOWUSAGE: return "too low usage";
		case AUTH_ASKDEMOGRAPHICS: return "need demographics";
		case AUTH_TOOMANYDEMO: return "too many demo players";
		case AUTH_NODEMO: return "no demo players allowed";
		default: return "???";
	}
}

local int packethandler(int dest, Byte *pkt, int len)
{
	if(dest != DEST_ZONE) return;
	
	switch(pkt[0])
	{
		case S2C_LOGINRESPONSE:
		{
			S2CLoginResponse *lr=(S2CLoginResponse*)pkt;
		
			printf("login response: type=%d response=%d version=%d vip=%d rr=%d msg=%s\n",
				lr->type,lr->response,lr->version,lr->isvip,lr->registrationrequest,loginmsg(lr->response));
			

			C2SArenaChange ac;
			int size=sizeof(C2SArenaChange);
			memset(&ac,0,size);
			ac.type=C2S_ARENACHANGE;
			ac.xres=resx;
			ac.yres=resy;

			net->SendPacket(DEST_ZONE,&ac,size);
			
			DO_CBS(CB_LOCATIONCHANGE,LocationChangeFunc,(LR_ZONEENTER));
	
			//i want to add keepalive packet set on interval while in zone, disregarding whether in arena
			// ml->SetTimer(keepalive,25,25,(void*)NULL,NULL);
			
			return TRUE;
		}
		break;
		case S2C_KEEPALIVE:
		{
			return TRUE;
		}
		break;
	}

	return FALSE;
}


EXPORT int MM_login(Imodman *mm2, mod_action action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		net=mm->GetInterface(I_NET);
		// ship=mm->GetInterface(I_SHIP);
		if(!lm || !net) return MM_FAIL;
		
		net->AddPacket(S2C_LOGINRESPONSE,packethandler);
		// net->AddPacket(S2C_CHAT,packethandler);
		net->AddPacket(S2C_KEEPALIVE,packethandler);
		
		mm->RegCallback(CB_CREATEGRAPHICS,login);
		mm->RegCallback(CB_DESTROYGRAPHICS,logout);
		mm->RegCallback(CB_RESOLUTION,resolution);
		mm->RegCallback(CB_CONNECTIONREADY,ConnectionReady);
		mm->RegCallback(CB_ZONEDISCONNECT,zonedisconnect);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		mm->UnregCallback(CB_CREATEGRAPHICS,login);
		mm->UnregCallback(CB_DESTROYGRAPHICS,logout);
		mm->UnregCallback(CB_RESOLUTION,resolution);
		mm->UnregCallback(CB_CONNECTIONREADY,ConnectionReady);
		mm->UnregCallback(CB_ZONEDISCONNECT,zonedisconnect);
		
		net->RemovePacket(S2C_LOGINRESPONSE,packethandler);
		// net->RemovePacket(S2C_CHAT,packethandler);
		net->RemovePacket(S2C_KEEPALIVE,packethandler);
	
		// mm->ReleaseInterface(ship);
		mm->ReleaseInterface(net);
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}
