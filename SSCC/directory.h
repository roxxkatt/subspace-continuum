
#ifndef __DIRECTORY_H
#define __DIRECTORY_H


///----------------------------------------------------------------------------------------------------


//C2D PACKET TYPES
#define C2D_LISTREQUEST 0x01

//D2C PACKET TYPES
#define D2C_LISTRESPONSE 0x01


///----------------------------------------------------------------------------------------------------


#pragma pack(push,1)

typedef struct C2DListRequest
{
	u8 type; //0x01
	u32 minimumplayers;
} C2DListRequest;

typedef struct ListEntry
{
	u32 ip;
	u16 port;
	u16 players;
	u16 billing;
	u32 version;
	u8 name[64];
	char description[]; //read until null
} ListEntry;

typedef struct D2CListResponse
{
	u8 type; //0x01, not repeated
	ListEntry le; //repeated
} D2CListResponse;

#pragma pack(pop)


///----------------------------------------------------------------------------------------------------


//call to populate directory list data
#define CB_DIRECTORYGETLIST "directorygetlist"
typedef void (*DirectoryGetListFunc)(void);

//called after directory list data is populated
#define CB_DIRECTORYLISTREADY "directorylistready"
typedef void (*DirectoryListReadyFunc)(void);


///----------------------------------------------------------------------------------------------------


#define I_DIRECTORY "directory-1"

typedef struct Idirectory
{
	INTERFACE_HEAD_DECL;

	void (*GetList)(void);
	
} Idirectory;

#endif

