
#include <gl/gl.h>

#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;
local Imainloop *ml;
local Iparsebmp *bmp;

BOOL draw=FALSE;
HDC *ghdc;
int screenwidth=0;
int screenheight=0;


local void StartDrawing(HWND hwnd, HDC *hdc, HGLRC *hrc)
{	
	*hdc=GetDC(hwnd);
	ghdc=hdc;

	PIXELFORMATDESCRIPTOR pfd;
	ZeroMemory(&pfd,sizeof(pfd));
	pfd.nSize=sizeof(pfd);
	pfd.nVersion=1;
	pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=24;
	pfd.cDepthBits=16;
	pfd.iLayerType=PFD_MAIN_PLANE;
	int format=ChoosePixelFormat(*hdc,&pfd);
	if(!format) lm->Log(L_INFO,"<opengl> Bad pixel format");
	if(!SetPixelFormat(*hdc,format,&pfd)) lm->Log(L_INFO,"<opengl> Can't set pixel format");

	*hrc=wglCreateContext(*hdc);
	if(!*hrc) lm->Log(L_INFO,"<opengl> Can't create rendering context");
	if(!wglMakeCurrent(*hdc,*hrc)) lm->Log(L_INFO,"<opengl> Can't activate GLRC");
	
	DO_CBS(CB_CREATEGRAPHICS,CreateGraphicsFunc,());
	
	draw=TRUE;
	lm->Log(L_INFO,"<opengl> Started drawing");
}

local void StopDrawing(HWND hwnd, HDC hdc, HGLRC hrc)
{
	draw=FALSE;
	
	wglMakeCurrent(NULL,NULL);
	wglDeleteContext(hrc);
	ReleaseDC(hwnd,hdc);
	
	DO_CBS(CB_DESTROYGRAPHICS,DestroyGraphicsFunc,());

	lm->Log(L_INFO,"<opengl> Stopped drawing");
}

local void SetResolution(int x, int y)
{
	glViewport(0,0,x,y);
	
	screenwidth=x;
	screenheight=y;
	
	lm->Log(L_INFO,"<opengl> Draw resolution set to (%i,%i)",x,y);
}

local void init()
{
}

local void MakeTexture(int* id, unsigned char* data, int width, int height, BOOL transparent)
{
	int type=GL_RGB;
	if(transparent) type=GL_RGBA;
	
	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE); //GL_DECAL
	glPixelStorei(GL_UNPACK_ALIGNMENT,1);
	
	glGenTextures(1,(GLuint*)id); //assigns a texture id
	glBindTexture(GL_TEXTURE_2D,(GLuint)*id); //select texture by id
	glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP);
	glTexImage2D(GL_TEXTURE_2D,0,type,(GLsizei)width,(GLsizei)height,0,GL_RGBA,GL_UNSIGNED_BYTE,data);
	
	glDisable(GL_TEXTURE_2D);
	
	int err=glGetError();
	if(err) lm->Log(L_INFO,"<opengl> gl err=%i",err);
}

local void CreateGraphic(char* filename, int* id, int* width, int* height, BOOL transparent)
{
	unsigned char* data=bmp->ParseBMP(filename,width,height);
	MakeTexture(id,data,*width,*height,transparent);
}

local void DrawSprite(int id, int x, int y, int w, int h, int col, int row, int maxcols, int maxrows)
{
	double tw=1/(double)maxcols;
	double th=1/(double)maxrows;
	double tx1=tw*(col-1);
	double tx2=tx1+tw;
	double ty1=th*(row-1);
	double ty2=ty1+th;
	
	int x1=x-w/2;
	int x2=x+w/2;
	int y1=y-h/2;
	int y2=y+h/2;
	
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,(GLuint)id);
	glBegin(GL_QUADS);
	glTexCoord2d(tx1,ty1); glVertex2d(x1,y1);
	glTexCoord2d(tx1,ty2); glVertex2d(x1,y2);
	glTexCoord2d(tx2,ty2); glVertex2d(x2,y2);
	glTexCoord2d(tx2,ty1); glVertex2d(x2,y1);
	glEnd();
	glDisable(GL_TEXTURE_2D);
}

local void Translate(int x, int y)
{
	glTranslated(x,y,0);
}

local void Scale(double n)
{
	glScaled(n,n,1);
}

local void Rotate(double n, int x, int y)
{
	glRotated(n,0,0,1);
}

double rot=0;
long lasttime=0;
local void MainLoop()
{
	if(draw)
	{
		long now=GetTickCount();
		if(now-lasttime < 1000/50) return; //frame limiter
		lasttime=now;
	
		glClearColor(2/rot,3/rot,7/rot,1.0f);
		// glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
		glClear(GL_COLOR_BUFFER_BIT);
		glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA); //GL_SRC_ALPHA
		
		// glEnable(GL_DEPTH_TEST);
		// glDepthFunc(GL_LESS);
		
		// glAlphaFunc(GL_GREATER,0.1f);
		// glEnable(GL_ALPHA_TEST);
		
		glColor3f(1.0f,1.0f,1.0f);
		
		glLoadIdentity();
		glMatrixMode(GL_PROJECTION);
		
		glPushMatrix();
		glRotated(rot,0.0,0.0,1.0);
		glBegin(GL_TRIANGLES);
		glColor3f(1.0f,0.0f,0.0f); glVertex2f(0.0f,1.0f);
		glColor3f(0.0f,1.0f,0.0f); glVertex2f(0.87f,-0.5f);
		glColor3f(0.0f,0.0f,1.0f); glVertex2f(-0.87f,-0.5f);
		glEnd();
		glPopMatrix();
		
		// glRotatef(rot,rot/2,rot/3,1.0f);
		// gluCylinder(gluNewQuadric(),.2,.3,.4,50,50);
		
		glPushMatrix();
		glRotated(-rot*2,0.0,0.0,1.0);
		glTranslatef(.0f,.75f,.0f);
		glColor3f(0.0f,0.75f,0.0f);
		glBegin(GL_QUADS);
		glVertex3f(-.1f,-.1f,.0f);
		glVertex3f(-.1f,.1f,.0f);
		glVertex3f(.1f,.1f,.0f);
		glColor4f(.0f,.5f,.0f,.1f);
		glVertex3f(.1f,-.1f,.0f);
		glEnd();
		glPopMatrix();
		
		
		glColor3f(1.0f,1.0f,1.0f);

		glLoadIdentity();
		
		//enable 2D
		int vPort[4];
		glGetIntegerv(GL_VIEWPORT,vPort);
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();
		glOrtho(0,vPort[2],vPort[3],0,-50,50);
		glMatrixMode(GL_MODELVIEW);
		// glPushMatrix();
		glLoadIdentity();
		// glPushAttrib(GL_DEPTH_BUFFER_BIT|GL_LIGHTING_BIT);
		// glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);
		
		
        glEnable(GL_BLEND);
		// glMatrixMode(GL_MODELVIEW);
		
		// int layer;
		// for(layer=DL_BELOWALL; layer <= DL_TOPMOST; layer++) DO_CBS(CB_DRAWGRAPHICS,DrawGraphicsFunc,(layer));
		
		glPushMatrix();
		DO_CBS(CB_DRAWGRAPHICS,DrawGraphicsFunc,(DL_TILES));
		DO_CBS(CB_DRAWGRAPHICS,DrawGraphicsFunc,(DL_SHIPS));
		DO_CBS(CB_DRAWGRAPHICS,DrawGraphicsFunc,(DL_AFTERSHIPS));
		glPopMatrix();
		
		
		//disable 2D
		// glMatrixMode(GL_PROJECTION);
		glPopMatrix();
		// glMatrixMode(GL_MODELVIEW);
		glPopMatrix();
		
		
		// glLoadIdentity();
		glPushMatrix();
		DO_CBS(CB_DRAWGRAPHICS,DrawGraphicsFunc,(DL_HUD));
		DO_CBS(CB_DRAWGRAPHICS,DrawGraphicsFunc,(DL_AFTERHUD));
		DO_CBS(CB_DRAWGRAPHICS,DrawGraphicsFunc,(DL_CHAT));
		DO_CBS(CB_DRAWGRAPHICS,DrawGraphicsFunc,(DL_AFTERCHAT));
		DO_CBS(CB_DRAWGRAPHICS,DrawGraphicsFunc,(DL_TOPMOST));
		glPopMatrix();
		
		
		
		// glFlush();
		SwapBuffers(*ghdc);
		
		rot+=.1f;
		
		
	}
}

local Iopengl oglint=
{
	INTERFACE_HEAD_INIT(I_OPENGL,"opengl"),
	StartDrawing, StopDrawing,
	SetResolution,
	CreateGraphic, DrawSprite,
	Translate, Scale, Rotate
};

EXPORT int MM_opengl(Imodman *mm2, mod_action action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		ml=mm->GetInterface(I_MAINLOOP);
		bmp=mm->GetInterface(I_PARSEBMP);
		if(!lm || !ml || !bmp) return MM_FAIL;
		
		init();
		
		mm->RegInterface(&oglint);
		
		mm->RegCallback(CB_MAINLOOP,MainLoop);
		mm->RegCallback(CB_RESOLUTION,SetResolution);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&oglint)) return MM_FAIL;
		
		mm->UnregCallback(CB_MAINLOOP,MainLoop);
		mm->UnregCallback(CB_RESOLUTION,SetResolution);
		
		mm->ReleaseInterface(bmp);
		mm->ReleaseInterface(ml);
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}

