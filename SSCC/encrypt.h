
#ifndef __ENCRYPT_H
#define __ENCRYPT_H

//called when data can be sent
#define CB_CONNECTIONREADY "connectionready"
typedef void (*ConnectionReadyFunc)(int dest);

//call when done
// #define CB_DISCONNECT "disconnect"
// typedef void (*DisconnectFunc)(int dest);

typedef struct Iencrypt
{
	INTERFACE_HEAD_DECL;

	int (*Encrypt)(Byte *pkt, int len);
	int (*Decrypt)(Byte *pkt, int len);
	
} Iencrypt;

#endif

