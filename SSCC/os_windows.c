
#include <stdio.h>

#ifndef WINVER
#define WINVER 0x601
//#define WINVER 0x0500
// #define WINVER _WIN32_WINNT_WINXP
#endif
#include <windows.h>
#include <commctrl.h>

#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;
local Imainloop *ml;
local Iopengl *ogl;
local Ikeyboard *kb;

// #include "keyboard.inc.c"

HINSTANCE hInst;
RECT desktop;

HDC hdc;
HGLRC hrc;

HWND zonelist;
HWND renderwin;

local void ExitRender();

enum 
{ 
    ID_NULL, ID_BUTTON, ID_LIST
};
/*
local HWND NewWindow(const char *title, flags, width, height, x, y, WNDPROC winfunc)
{
    wc.lpszMenuName=NULL;
    wc.lpfnWndProc=(WNDPROC)winfunc;
    RegisterClass(&wc);
    mainwin=CreateWindow(classname,title,WS_OVERLAPPEDWINDOW|WS_MINIMIZEBOX|WS_MAXIMIZEBOX|WS_HSCROLL|WS_VSCROLL,0,0,1000,1000,0,0,hInst,0);
}*/

HWND stx, button, list;
local LRESULT APIENTRY ZoneListWindow(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch(msg)
	{
        case WM_CREATE:
        {
			stx=CreateWindow("Static","This is text",WS_CHILD|WS_VISIBLE,5,5,200,25,hwnd,(HMENU)ID_NULL,hInst,0);
			// button=CreateWindow("Button","Play",BS_PUSHBUTTON|WS_CHILD|WS_VISIBLE,350,105,100,25,hwnd,(HMENU)ID_BUTTON,hInst,0);
			// button=CreateWindow("Button","Play",WS_CHILD|BS_OWNERDRAW,5,50,100,25,hwnd,(HMENU)ID_BUTTON,hInst,0);
			button=CreateWindow("Button","Play",BS_PUSHBUTTON|WS_CHILD|WS_VISIBLE,5,50,100,25,hwnd,(HMENU)ID_BUTTON,hInst,0);
if(!button)
printf("button null\n");
			// #define LVS_EX_FULLROWSELECT  0x20
list=CreateWindow(WC_LISTVIEW,"",LVS_REPORT/*|LVS_NOCOLUMNHEADER/*|LVS_OWNERDRAWFIXED*/ /*|LVS_EX_FULLROWSELECT*/|WS_VISIBLE|WS_CHILD|WS_BORDER,5,100,200,200,hwnd,(HMENU)ID_LIST,hInst,0);

            int lvcc=0;
            LVCOLUMN lvc;
            lvc.mask=LVCF_FMT|LVCF_WIDTH|LVCF_TEXT|LVCF_SUBITEM;
            lvc.iSubItem=0;
            lvc.cx=50;
            lvc.fmt=LVCFMT_LEFT;
            lvc.pszText="Ping";
            SendMessage(list,LVM_INSERTCOLUMN,lvcc++,(LPARAM)&lvc);
            lvc.cx=50;
            lvc.fmt=LVCFMT_RIGHT;
            lvc.pszText="Players";
            SendMessage(list,LVM_INSERTCOLUMN,lvcc++,(LPARAM)&lvc);
            lvc.cx=75;
            lvc.fmt=LVCFMT_LEFT;
            lvc.pszText="Name";
            SendMessage(list,LVM_INSERTCOLUMN,lvcc++,(LPARAM)&lvc);
			
			//SendMessage(list,LVM_SETEXTENDEDLISTVIEWSTYLE,0,LVS_EX_FULLROWSELECT); //LVS_EX_GRIDLINES
			
			
			
int lvic=0;
LVITEM lvi;
int lvisc=0;
lvi.mask=LVIF_TEXT;
lvi.cchTextMax=512;
lvi.iItem=lvic++;
lvi.iSubItem=lvisc++;
lvi.pszText="50";
lvi.iImage=lvic;
SendMessage(list,LVM_INSERTITEM,0,(LPARAM)&lvi);

lvi.iSubItem=lvisc++;
lvi.pszText="10";
SendMessage(list,LVM_SETITEM,0,(LPARAM)&lvi);

lvi.iSubItem=lvisc++;
lvi.pszText="SomeZone";
SendMessage(list,LVM_SETITEM,0,(LPARAM)&lvi);

		}
		break;
		case WM_COMMAND:
		{
			/*
			switch(wParam) //menu crap
			{
			}
			*/
			switch(HIWORD(wParam))
			{
				case BN_CLICKED:
				{
					switch(wParam)
					{
						case ID_BUTTON:
						{
							ShowWindow(zonelist,SW_HIDE);
							// ShowWindow(renderwin,SW_SHOW);
							AnimateWindow(renderwin,500,AW_CENTER);
						}
						break;
					}
				}
				break;
			}
		}
		break;
		case WM_DRAWITEM:
		{
printf("WM_DRAWITEM\n");
			switch(wParam)
			{
				case ID_BUTTON:
				{
printf("WM_DRAWITEM ID_BUTTON\n");
					LPDRAWITEMSTRUCT lpdis=(DRAWITEMSTRUCT*)lParam;
					SIZE size;
					char text[256];
					sprintf(text,"%s","Play");
					GetTextExtentPoint32(lpdis->hDC,text,strlen(text),&size);
					SetTextColor(lpdis->hDC,RGB(0,200,0));
					SetBkColor(lpdis->hDC,RGB(0,0,100));

					ExtTextOut(lpdis->hDC,
						((lpdis->rcItem.right-lpdis->rcItem.left)-size.cx)/2,
						((lpdis->rcItem.bottom-lpdis->rcItem.top)-size.cy)/2,
						ETO_OPAQUE|ETO_CLIPPED,&lpdis->rcItem,text,strlen(text),NULL);

					DrawEdge(lpdis->hDC,&lpdis->rcItem,(lpdis->itemState & ODS_SELECTED ? EDGE_SUNKEN : EDGE_RAISED),BF_RECT);

					return TRUE;
				}
				break;
				case ID_LIST:
				{
				}
				break;
			}
		}
		break;
		case WM_PRINT: //prob not needed
		{
printf("WM_PRINT1\n");
			return DefWindowProc(hwnd,msg,wParam,lParam);
		}
		break;
		case WM_SIZE:
		{
		}
		break;
		case WM_DESTROY:
		{
			PostQuitMessage(0);
		}
		break;
		default: return DefWindowProc(hwnd,msg,wParam,lParam);
	}
}

local LRESULT APIENTRY RenderWindow(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch(msg)
	{
        case WM_CREATE:
        {
		}
		break;
		case WM_SHOWWINDOW:
		{
			if(wParam) //being shown
			{ //ShowWindow()
				// ZeroMemory(keystates,sizeof(char)*256);
				// ogl->StartDrawing(renderwin,&hdc,&hrc);
			}
			else //being hidden
			{
				ogl->StopDrawing(renderwin,hdc,hrc);
			}
		}
		break;
		case WM_PRINT:
		{ //AnimateWindow()
			ogl->StartDrawing(renderwin,&hdc,&hrc);
			
			return DefWindowProc(hwnd,msg,wParam,lParam);
		}
		break;
		case WM_ACTIVATEAPP:
		{
// printf("WM_ACTIVATEAPP\n");
			// if(!wParam) //losing focus
		}
		break;/*
		case WM_SYSCOMMAND:
		{
			switch(wParam)
			{
				case SC_SCREENSAVE://screensaver trying to start
				case SC_MONITORPOWER://monitor trying to enter power save mode
					return 0; //prevent
			}
		}
		break;*/
		case WM_CHAR:
		{
// printf("WM_CHAR n=%d=%02x\n",wParam,wParam);
			if(kb->updatekeyboard(wParam,TRUE,FALSE)) ExitRender();
		}
		break;
		case WM_KEYDOWN:
		{
// printf("WM_KEYDOWN n=%d=%02x\n",wParam,wParam);
			
			if(kb->updatekeyboard(wParam,TRUE,TRUE)) ExitRender();
		}
		break;
		case WM_KEYUP:
		{
			kb->updatekeyboard(wParam,FALSE,TRUE);
		}
		break;
		case WM_SIZE:
		{
			int x=LOWORD(lParam);
			int y=HIWORD(lParam);
			// ogl->SetResolution(x,y);
			DO_CBS(CB_RESOLUTION,ResolutionFunc,(x,y));
		}
		break;
		case WM_CLOSE:
		{
			ShowWindow(renderwin,SW_HIDE);
			ShowWindow(zonelist,SW_SHOW);
		}
		break;
		default: return DefWindowProc(hwnd,msg,wParam,lParam);
	}
    return 0;
}

local void init()
{
	GetWindowRect(GetDesktopWindow(),&desktop);
	int width=desktop.right;
	int height=desktop.bottom;

    WNDCLASS wc;
    wc.cbClsExtra=0;
    wc.cbWndExtra=0;
    // wc.hbrBackground=(HBRUSH)(COLOR_BACKGROUND+1);
	wc.hbrBackground=(HBRUSH)GetStockObject(BLACK_BRUSH);
    wc.hInstance=hInst;
	wc.hIcon=LoadIcon(NULL,IDI_QUESTION);
	wc.hCursor=LoadCursor(NULL,IDC_ARROW);
    wc.lpfnWndProc=(WNDPROC)ZoneListWindow;
    wc.lpszClassName="zonelist";
    wc.lpszMenuName=NULL;//MAKEINTRESOURCE(IDM_MENU1);
    wc.style=CS_HREDRAW|CS_VREDRAW;
    RegisterClass(&wc);
    
    wc.lpszClassName="renderwin";
    wc.lpszMenuName=NULL;
    wc.lpfnWndProc=(WNDPROC)RenderWindow;
    RegisterClass(&wc);
    
    zonelist=CreateWindow("zonelist","SubSpace Continuum",WS_OVERLAPPEDWINDOW|WS_MINIMIZEBOX|WS_MAXIMIZEBOX,0,0,500,500,0,0,hInst,0);
    renderwin=CreateWindow("renderwin","SubSpace Continuum",WS_POPUP,0,0,width,height,0,0,hInst,0);
	
	ShowWindow(renderwin,SW_HIDE);
    ShowWindow(zonelist,SW_SHOW);
	lm->Log(L_INFO,"<windows> created initial windows");
}

local void MainLoop()
{
    MSG msg;
	if(PeekMessage(&msg,0,0,0,PM_REMOVE))
	{
		if(msg.message == WM_QUIT) 
		{
			lm->Log(L_INFO,"<windows> recieved WM_QUIT");
			ml->Quit(EXIT_OK);
		} 
		else 
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
}

local void ExitRender()
{
	ShowWindow(renderwin,SW_HIDE);
    ShowWindow(zonelist,SW_SHOW);
}

EXPORT int MM_os_windows(Imodman *mm2, mod_action action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		ml=mm->GetInterface(I_MAINLOOP);
		ogl=mm->GetInterface(I_OPENGL);
		kb=mm->GetInterface(I_KEYBOARD);
		if(!lm || !ml || !ogl || !kb) return MM_FAIL;
		
		init();
		
		mm->RegCallback(CB_MAINLOOP,MainLoop);
		// mm->RegCallback(CB_EXITRENDER,ExitRender);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		mm->UnregCallback(CB_MAINLOOP,MainLoop);
		// mm->UnregCallback(CB_EXITRENDER,ExitRender);
		
		mm->ReleaseInterface(kb);
		mm->ReleaseInterface(ogl);
		mm->ReleaseInterface(ml);
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}

