
//#include <sys/types.h>

#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;
local Iencrypt *enc;

#ifdef WIN32
#define close(x) closesocket(x)
#endif

typedef struct sockaddr_in sockaddrin;

pthread_t sthd;
pthread_t rthd;
pthread_t rthd2;
pthread_t rthd3;
	
SOCKET zonesock=INVALID_SOCKET;
SOCKET pingsock=INVALID_SOCKET;
SOCKET dirsock=INVALID_SOCKET;

local LinkedList packethandlers=LL_INITIALIZER;
local LinkedList corehandlers=LL_INITIALIZER;

BOOL connected=FALSE;
BOOL connected2=FALSE;
BOOL connected3=FALSE;

typedef struct PacketHandler
{
	int dest;
	Byte type;
	PacketFunc func;
} PacketHandler;

local void printpacket(Byte *pkt, int len, int dest, int core, int send)
{
if(pkt[0]==S2C_KEEPALIVE) return;
if(pkt[0]==S2C_POSITION) return;
if((!pkt[0]) && (pkt[1]==0x03)) return;
if((!pkt[0]) && (pkt[1]==0x04)) return;
if((!pkt[0]) && (pkt[1]==0x0A)) return;

printf("%s%son %d: size=%d\n",(send ? "send " : "recv "),(core ? "core " : ""),dest,len);
	int i=0;
	int c=0;
	for(i=0; i<len; i++)
	{
		if(c >= 16)
		{
			printf("\n");
			c=0;
		}
		printf("%02x ",pkt[i]);
		c++;
	}
	printf("\n");
}

local void AddPacket(Byte type, PacketFunc func)
{
	PacketHandler *ph=amalloc(sizeof(PacketHandler));
	ph->type=type;
	ph->func=func;
	LLAddLast(&packethandlers,ph);
}

local void RemovePacket(Byte type, PacketFunc func)
{
	PacketHandler *ph;
	FOR_EACH_LINK(packethandlers,ph,l)
	{
		if((ph->type == type) && (ph->func == func))
		{
			LLRemove(&packethandlers,ph);
			break;
		}
	}
}

local void AddCorePacket(Byte type, PacketFunc func)
{
	PacketHandler *ph=amalloc(sizeof(PacketHandler));
	ph->type=type;
	ph->func=func;
	LLAddLast(&corehandlers,ph);
}

local void RemoveCorePacket(Byte type, PacketFunc func)
{
	PacketHandler *ph;
	FOR_EACH_LINK(corehandlers,ph,l)
	{
		if((ph->type == type) && (ph->func == func))
		{
			LLRemove(&corehandlers,ph);
			break;
		}
	}
}

local char* SocketErrorDesc(const int Error)
{
#ifdef WIN32
	switch(Error)
	{
		case WSAEINTR:				return "Interrupted system call";
		case WSAEBADF:				return "Bad file number";
		case WSAEACCES:				return "Permission denied";
		case WSAEFAULT:				return "Bad address";
		case WSAEINVAL:				return "Invalid argument";
		case WSAEMFILE:				return "Too many open files";
		case WSAEWOULDBLOCK:		return "Operation would block";
		case WSAEINPROGRESS:		return "Operation now in progress";
		case WSAEALREADY:			return "Operation already in progress";
		case WSAENOTSOCK:			return "Socket operation on non-socket";
		case WSAEDESTADDRREQ:		return "Destination address required";
		case WSAEMSGSIZE:			return "Message too long";
		case WSAEPROTOTYPE:			return "Protocol wrong type for socket";
		case WSAENOPROTOOPT:		return "Protocol not available";
		case WSAEPROTONOSUPPORT:	return "Protocol not supported";
		case WSAESOCKTNOSUPPORT:	return "Socket type not supported";
		case WSAEOPNOTSUPP:			return "Operation not supported on socket";
		case WSAEPFNOSUPPORT:		return "Protocol family not supported";
		case WSAEAFNOSUPPORT:		return "Address family not supported by protocol family";
		case WSAEADDRINUSE:			return "Address already in use";
		case WSAEADDRNOTAVAIL:		return "Can't assign requested address";
		case WSAENETDOWN:			return "Network is down";
		case WSAENETUNREACH:		return "Network is unreachable";
		case WSAENETRESET:			return "Network dropped connection on reset";
		case WSAECONNABORTED:		return "Software caused connection abort";
		case WSAECONNRESET:			return "Connection reset by peer";
		case WSAENOBUFS:			return "No buffer space available";
		case WSAEISCONN:			return "Socket is already connected";
		case WSAENOTCONN:			return "Socket is not connected";
		case WSAESHUTDOWN:			return "Can't send after socket shutdown";
		case WSAETOOMANYREFS:		return "Too many references: can't splice";
		case WSAETIMEDOUT:			return "Connection timed out";
		case WSAECONNREFUSED:		return "Connection refused";
		case WSAELOOP:				return "Too many levels of symbolic links";
		case WSAENAMETOOLONG:		return "File name too long";
		case WSAEHOSTDOWN:			return "Host is down";
		case WSAEHOSTUNREACH:		return "No route to host";
		case WSAENOTEMPTY:			return "Directory not empty";
		case WSAEPROCLIM:			return "Too many processes";
		case WSAEUSERS:				return "Too many users";
		case WSAEDQUOT:				return "Disc quota exceeded";
		case WSAESTALE:				return "Stale NTFS file handle";
		case WSAEREMOTE:			return "Too many levels of remote in path";
		case WSASYSNOTREADY:		return "Network sub-system is unusable";
		case WSAVERNOTSUPPORTED:	return "WinSock DLL cannot support this application";
		case WSANOTINITIALISED:		return "WinSock not initialized";
		case WSAHOST_NOT_FOUND:		return "Host not found";
		case WSATRY_AGAIN:			return "Non-authoritative host not found";
		case WSANO_RECOVERY:		return "Non-recoverable error";
		case WSANO_DATA:			return "No Data";
		default:					return "Not a WinSock error";
	}
#endif
	return NULL;
}

local void SendPacket(int dest, void *vp, int size)
{
	// Byte *pkt=amalloc(size); //immediately copy to prevent segfault
	// memcpy(pkt,vp,size);

	SOCKET socket=INVALID_SOCKET;
	if(dest == DEST_ZONE) socket=zonesock;
	else if(dest == DEST_DIR) socket=dirsock;
	else if(dest == DEST_PING) socket=pingsock;

	Byte *pkt=vp;
	
if(size != 32)
{
if(!pkt[0] && pkt[1]==4) goto asdf;
// printf("send on %d: size=%d\n",dest,size);
printpacket(pkt,size,dest,0,1);
asdf:
size=size;
}
	if(enc) enc->Encrypt(pkt,size);
	// if(send(sock,(char*)pkt,size,0) == -1) lm->Log(L_WARN,"send failed");
	if(send(socket,(char*)pkt,size,0) == -1)
	{
		printf("send failed: %s\n",SocketErrorDesc(WSAGetLastError()));
		printf("send failed: errno='%s'\n",strerror(errno));
	}
	
	// afree(pkt);
}

local void ReceivePacket(int dest, void *vp, int len)
{
	if(len < 1) return;
	
	Byte *pkt=vp;
	Byte type=pkt[0];
	Byte subtype=0;
	if(len >= 2) subtype=pkt[1];
	
	if(type == CORE_PACKET)
	{
		BOOL handled=FALSE;
		PacketHandler *ph;
		FOR_EACH_LINK(corehandlers,ph,l)
		{
			if(ph->type != subtype) continue;
			
			if(ph->func(dest,pkt,len))
			{
				handled=TRUE;
				// break; //ultimately not a good idea
			}
		}
		if(!handled) printf("unhandled core packet type on %d: %02x\n",dest,pkt[1]);
		else
		{
			// printf("recv core on %d: size=%d\n",dest,len);
			printpacket(pkt,len,dest,1,0);
		}
	}
	else
	{
		BOOL handled=FALSE;
		PacketHandler *ph;
		FOR_EACH_LINK(packethandlers,ph,l)
		{
			if(ph->type != type) continue;
			
			if(ph->func(dest,pkt,len))
			{
				handled=TRUE;
				// break; //ultimately not a good idea
			}
		}
		if(!handled) printf("unhandled packet type on %d: %02x\n",dest,pkt[0]);
		else
		{
			// printf("recv on %d: size=%d\n",dest,len);
			printpacket(pkt,len,dest,0,0);
		}
	}

	
}

local void* ReceiveThread(void *vp)
{
	while(1)
	{
		if(connected)
		{
			unsigned char rbuf[1024];
			memset(rbuf,0,sizeof(rbuf));
			int bytes=recv(zonesock,rbuf,sizeof(rbuf),0); //if set nonblocking, will return -1 on nothing there
			// if(bytes == -1) lm->Log(L_WARN,"recv failed");
			// if(bytes == -1) printf("recv failed: errno='%s'\n",strerror(errno));
			// if(bytes == -1) printf("recv failed: %s\n",SocketErrorDesc(WSAGetLastError()));
			// if(bytes == -1) break;
			
			if(bytes != -1)
			{
				Byte *pkt=(Byte*)rbuf;
	// printf("recv:\n");
	// printpacket(pkt,bytes);
				if(enc) enc->Decrypt(pkt,bytes);
				ReceivePacket(DEST_ZONE,pkt,bytes);
			}
		}
		fullsleep(5);
	}
}

local void* ReceiveThread2(void *vp)
{
	while(1)
	{
		if(connected2)
		{
			unsigned char rbuf[1024];
			memset(rbuf,0,sizeof(rbuf));
			int bytes=recv(dirsock,rbuf,sizeof(rbuf),0); //if set nonblocking, will return -1 on nothing there
			if(bytes == -1) printf("recv2 failed: errno='%s'\n",strerror(errno));
			if(bytes == -1) printf("recv2 failed: %s\n",SocketErrorDesc(WSAGetLastError()));
			// if(bytes == -1) break;
			
			if(bytes != -1)
			{
				Byte *pkt=(Byte*)rbuf;
				
// printf("recv on 2:\n");
// printpacket(pkt,bytes);
				if(enc) enc->Decrypt(pkt,bytes);
// printf("recv dec on 2:\n");
// printf("recv on 2:\n");
// printpacket(pkt,bytes);

				ReceivePacket(DEST_DIR,pkt,bytes);

			}
		}
		fullsleep(25);
	}
}

local void* ReceiveThread3(void *vp)
{
	while(1)
	{
		if(connected3)
		{
			unsigned char rbuf[1024];
			memset(rbuf,0,sizeof(rbuf));
			int bytes=recv(pingsock,rbuf,sizeof(rbuf),0); //if set nonblocking, will return -1 on nothing there
			if(bytes == -1) printf("recv2 failed: errno='%s'\n",strerror(errno));
			if(bytes == -1) printf("recv2 failed: %s\n",SocketErrorDesc(WSAGetLastError()));
			// if(bytes == -1) break;
			
			if(bytes != -1)
			{
				Byte *pkt=(Byte*)rbuf;

printf("console color test:  ->  text \27[31m text  \27[0m text   <- \n"); //no ansi on windows?

// printf("recv on 3:\n");
printpacket(pkt,bytes,3,0,0);

				S2CPingResponse *pr=(S2CPingResponse*)rbuf;
				printf("%d players. time=%d now=%d diff=%d\n",pr->total,pr->timestamp,current_ticks(),current_ticks()-pr->timestamp);
			}
		}
		fullsleep(25);
	}
}

local void* SendThread(void *vp)
{
	fullsleep(10);
}

local void RegisterEncryption(Iencrypt *ie)
{
	enc=ie; //last one loaded wins!
}

local void ConnectionConnect(int dest, char *ip, int port)
{
printf("connecting to %s:%d\n",ip,port);
	
	sockaddrin sain;
	sain.sin_family=AF_INET;
	sain.sin_addr.s_addr=inet_addr(ip);
	sain.sin_port=htons(port);
	
	if(dest == DEST_ZONE)
	{
		if(connect(zonesock,(SOCKADDR*)&sain,sizeof(sain)) == -1) lm->Log(L_WARN,"no server");
		else connected=TRUE;
	}
	else if(dest == DEST_DIR)
	{
		if(connect(dirsock,(SOCKADDR*)&sain,sizeof(sain)) == -1) lm->Log(L_WARN,"no server2");
		else connected2=TRUE;
	}
	else if(dest == DEST_PING)
	{
		if(connect(pingsock,(SOCKADDR*)&sain,sizeof(sain)) == -1) lm->Log(L_WARN,"no server3");
		else connected3=TRUE;
	}
	
	DO_CBS(CB_CONNECTIONINIT,ConnectionInitFunc,(dest));
}

local void init(void)
{


#ifdef WIN32
	WSADATA wsaData;
	if(WSAStartup(MAKEWORD(2,2),&wsaData) != NO_ERROR) lm->Log(L_WARN,"bad wsastartup: %d",WSAGetLastError());
#endif

	zonesock=socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);
	if(zonesock == -1) lm->Log(L_WARN,"bad socket");
	pingsock=socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);
	if(pingsock == -1) lm->Log(L_WARN,"bad socket2");
	dirsock=socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);
	if(dirsock == -1) lm->Log(L_WARN,"bad socket3");
	
	
	sockaddrin sain;
	sain.sin_family=AF_INET;
	sain.sin_addr.s_addr=inet_addr("66.36.241.110"); //tw
	sain.sin_port=htons(5400+1);
	if(connect(pingsock,(SOCKADDR*)&sain,sizeof(sain)) == -1) lm->Log(L_WARN,"no server2");
	else connected3=TRUE;
	
	
	C2SPingRequest pr;
	pr.timestamp=current_ticks();
	
	pthread_create(&sthd,NULL,SendThread,NULL);
	pthread_create(&rthd,NULL,ReceiveThread,NULL);
	pthread_create(&rthd2,NULL,ReceiveThread2,NULL);
	pthread_create(&rthd3,NULL,ReceiveThread3,NULL);
	
	if(send(pingsock,(char*)&pr,sizeof(C2SPingRequest),0) == -1) //lm->Log(L_WARN,"send failed");
	{
		printf("send2 failed: %s\n",SocketErrorDesc(WSAGetLastError()));
		printf("send2 failed: errno='%s'\n",strerror(errno));
	}
	
}

local Inet netint=
{
	INTERFACE_HEAD_INIT(I_NET,"net"),
	AddPacket, RemovePacket, AddCorePacket, RemoveCorePacket,
	SendPacket, ReceivePacket,
	RegisterEncryption
};

EXPORT int MM_net(Imodman *mm2, mod_action action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		// enc=mm->GetInterface("enc-vie");
		enc=NULL; //to be gotten later
		if(!lm) return MM_FAIL;
		
		mm->RegCallback(CB_CONNECTIONCONNECT,ConnectionConnect);
		
		mm->RegInterface(&netint);
		
		init();
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&netint)) return MM_FAIL;
		
		mm->UnregCallback(CB_CONNECTIONCONNECT,ConnectionConnect);
		
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}


