
#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;
local Inet *net;
local Imainloop *ml;

//typedef struct Weapons
//{
//	u16 type			:5;
//	u16 level			:2;
//	u16 shrapbouncing	:1;
//	u16 shraplevel		:2;
//	u16 shrap			:5;
//	u16 alternate		:1;
//} Weapons;

// Ship myship;

bool inzone=FALSE;
bool inarena=FALSE;

local void DoChecksum(C2SPosition *pkt)
{
	int size=sizeof(C2SPosition)-sizeof(struct ExtraPosData); // =22
	u8 ck=0;
	pkt->checksum=0;
	int i;
	for(i=0; i < size; i++) ck^=((Byte*)pkt)[i];
	pkt->checksum=ck;
}


	C2SPosition pos;
// int postime=0;
local int positionupdate(void* vp)
{
// printf("z=%d z=%d\n",inzone,inarena);
	if(!inzone || !inarena) return TRUE;
	
	int size=sizeof(C2SPosition);
	memset(&pos,0,size);
	
	pos.type=C2S_POSITION;
	pos.time=1;
	// pos.time=current_ticks();
	// pos.time=current_ticks()/10;
	// pos.time=postime;
// postime++;
// if(postime > 100) postime=0;
	// pos.x=512*16;
	// pos.y=512*16;
	pos.x=512;
	pos.y=512;
	DoChecksum(&pos);
	
	net->SendPacket(DEST_ZONE,&pos,sizeof(C2SPosition));

}

local void LocationChange(LocationReason reason)
{
	switch(reason)
	{
		case LR_ZONEENTER:
			inzone=TRUE;
			break;
		case LR_ZONEEXIT:
			inzone=FALSE;
			break;
		// case LR_ARENAENTER:
			// inarena=TRUE;
			// break;
		case LR_ARENAEXIT:
			inarena=FALSE;
			break;
	}
}

local void SetShip(ShipType ship)
{
	int size=sizeof(C2SShipChange);
	C2SShipChange *sc=amalloc(size);
	sc->type=C2S_SETSHIP;
	sc->ship=ship;
	net->SendPacket(DEST_ZONE,sc,size);
	
	self.ship=ship;
printf("set ship=%d\n",ship);
}

local void SetFreq(int freq)
{
	int size=sizeof(C2SFreqChange);
	C2SFreqChange *fc=amalloc(size);
	fc->type=C2S_SETFREQ;
	fc->freq=freq;
	net->SendPacket(DEST_ZONE,fc,size);
	
	self.freq=freq;
printf("set freq=%d\n",freq);
}

local int packethandler(int dest, Byte *pkt, int len)
{
	if(dest != DEST_ZONE) return;
	
	switch(pkt[0])
	{
		case S2C_ENTERINGARENA:
		{
printf("entering arena\n");
			
//			SetShip(SHIP_SPEC);
			inarena=TRUE;
			
			return TRUE;
		}
		break;
	}

	return FALSE;
}

local void ShipChange(int ship)
{
	SetShip(ship);
}

local void FreqChange(int freq)
{
	SetFreq(freq);
}

local Iship shipint=
{
	INTERFACE_HEAD_INIT(I_SHIP,"ship"),
	SetShip, SetFreq
};

EXPORT int MM_ship(Imodman *mm2, mod_action action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		net=mm->GetInterface(I_NET);
		ml=mm->GetInterface(I_MAINLOOP);
		if(!lm || !net || !ml) return MM_FAIL;
		
		net->AddPacket(S2C_ENTERINGARENA,packethandler);
		
		mm->RegCallback(CB_LOCATIONCHANGE,LocationChange);
		mm->RegCallback(CB_SHIPCHANGE,ShipChange);
		mm->RegCallback(CB_FREQCHANGE,FreqChange);
		
		ml->SetTimer(positionupdate,25,25,(void*)NULL,NULL);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		mm->UnregCallback(CB_LOCATIONCHANGE,LocationChange);
		mm->UnregCallback(CB_SHIPCHANGE,ShipChange);
		mm->UnregCallback(CB_FREQCHANGE,FreqChange);
		
		net->RemovePacket(S2C_ENTERINGARENA,packethandler);
		
		mm->ReleaseInterface(ml);
		mm->ReleaseInterface(lm);
		mm->ReleaseInterface(net);
		
		return MM_OK;
	}
	return MM_FAIL;
}
