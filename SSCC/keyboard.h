

#ifndef __KEYBOARD_H
#define __KEYBOARD_H


///----------------------------------------------------------------------------------------------------
//windows key codes
#define KB_LBUTTON		0x01	//Left mouse button
#define KB_RBUTTON		0x02	//Right mouse button
#define KB_CANCEL		0x03	//Control-break processing
#define KB_MBUTTON		0x04	//Middle mouse button
#define KB_BACK			0x08	//BACKSPACE key
#define KB_TAB			0x09	//TAB key
#define KB_CLEAR		0x0C	//CLEAR key
#define KB_RETURN		0x0D	//ENTER key
#define KB_SHIFT		0x10	//SHIFT key
#define KB_CONTROL		0x11	//CTRL key
#define KB_MENU			0x12	//ALT key
#define KB_PAUSE		0x13	//PAUSE key
#define KB_CAPITAL		0x14	//CAPS LOCK key
#define KB_ESCAPE		0x1B	//ESC key
#define KB_SPACE		0x20	//SPACEBAR
#define KB_PGUP			0x21	//PAGE UP key
#define KB_PGDN			0x22	//PAGE DOWN key
#define KB_END			0x23	//END key
#define KB_HOME			0x24	//HOME key
#define KB_LEFT			0x25	//LEFT ARROW key
#define KB_UP			0x26	//UP ARROW key
#define KB_RIGHT		0x27	//RIGHT ARROW key
#define KB_DOWN			0x28	//DOWN ARROW key
#define KB_SELECT		0x29	//SELECT key
#define KB_EXECUTE		0x2B	//EXECUTE key
#define KB_SNAPSHOT		0x2C	//PRINT SCREEN key
#define KB_INSERT		0x2D	//INS key
#define KB_DELETE		0x2E	//DEL key
#define KB_HELP			0x2F	//HELP key
#define KB_LWIN			0x5B	//Left Windows key
#define KB_RWIN			0x5C	//Right Windows key
#define KB_APPS			0x5D	//Applications key
#define KB_NUMPAD0		0x60	//Keypad 0 key
#define KB_NUMPAD1		0x61	//Keypad 1 key
#define KB_NUMPAD2		0x62	//Keypad 2 key
#define KB_NUMPAD3		0x63	//Keypad 3 key
#define KB_NUMPAD4		0x64	//Keypad 4 key
#define KB_NUMPAD5		0x65	//Keypad 5 key
#define KB_NUMPAD6		0x66	//Keypad 6 key
#define KB_NUMPAD7		0x67	//Keypad 7 key
#define KB_NUMPAD8		0x68	//Keypad 8 key
#define KB_NUMPAD9		0x69	//Keypad 9 key
#define KB_MULTIPLY		0x6A	//Multiply key
#define KB_ADD			0x6B	//Add key
#define KB_SEPARATOR	0x6C	//Separator key
#define KB_SUBTRACT		0x6D	//Subtract key
#define KB_DECIMAL		0x6E	//Decimal key
#define KB_DIVIDE		0x6F	//Divide key
#define KB_F1			0x70	//F1 key
#define KB_F2			0x71	//F2 key
#define KB_F3			0x72	//F3 key
#define KB_F4			0x73	//F4 key
#define KB_F5			0x74	//F5 key
#define KB_F6			0x75	//F6 key
#define KB_F7			0x76	//F7 key
#define KB_F8			0x77	//F8 key
#define KB_F9			0x78	//F9 key
#define KB_F10			0x79	//F10 key
#define KB_F11			0x7A	//F11 key
#define KB_F12			0x7B	//F12 key
#define KB_F13			0x7C	//F13 key
#define KB_F14			0x7D	//F14 key
#define KB_F15			0x7E	//F15 key
#define KB_F16			0x7F	//F16 key
#define KB_F17			0x80	//F17 key
#define KB_F18			0x81	//F18 key
#define KB_F19			0x82	//F19 key
#define KB_F20			0x83	//F20 key
#define KB_F21			0x84	//F21 key
#define KB_F22			0x85	//F22 key
#define KB_F23			0x86	//F23 key
#define KB_F24			0x87	//F24 key
#define KB_NUMLOCK		0x90	//NUM LOCK key
#define KB_SCROLL		0x91	//SCROLL LOCK key
#define KB_LSHIFT		0xA0	//Left SHIFT
#define KB_RSHIFT		0xA1	//Right SHIFT
#define KB_LCONTROL		0xA2	//Left CTRL
#define KB_RCONTROL		0xA3	//Right CTRL
#define KB_LMENU		0xA4	//Left ALT
#define KB_RMENU		0xA5	//Right ALT


///----------------------------------------------------------------------------------------------------

typedef enum
{
	BUTTON_MENU_MOD,
	BUTTON_CHAT_MOD,

	BUTTON_UP,
	BUTTON_DOWN,
	BUTTON_LEFT,
	BUTTON_RIGHT,
	BUTTON_PGUP,
	BUTTON_PGDN,
	BUTTON_AFTERBURN,
	
	BUTTON_BULLET,
	BUTTON_BOMB,
	BUTTON_MINE,
	
	BUTTON_WARP,
	BUTTON_STEALTH,
	BUTTON_CLOAK,
	BUTTON_XRADAR,
	BUTTON_ANTIWARP,
	BUTTON_MULTI,

	BUTTON_BURST,
	BUTTON_REPEL,
	BUTTON_ROCKET,
	BUTTON_DECOY,
	BUTTON_BRICK,
	BUTTON_PORTAL

} Button;


#define CB_BUTTONPRESSED "buttonpressed"
typedef void (*ButtonPressedFunc)(Button key, bool down);

#define I_KEYBOARD "keyboard-1"

typedef struct Ikeyboard
{
	INTERFACE_HEAD_DECL;

	char* (*KeyboardState)(void);
	bool (*updatekeyboard)(int key, bool down, bool save);
	
} Ikeyboard;


#endif

