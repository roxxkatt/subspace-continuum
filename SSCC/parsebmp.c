
#include <stdio.h>

#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;

unsigned char* ParseBMP(char* filename, int *bw, int *bh)
{
	short unsigned int offset=0;
	short unsigned int width=0;
	short unsigned int height=0;

	FILE *file;
	file=fopen(filename,"r");
	if(!file)
	{
		lm->Log(L_INFO,"<bmp> No file found at '%s'",filename);
		return NULL;
	}
	
	if((fgetc(file) != 66) && (fgetc(file) != 77))
	{
		lm->Log(L_INFO,"<bmp> File at '%s' is not a BMP",filename);
		return NULL;
	}
	
	BMPFH bmpfh;
	fseek(file,0L,SEEK_SET);
	fread(&bmpfh,sizeof(BMPFH),1,file);
	
	offset=bmpfh.offset;
	width=bmpfh.width;
	height=bmpfh.height;
	
	//bmp stores last row to first row, left to right
	int pixelsize=3*sizeof(char);
	int outpixelsize=4*sizeof(char);
	int rowsize=width*pixelsize;
	int newrowsize=width*outpixelsize;
	int alignment=4;
	int padding=(alignment-(rowsize % alignment)) % alignment;
	int paddedrowsize=rowsize+padding;
	unsigned char *colordata=(unsigned char*)amalloc(height*newrowsize);
	// fseek(file,(long)(offset),SEEK_SET);
// lm->Log(L_INFO,"iseek=%i=%x",ftell(file),ftell(file));
	int y=0;
	int x=0;
	int n=0;
	for(y=height-1; y >= 0; y--)
	{
		int currow=height-1-y;
		int curpos=ftell(file);
// if(n <= width*3) lm->Log(L_INFO,"seek1=%i=%x",ftell(file),ftell(file));
		fseek(file,(long)(offset+currow*paddedrowsize),SEEK_SET);
// if(n <= width*3) lm->Log(L_INFO,"seek2=%i=%x",ftell(file),ftell(file));
		for(x=0; x < width; x++)
		{
			if(ferror(file))
			{
				perror("wtf: ");
				clearerr(file);
			}
			clearerr(file); //why the fuck do i even need this
			
			BGRPixel p;
			fread(&p,sizeof(BGRPixel),1,file);
			// int b=fgetc(file);
			// int g=fgetc(file);
			// int r=fgetc(file);
			int a=0xFF;
			if(!p.r && !p.g && !p.b) a=0;
			n++;
			colordata[y*newrowsize+x*outpixelsize+0]=p.r;
			colordata[y*newrowsize+x*outpixelsize+1]=p.g;
			colordata[y*newrowsize+x*outpixelsize+2]=p.b;
			colordata[y*newrowsize+x*outpixelsize+3]=a;
		}
		/*
		int curpos=ftell(file);
		int padding=(32-(curpos % 32)) % 32;
if(n <= width*3) lm->Log(L_INFO,"seek1=%i=%x",ftell(file),ftell(file));
		// fseek(file,(long)(offset+currow*paddedrowsize),SEEK_SET);
		fseek(file,(long)(padding),SEEK_CUR);
if(n <= width*3) lm->Log(L_INFO,"seek2=%i=%x",ftell(file),ftell(file));
*/
	}
	
	// fseek(file,0L,SEEK_END);
	unsigned long filesize=ftell(file);
	fclose(file);
	
	lm->Log(L_INFO,"<bmp> Loaded BMP at '%s': %ix%i, %i pixels, %lu bytes read",filename,width,height,n,filesize);
// lm->Log(L_INFO,"bsize=%i pixelsize=%i rowsize=%i offset=%i colordata=%i",
// sizeof(char),pixelsize,rowsize,offset,colordata);
// lm->Log(L_INFO,"padding=%i paddedrowsize=%i",
// padding,paddedrowsize);
	
	*bw=width;
	*bh=height;
	return colordata;
}

local Iparsebmp bmpint=
{
	INTERFACE_HEAD_INIT(I_PARSEBMP,"parsebmp"),
	ParseBMP
};

EXPORT int MM_parsebmp(Imodman *mm2, mod_action action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		if(!lm) return MM_FAIL;
		
		mm->RegInterface(&bmpint);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&bmpint)) return MM_FAIL;
		
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}

