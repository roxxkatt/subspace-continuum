
#ifndef __CMOD_H
#define __CMOD_H

#include "module.h"

#define CFG_CMOD_SEARCH_PATH "localbin:bin:corebin"

void RegCModLoader(Imodman *mm);
void UnregCModLoader(void);

#endif
