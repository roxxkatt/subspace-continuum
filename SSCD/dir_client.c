
#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;
local Inet *net;
local Icore *core;
local Iconfig *cfg;
local Idirdata *dd;

local bool packethandler(NetLoc loc, Byte *pkt, int len, WebInfo* wi)
{
	if(loc != LOC_CLIENT)
	{
		lm->Log(L_INFO,"<dir_client> got client list request on bad loc somehow: %u",loc);
		return false;
	}
	
	if(len < sizeof(C2DListRequest))
	{
		lm->Log(L_INFO,"<dir_client> got client list request on %d but is too small %d < %d",loc,len,sizeof(C2DListRequest));
		return false;
	}
	
	//TODO: more sanity checking
	
	switch(pkt[0])
	{
		case C2D_LISTREQUEST:
		{
			C2DListRequest *lr=(C2DListRequest*)pkt;
			lm->Log(L_INFO,"<dir_client> got client list request: minplayers=%i",lr->minplayers);

			//TODO FIXME: continuum is an asshole and only accepts a giant packet instead of a series of 01 then an 02
			int num=0;
			int extralen=0;
			
			LinkedList *entries=dd->GetList();
			DirEntry* de=NULL;
			FOR_EACH_LINK(*entries,de,l)
			{
				num++;
				extralen+=de->desclen;
			}
			
			int bigsize=sizeof(C2DListData)*num+sizeof(char)*extralen;
			C2DListData* pld=amalloc(bigsize);
			// memset(pld,0,bigsize);
			pld->type=C2D_LISTRESPONSE;
			
			C2DListData* ld=(C2DListData*)((Byte*)pld); //no type byte
			de=NULL;
			FOR_EACH_LINK(*entries,de,ll)
			{
				ld->ip=de->ip;
				ld->port=de->port;
				ld->players=de->players;
				ld->scores=de->scores;
				ld->version=de->version;
				strncpy(ld->name,de->name,64);
				strncpy(ld->desc,de->desc,de->desclen);
				
				int smallsize=sizeof(C2DListData)-1+de->desclen; //no type byte
// printf("big=%u small=%u desc=%u ld=%u %p %p\n",bigsize,smallsize,de->desclen,sizeof(C2DListData),pld,ld);
				ld=(C2DListData*)((Byte*)ld+smallsize); //because pointer math
			}
			
			if(num) net->SendPacketToFrom(wi,loc,pld,bigsize);
			lm->Log(L_DRIVEL,"<dir_client> sent %d bytes for %u entries",bigsize,num);
			afree(pld);
			
			return true;
		}
		break;
	}
	
	return false;
}

EXPORT int MM_dir_client(Imodman *mm2, int action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		net=mm->GetInterface(I_NET);
		core=mm->GetInterface(I_CORE);
		cfg=mm->GetInterface(I_CONFIG);
		dd=mm->GetInterface(I_DIRDATA);
		if(!lm || !net || !core || !cfg || !dd) return MM_FAIL;
		
		int port=cfg->GetInt(GLOBAL,"Listen","ClientPort",4990);
		net->CreateNewSocket(LOC_CLIENT,true);
		net->CreateNewEndpoint(LOC_CLIENT,cfg->GetStr(GLOBAL,"Listen","ClientIP"),port);
		net->ConnectToLocation(LOC_CLIENT);
		core->UseCoreProtocol(LOC_CLIENT);
		
		net->AddPacket(LOC_CLIENT,C2D_LISTREQUEST,packethandler);
		
		lm->Log(L_INFO,"<dir_client> Listening for clients on port %d",port);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		core->StopUsingCoreProtocol(LOC_CLIENT);
		
		net->RemovePacket(LOC_CLIENT,C2D_LISTREQUEST,packethandler);
		
		mm->ReleaseInterface(dd);
		mm->ReleaseInterface(cfg);
		mm->ReleaseInterface(core);
		mm->ReleaseInterface(net);
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}
