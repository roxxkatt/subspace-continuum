
#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;

local LinkedList entries=LL_INITIALIZER;

local DirEntry* FindEntry(char* name)
{
	DirEntry* de=NULL;
	FOR_EACH_LINK(entries,de,l)
	{
		if(!strcmp(name,de->name)) return de;
	}
	
	return NULL;
}

local void AddEntry(DirEntry* de)
{
	DirEntry* de2=amalloc(sizeof(DirEntry));
	memcpy(de2,de,sizeof(DirEntry));
	
	LLAddLast(&entries,de2);
}

local void UpdateEntry(DirEntry* de)
{
	DirEntry* de2=FindEntry(de->name);
	if(!de2) AddEntry(de);
	else
	{
		memcpy(de2,de,sizeof(DirEntry)); //FIXME HACK: i am lazy
	}
}

local LinkedList* GetList(void)
{
	return &entries;
}

local void SaveData(void)
{
	// lm->Log(L_INFO,"<dataman> saving: %s\n",rs->name);
	// DO_CBS(CB_DATAFUNC,DataFunc,(void));
}

local Idirdata dataint=
{
	INTERFACE_HEAD_INIT(I_DIRDATA,"dataman"),
	// SaveData,
	FindEntry, UpdateEntry, GetList
};

EXPORT ModReturn MM_dirdata(Imodman *mm2, ModAction action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		if(!lm) return MM_FAIL;
		
		mm->RegInterface(&dataint);
		
		// mm->RegCallback(CB_DATAFUNC,SaveData);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&dataint)) return MM_FAIL;
		
		// mm->UnregCallback(CB_DATAFUNC,SaveData);
		
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	
	return MM_FAIL;
}

