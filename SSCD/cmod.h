
#ifndef __CMOD_H
#define __CMOD_H

#define CFG_CMOD_SEARCH_PATH "localbin:bin:corebin:core"

void RegCModLoader(Imodman *mm);
void UnregCModLoader(void);

#endif
