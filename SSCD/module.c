
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <assert.h>

#include "defs.h"
#include "module.h"

typedef struct ModuleData
{
	mod_args_t args;
	ModuleLoaderFunc loader;
	char loadername[16];
} ModuleData;

local HashTable *arenacallbacks, *globalcallbacks;
local HashTable *arenaints, *globalints, *intsbyname;
local HashTable *arenaadvs, *globaladvs;
local HashTable *loaders;
local LinkedList mods;
local int nomoremods;

local pthread_mutex_t modmtx;
local pthread_mutex_t intmtx=PTHREAD_MUTEX_INITIALIZER;
local pthread_mutex_t cbmtx=PTHREAD_MUTEX_INITIALIZER;
local pthread_mutex_t advmtx=PTHREAD_MUTEX_INITIALIZER;

local int LoadModule_(char *);
local int UnloadModule(char *);
local void EnumModules(void (*)(char *, char *, void *), void *);
local char *GetModuleInfo(char *);
local char *GetModuleLoader(char *);

local void RegInterface(void *iface);
local int UnregInterface(void *iface);
local void *GetInterface(char *id);
local void *GetInterfaceByName(char *name);
local void ReleaseInterface(void *iface);

local void RegCallback(char *, void *);
local void UnregCallback(char *, void *);
local void LookupCallback(char *, LinkedList *);
local void FreeLookupResult(LinkedList *);

local void RegModuleLoader(char *sig, ModuleLoaderFunc func);
local void UnregModuleLoader(char *sig, ModuleLoaderFunc func);

local void DoStage(int);
local void UnloadAllModules(void);
local void NoMoreModules(void);

local Imodman mmint=
{
	INTERFACE_HEAD_INIT(I_MODMAN,"modman"),
	LoadModule_, UnloadModule, EnumModules,
	RegInterface, UnregInterface, GetInterface, GetInterfaceByName, ReleaseInterface,
	RegCallback, UnregCallback, LookupCallback, FreeLookupResult,
	RegModuleLoader, UnregModuleLoader,
	GetModuleInfo, GetModuleLoader,
	DoStage, UnloadAllModules, NoMoreModules
};

Imodman* InitModuleManager(void)
{
	pthread_mutexattr_t attr;

	pthread_mutexattr_init(&attr);
	pthread_mutexattr_settype(&attr,PTHREAD_MUTEX_RECURSIVE);
	pthread_mutex_init(&modmtx,&attr);
	pthread_mutexattr_destroy(&attr);

	LLInit(&mods);
	arenacallbacks=HashAlloc();
	globalcallbacks=HashAlloc();
	arenaints=HashAlloc();
	globalints=HashAlloc();
	intsbyname=HashAlloc();
	arenaadvs=HashAlloc();
	globaladvs=HashAlloc();
	loaders=HashAlloc();
	mmint.head.refcount=1;
	nomoremods=0;
	
	RegInterface(&mmint);
	
	return &mmint;
}

void DeInitModuleManager(Imodman *mm)
{
	if(LLGetHead(&mods))
		fprintf(stderr, "All modules not unloaded!!!\n");
	if(mm && mm->head.refcount > 1)
		fprintf(stderr, "There are remaining references to the module manager!!!\n");
	LLEmpty(&mods);
	HashFree(arenacallbacks);
	HashFree(globalcallbacks);
	HashFree(arenaints);
	HashFree(globalints);
	HashFree(intsbyname);
	HashFree(arenaadvs);
	HashFree(globaladvs);
	HashFree(loaders);
	pthread_mutex_destroy(&modmtx);
}

local void RegModuleLoader(char *sig, ModuleLoaderFunc func)
{
	pthread_mutex_lock(&modmtx);
	HashAdd(loaders,sig,func);
	pthread_mutex_unlock(&modmtx);
}

local void UnregModuleLoader(char *sig, ModuleLoaderFunc func)
{
	pthread_mutex_lock(&modmtx);
	HashRemove(loaders, sig, func);
	pthread_mutex_unlock(&modmtx);
}

/* call with modmtx held */
local ModuleData* get_module_by_name(char *name)
{
	Link *l;
	for(l=LLGetHead(&mods); l; l=l->next)
	{
		ModuleData *mod=(ModuleData*)(l->data);
		if(!strcasecmp(mod->args.name,name)) return mod;
	}
	return NULL;
}

local int LoadModule_(char *spec) //LoadModule in winbase.h, keep _
{
	int ret=MM_FAIL;
	ModuleData *mod;
	char loadername[32]="c";
	ModuleLoaderFunc loader;
	char *t=spec;

	while(*t && isspace(*t)) t++;

	if(*t == '<')
	{
		t=delimcpy(loadername, t+1, sizeof(loadername), '>');
		if(!t)
		{
			fprintf(stderr, "E <module> Bad module specifier: '%s'\n", spec);
			return MM_FAIL;
		}
		while(*t && isspace(*t)) t++;
	}

	pthread_mutex_lock(&modmtx);

	/* check if already loaded */
	if(get_module_by_name(t))
	{
		fprintf(stderr, "E <module> Tried to load '%s' twice\n", t);
		return MM_FAIL;
	}

	loader=HashGetOne(loaders, loadername);
	if(loader)
	{
		mod=amalloc(sizeof(*mod));
		ret=loader(MM_LOAD, &mod->args, t);
		if(ret == MM_OK)
		{
			astrncpy(mod->loadername, loadername, sizeof(mod->loadername));
			mod->loader=loader;
			LLAddLast(&mods, mod);
		}
		else afree(mod);
	}
	else fprintf(stderr, "E <module> Can't find module loader for signature <%s>\n", loadername);

	pthread_mutex_unlock(&modmtx);

	return ret;
}

/* call with modmtx held */
local int unload_by_ptr(ModuleData *mod)
{
	Link *l, *next;
	int ret;

	ret=mod->loader(MM_UNLOAD, &mod->args, NULL);
	if(ret == MM_OK)
	{
		/* now unload it */
		LLRemove(&mods, mod);
		afree(mod);
	}

	return ret;
}

int UnloadModule(char *name)
{
	int ret=MM_FAIL;
	ModuleData *mod;

	pthread_mutex_lock(&modmtx);
	mod=get_module_by_name(name);
	if(mod) ret=unload_by_ptr(mod);
	pthread_mutex_unlock(&modmtx);

	return ret;
}

/* call with modmtx held */
local void recursive_unload(Link *l)
{
	if(l)
	{
		ModuleData *mod=l->data;
		recursive_unload(l->next);
		if(unload_by_ptr(mod) == MM_FAIL)
			fprintf(stderr, "E <module> Error unloading module %s\n",
					mod->args.name);
	}
}

void DoStage(int stage)
{
	Link *l;
	pthread_mutex_lock(&modmtx);
	for(l=LLGetHead(&mods); l; l=l->next)
	{
		ModuleData *mod=l->data;
		mod->loader(stage, &mod->args, NULL);
	}
	pthread_mutex_unlock(&modmtx);
}

void UnloadAllModules(void)
{
	pthread_mutex_lock(&modmtx);
	recursive_unload(LLGetHead(&mods));
	LLEmpty(&mods);
	pthread_mutex_unlock(&modmtx);
}

void NoMoreModules(void)
{
	nomoremods=1;
}

void EnumModules(void (*func)(char *, char *, void *), void *clos)
{
	Link *l;
	pthread_mutex_lock(&modmtx);
	/* this returns modules in load order*/
		for(l=LLGetHead(&mods); l; l=l->next)
		{
			ModuleData *mod=l->data;
			func(mod->args.name, mod->args.info, clos);
		}
	pthread_mutex_unlock(&modmtx);
}

char* GetModuleInfo(char *name)
{
	ModuleData *mod;
	char *ret=NULL;
	pthread_mutex_lock(&modmtx);
	mod=get_module_by_name(name);
	if(mod) ret=mod->args.info;
	pthread_mutex_unlock(&modmtx);
	return ret;
}

char* GetModuleLoader(char *name)
{
	ModuleData *mod;
	char *ret=NULL;
	pthread_mutex_lock(&modmtx);
	mod=get_module_by_name(name);
	if(mod) ret=mod->loadername;
	pthread_mutex_unlock(&modmtx);
	return ret;
}

void RegInterface(void *iface)
{
	char *id;
	InterfaceHead *head=(InterfaceHead*)iface;

	assert(iface);
	assert(head->magic == MODMAN_MAGIC);

	id=head->iid;

	pthread_mutex_lock(&intmtx);

	/* use HashAddFront so that newer interfaces override older ones.
	 * slightly evil, relying on implementation details of hash tables. */
	HashAddFront(intsbyname, head->name, iface);
	HashAddFront(globalints, id, iface);

	pthread_mutex_unlock(&intmtx);
}

int UnregInterface(void *iface)
{
	char *id;
	InterfaceHead *head=(InterfaceHead*)iface;

	assert(head->magic == MODMAN_MAGIC);

	id=head->iid;

	if(head->refcount > 0) return head->refcount;

	pthread_mutex_lock(&intmtx);

	HashRemove(intsbyname, head->name, iface);
	HashRemove(globalints, id, iface);

	pthread_mutex_unlock(&intmtx);

	return 0;
}

void* GetInterface(char *id)
{
	InterfaceHead *head;

	pthread_mutex_lock(&intmtx);
	head=HashGetOne(globalints,id);
	pthread_mutex_unlock(&intmtx);
	if(head) head->refcount++;
	
	return head;
}

void* GetInterfaceByName(char *name)
{
	InterfaceHead *head;
	pthread_mutex_lock(&intmtx);
	head=HashGetOne(intsbyname, name);
	pthread_mutex_unlock(&intmtx);
	if(head) head->refcount++;
	return head;
}

void ReleaseInterface(void *iface)
{
	InterfaceHead *head=(InterfaceHead*)iface;
	if(!iface) return;
	assert(head->magic == MODMAN_MAGIC);
	head->refcount--;
}

void RegCallback(char *id, void *f)
{
	pthread_mutex_lock(&cbmtx);
	HashAdd(globalcallbacks, id, f);
	pthread_mutex_unlock(&cbmtx);
}

void UnregCallback(char *id, void *f)
{
	pthread_mutex_lock(&cbmtx);
	HashRemove(globalcallbacks, id, f);
	pthread_mutex_unlock(&cbmtx);
}

void LookupCallback(char *id, LinkedList *ll)
{
	LLInit(ll);
	pthread_mutex_lock(&cbmtx);
	/* first get global ones */
	HashGetAppend(globalcallbacks, id, ll);
	pthread_mutex_unlock(&cbmtx);
}

void FreeLookupResult(LinkedList *lst)
{
	LLEmpty(lst);
}
