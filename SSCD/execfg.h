
#ifndef __EXECFG_H
#define __EXECFG_H

#define EXE_NAME		"SubSpace Continuum Directory"
#define EXE_VERSION		"1.0.2"
#define L_EXE			'D'

//activate verbose logging for troubleshooting
//useful to prevent log spam
// #define MAKE_NET_PRINT_PACKETS
// #define MAKE_NET_PRINT_EXTRA_INFO
// #define MAKE_CORE_PRINT_PACKETS
// #define MAKE_SQL_LOG_QUERIES

#endif
