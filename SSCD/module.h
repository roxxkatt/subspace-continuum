
#ifndef __MODMAN_H
#define __MODMAN_H

#include "util.h"

#define MAX_ID_LEN 128
#define INTERFACE_HEAD_DECL InterfaceHead head
#define INTERFACE_HEAD_INIT(iid,name) { MODMAN_MAGIC, iid, name, 0, 0 }
#define MODMAN_MAGIC 0x46692017

typedef enum
{
	MM_LOAD			=0,
	MM_POSTLOAD		=1,
	MM_PREUNLOAD	=2,
	MM_UNLOAD		=3,
	MM_ATTACH		=4,
	MM_DETACH		=5
} ModAction;

typedef enum
{
	MM_OK			=0,
	MM_FAIL			=1
} ModReturn;

typedef struct InterfaceHead
{
	unsigned long magic;
	char *iid;
	char *name;
	int reserved1;
	int refcount;
} InterfaceHead;

typedef struct mod_args_t
{
	char name[32];
	char *info;
	void *privdata;
} mod_args_t;

typedef ModReturn (*ModuleLoaderFunc)(ModAction action, mod_args_t *args, char *line);

#define I_MODMAN "modman-1"

typedef struct Imodman
{
	INTERFACE_HEAD_DECL;
	
	int (*LoadModule)(char *specifier);
	int (*UnloadModule)(char *name);

	void (*EnumModules)(void (*func)(char *name, char *info, void *clos), void *clos);
	
	void (*RegInterface)(void *iface);
	int (*UnregInterface)(void *iface);


	void* (*GetInterface)(char *id);
	void* (*GetInterfaceByName)(char *name);
	void (*ReleaseInterface)(void *iface);


	void (*RegCallback)(char *id, void *func);
	void (*UnregCallback)(char *id, void *func);

	void (*LookupCallback)(char *id, LinkedList *res);
	void (*FreeLookupResult)(LinkedList *res);


	void (*RegModuleLoader)(char *signature, ModuleLoaderFunc func);
	void (*UnregModuleLoader)(char *signature, ModuleLoaderFunc func);

	char *(*GetModuleInfo)(char *modname);
	char *(*GetModuleLoader)(char *modname);

	
	
	void (*DoStage)(int stage);
	void (*UnloadAllModules)(void);
	void (*NoMoreModules)(void);
} Imodman;


Imodman* InitModuleManager(void);
void DeInitModuleManager(Imodman *mm);



#define DO_CBS(cb, type, args)									\
{																\
	LinkedList _a_lst;											\
	Link *_a_l;													\
	mm->LookupCallback(cb, &_a_lst);							\
	for(_a_l = LLGetHead(&_a_lst); _a_l; _a_l = _a_l->next)		\
		((type)_a_l->data) args ;								\
	mm->FreeLookupResult(&_a_lst);								\
}

#define DO_CBS_RET(cb, type, args)								\
	bool _cbs_ret = false;										\
	LinkedList _a_lst;											\
	Link *_a_l;													\
	mm->LookupCallback(cb, &_a_lst);							\
	for(_a_l = LLGetHead(&_a_lst); _a_l; _a_l = _a_l->next)		\
	{															\
		if( (((type)_a_l->data) args) ) 						\
		{														\
			_cbs_ret = true;									\
			break;												\
		}														\
	}															\
	mm->FreeLookupResult(&_a_lst);								\

#endif

