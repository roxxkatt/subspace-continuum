
#ifndef __DEFS_H
#define __DEFS_H


#define COMPILE_DATE __DATE__
#define COMPILE_TIME __TIME__
//__FILE__ __func__ __LINE__ are also handy
//not so handy: #error "abcdef" and #warning "abcdef"


//OS detection
#define PLATFORM_WINDOWS	1
#define PLATFORM_MAC		2
#define PLATFORM_UNIX		3
#define PLATFORM_ANDROID	4

#if (defined _WIN32 || defined WIN32)
#define PLATFORM PLATFORM_WINDOWS
#define PLATFORMNAME "Windows"
#elif defined __APPLE__
#define PLATFORM PLATFORM_MAC
#define PLATFORMNAME "Mac"
#elif defined __ANDROID__
#define PLATFORM PLATFORM_ANDROID
#define PLATFORMNAME "Android"
#else
#define PLATFORM PLATFORM_UNIX
#define PLATFORMNAME "Linux"
#endif
// __linux__
// __unix__

//usage:
//#if PLATFORM == PLATFORM_WINDOWS


//architecture detection
#define ARCHITECTURE_32BIT	32
#define ARCHITECTURE_64BIT	64
#if !(defined __LP64__ || defined __LLP64__) || ((defined _WIN32 || defined WIN32) && !defined _WIN64)
#define ARCHITECTURE ARCHITECTURE_32BIT
#define ARCHITECTURENAME "32bit"
#else
#define ARCHITECTURE ARCHITECTURE_64BIT
#define ARCHITECTURENAME "64bit"
#endif

//usage:
//#if ARCHITECTURE == ARCHITECTURE_64BIT


//commonly used standard headers
#include <stdarg.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>
#include <time.h>
#include <string.h>
#include <limits.h>
#include <malloc.h>

#if PLATFORM == PLATFORM_WINDOWS

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <windef.h>
#include <wincon.h>
#include <winsock.h>
#include <direct.h>
#include <io.h>

#include "pthread_win32.h"

#define EXPORT __declspec(dllexport)

#define dlopen(a,b) ((a) ? LoadLibrary(a) : GetModuleHandle(NULL))
#define dlsym(a,b) ((void*)GetProcAddress(a,b))
#define dlclose(a) FreeLibrary(a)

#ifndef PATH_MAX
#define PATH_MAX 4096
#endif
#ifndef NAME_MAX
#define NAME_MAX 256
#endif

#define usleep(x) Sleep((x)/1000)
#define sleep(x) Sleep((x)*1000)

#else // not PLATFORM_WINDOWS

#include <unistd.h>
#include <fcntl.h>
#include <sys/io.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <netinet/in.h>
#include <arpa/inet.h>
//#include <signal.h>
#include <errno.h>
#include <dlfcn.h>
#include <pthread.h>

#define EXPORT

typedef int SOCKET;
#define INVALID_SOCKET (-1)

#endif


//set up malloc stuff
#undef ATTR_FORMAT
#undef ATTR_MALLOC
#undef ATTR_UNUSED
#if defined(__GNUC__) && __GNUC__ >= 3
#define ATTR_FORMAT(type, idx, first) __attribute__ ((format (type, idx, first)))
#define ATTR_MALLOC() __attribute__ ((malloc))
#define ATTR_UNUSED() __attribute__ ((unused))
#else
#define ATTR_FORMAT(type, idx, first)
#define ATTR_MALLOC()
#define ATTR_UNUSED()
#endif


#if(__STDC_VERSION__ >= 199901L)
//try c99 if we can
#include <stdint.h>
#elif defined(__FreeBSD__)
//someone said this works on freebsd
#include <sys/types.h>
#elif defined(__GNUC__)
//this might work on recent gcc too
#include <stdint.h>
#elif defined(_MSC_VER)
//on windows try ms visual c
typedef __int8 int8_t;
typedef __int16 int16_t;
typedef __int32 int32_t;
typedef __int64 int64_t;
typedef unsigned __int8 uint8_t;
typedef unsigned __int16 uint16_t;
typedef unsigned __int32 uint32_t;
typedef unsigned __int64 uint64_t;
#else
//this might work on other 32 bit platforms
typedef signed char int8_t;
typedef signed short int16_t;
typedef signed int int32_t;
typedef signed long long int64_t;
typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long long uint64_t;
#endif
//fallbacks
typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;


//custom types
typedef unsigned char Byte;
typedef unsigned int Ticks;
typedef size_t Size;


/** an alias to use to keep stuff local to modules */
#define local static

//useful stuff for lazy typing
#define null NULL
// #define byte Byte
// #define bytes Byte
#undef BOOL
#undef TRUE
#undef FALSE
#define BOOL bool
#define TRUE true
#define FALSE false


//force safe version usage - i want to make this work
// #define free(x) afree(x)
// #define malloc(x) amalloc(x)
// #define atoi(x) aatoi(x)
// #define strcpy(x,y) strncpy(x,y,sizeof(y))

//secretly use strlcpy or sprintf instead for bytes copied returned instead of completely worthless pointer
// #define strncpy(x,y,z) strlcpy(x,y,z)

#endif

