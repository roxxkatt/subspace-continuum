
#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;
local Imainloop *ml;
local Isql *sql;
local Iconfig *cfg;
local Idirdata *dd;

local int writeplayercounts;

local void initres(int status, db_res* res, void* vp)
{
	if(status)
	{
		//TODO FIXME: retry again after reconnect, and replace now existing entries
		lm->Log(L_DRIVEL,"<data_sql> error getting initial server list from database: %u",status);
		return;
	}
	
	int rows=sql->GetRowCount(res);
	int cols=sql->GetFieldCount(res);
	
	int num=0;
	int i=0;
	for(i=0; i < rows; i++)
	{
		DirEntry de;
		DirEntry* dep=&de;
		memset(dep,0,sizeof(DirEntry));
		
		db_row* row=sql->GetRow(res);
		
		strncpy(dep->name,sql->GetField(row,0),32);
		strncpy(dep->password,sql->GetField(row,1),16);
		dep->ip=aatoi(sql->GetField(row,2));
		dep->port=aatoi(sql->GetField(row,3));
		dep->players=0;
		dep->scores=aatoi(sql->GetField(row,4));
		dep->version=aatoi(sql->GetField(row,5));
		dep->desclen=strlen(sql->GetField(row,6));
		strncpy(dep->desc,sql->GetField(row,6),dep->desclen);
		
		dd->UpdateEntry(dep);
		num++;
	}
	lm->Log(L_INFO,"<data_sql> got initial %u entries from database",num);
}

local void dontcare(int status, db_res* res, void* vp)
{
}

local void ZonePlayerCount(DirEntry* de)
{
	if(writeplayercounts)
		sql->Query(dontcare,de,true,"INSERT INTO PlayerCounts SET Name=?, Players=#",de->name,de->players);
}

local int autosave(void* vp)
{
	int num=0;
	LinkedList *entries=dd->GetList();
	DirEntry* de=NULL;
	FOR_EACH_LINK(*entries,de,l)
	{
		sql->Query(dontcare,de,true,"CALL PushZone(?,?,#,#,#,#,?)",
			de->name,de->password,de->ip,de->port,de->scores,de->version,de->desc);
		num++;
	}
	lm->Log(L_INFO,"<data_sql> saved %u entries to database",num);
	
	return true;
}

EXPORT ModReturn MM_data_sql(Imodman *mm2, ModAction action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		ml=mm->GetInterface(I_MAINLOOP);
		sql=mm->GetInterface(I_SQL);
		cfg=mm->GetInterface(I_CONFIG);
		dd=mm->GetInterface(I_DIRDATA);
		if(!lm || !ml || !sql || !cfg || !dd) return MM_FAIL;
		
		mm->RegCallback(CB_ZONEPLAYERCOUNT,ZonePlayerCount);
		
		writeplayercounts=cfg->GetInt(GLOBAL,"SQL","WritePlayerCounts",1);
		
		int savedelaymins=cfg->GetInt(GLOBAL,"SQL","WriteDelay",60);
		int savedelay=100*60*savedelaymins;
		ml->SetTimer(autosave,savedelay,savedelay,NULL,NULL);
		
		int ZoneDecay=cfg->GetInt(GLOBAL,"SQL","ZoneDecay",30);
		sql->Query(initres,NULL,true,"SELECT Name, Password, IP, Port, Scores, Version, Description FROM Zones WHERE DATEDIFF(NOW(),LastSeen) < #",ZoneDecay);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		ml->ClearTimer(autosave,NULL);
		
		mm->UnregCallback(CB_ZONEPLAYERCOUNT,ZonePlayerCount);
		
		mm->ReleaseInterface(dd);
		mm->ReleaseInterface(cfg);
		mm->ReleaseInterface(sql);
		mm->ReleaseInterface(ml);
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}

