
#ifndef __DATAMAN_H
#define __DATAMAN_H

//386 current value in ASSS
#define MAXDESCLEN 386
typedef struct DirEntry
{
	u32 ip;
	u16 port;
	u16 players;
	u16 scores;
	u32 version;
	char name[32];
	char password[16];
	char desc[MAXDESCLEN];
	
	int desclen;
	bool verified;
} DirEntry;

//for saving player count updates
#define CB_ZONEPLAYERCOUNT "ZonePlayerCount"
typedef void (*ZonePlayerCountFunc)(DirEntry* de);

#define CB_DATAFUNC "datafunc"
typedef void (*DataFunc)(void);

#define I_DIRDATA "dirdata-1"

typedef struct Idirdata
{
	INTERFACE_HEAD_DECL;

	// bool (*GetStatus)(void); //true if connected, false if not

	// void (*SaveData)(void); //triggers master save data callback, currently noop
	//is push or pull better?
	//logman uses push
	//but prob best to pull atm since dbs slower than files, etc
	
	DirEntry* (*FindEntry)(char* name);
	void (*UpdateEntry)(DirEntry* de);
	LinkedList* (*GetList)(void);
	//TODO FIXME: is passing raw list wise?
	//TODO: actually call data from data modules, ex: select from mysql
	//currently just reports info gotten since start
	//can add selectors, like date since last seen, instead of just min players
	
	// void (*Ready)(bool status); //for data modules to report their status


} Idirdata;

#endif
