
#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;
local Inet *net;
local Icore *core;
local Iconfig *cfg;
local Idirdata *dd;

local bool UnhandledPacket(NetLoc loc, void *pkt, int len, WebInfo* wi)
{
	if(loc != LOC_SERVER) return;
	
	if(len < sizeof(S2DRegisterServer))
	{
		lm->Log(L_MALICIOUS,"<dir_server> packet recieved on %d but is too small %d < %d",loc,len,sizeof(S2DRegisterServer));
		return false;
	}
	
	S2DRegisterServer *rs=(S2DRegisterServer*)pkt;
	lm->Log(L_DRIVEL,"<dir_server> got server update from %s",rs->name);
	
	int desclen=len-sizeof(S2DRegisterServer);
	if(desclen > MAXDESCLEN) desclen=MAXDESCLEN;
	
	DirEntry de;
	DirEntry* dep=&de;
	memset(dep,0,sizeof(DirEntry));
	dep->desclen=desclen;
	// dep->ip=rs->ip;
	dep->ip=wi->sin_addr.s_addr; //TODO FIXME: protocol only sends zero ip atm
	dep->port=rs->port;
	dep->players=rs->players;
	dep->scores=rs->scores;
	dep->version=rs->version;
	strncpy(dep->name,rs->name,32);
	strncpy(dep->password,rs->password,16);
	strncpy(dep->desc,rs->desc,desclen);
	
	DirEntry* check=dd->FindEntry(dep->name);
	if(check && stricmp(dep->password,check->password))
	{
		lm->Log(L_MALICIOUS,"<dir_server> %s failed password check",dep->name);
		return true;
	}
	
	dd->UpdateEntry(dep);
	
	{ DO_CBS(CB_ZONEPLAYERCOUNT,ZonePlayerCountFunc,(dep)); }
	
	return true;
}

EXPORT int MM_dir_server(Imodman *mm2, int action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		net=mm->GetInterface(I_NET);
		core=mm->GetInterface(I_CORE);
		cfg=mm->GetInterface(I_CONFIG);
		dd=mm->GetInterface(I_DIRDATA);
		if(!lm || !net || !core || !cfg || !dd) return MM_FAIL;
		
		int port=cfg->GetInt(GLOBAL,"Listen","ServerPort",4991);
		net->CreateNewSocket(LOC_SERVER,true);
		net->CreateNewEndpoint(LOC_SERVER,cfg->GetStr(GLOBAL,"Listen","ServerIP"),port);
		net->ConnectToLocation(LOC_SERVER);
		// core->UseCoreProtocol(LOC_SERVER); //TODO FIXME: protocol is dumb as fuck atm
		
		mm->RegCallback(CB_UNHANDLEDPACKET,UnhandledPacket);
		
		lm->Log(L_INFO,"<dir_server> Listening for zones on port %d",port);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		core->StopUsingCoreProtocol(LOC_SERVER);
		
		mm->UnregCallback(CB_UNHANDLEDPACKET,UnhandledPacket);
		
		mm->ReleaseInterface(dd);
		mm->ReleaseInterface(cfg);
		mm->ReleaseInterface(core);
		mm->ReleaseInterface(net);
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}
