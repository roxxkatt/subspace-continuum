
#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;

#define ID_REUSE_DELAY 10

local int dummykey;
local int magickey;

local pthread_mutex_t lockmtx;
#define LOCK() pthread_mutex_lock(&lockmtx)
#define UNLOCK() pthread_mutex_unlock(&lockmtx)

local struct
{
	Score *s;
	int next;
	time_t available;
} *idmap;

local int firstfreeid;
local int idmapsize;
local int perunitspace;
local pthread_mutexattr_t recmtxattr;

local LinkedList blocks;
local pthread_mutex_t blockmtx;
struct block
{
	int start;
	int len;
};

local Iscores scdint;
#define scd (&scdint)

local void Lock(void)
{
	LOCK();
}

local void Unlock(void)
{
	UNLOCK();
}

local Score* alloc_score(void)
{
	Score *s=amalloc(sizeof(*s)+perunitspace);
	*(unsigned*)GETDATA(s,magickey)=MODMAN_MAGIC;
	return s;
}

local Score* NewScore(void)
{
	LOCK();
	time_t now=time(NULL);

	//find a free id that's available
	int *ptr=&firstfreeid;
	int id=firstfreeid;
	while(id != -1 && idmap[id].available > now)
	{
		ptr=&idmap[id].next;
		id=idmap[id].next;
	}

	if(id == -1)
	{
		//no available ids
		int newsize=idmapsize*2;
		idmap=arealloc(idmap,newsize*sizeof(idmap[0]));
		for(id=idmapsize; id < newsize; id++)
		{
			idmap[id].s=NULL;
			idmap[id].next=id+1;
			idmap[id].available=0;
		}
		idmap[newsize-1].next=firstfreeid;
		firstfreeid=idmapsize;
		ptr=&firstfreeid;
		id=firstfreeid;
		idmapsize=newsize;
	}

	*ptr=idmap[id].next;

	Score *s=idmap[id].s;
	if(!s) s=idmap[id].s=alloc_score();

	s->id=id;
	LLAddLast(&scd->scorelist,s);
	UNLOCK();

	DO_CBS(CB_SCOREPOINTER,ScorePointerFunc,(s,TRUE));

	return s;
}

local Size AllocateScoreData(Size bytes)
{
	struct block *b, *nb;
	int current=0;

	//round up to next multiple of word size
	bytes=(bytes+(sizeof(int)-1)) & (~(sizeof(int)-1));

	pthread_mutex_lock(&blockmtx);

	//first try before between two blocks (or at the beginning)
	Link *link;
	Link *last=NULL;
	for(link=LLGetHead(&blocks); link; link=link->next)
	{
		b=link->data;
		if((b->start - current) >= (int)bytes) goto found;
		else current=b->start + b->len;
		last=link;
	}

	//if we couldn't get in between two blocks, try at the end
	if((perunitspace-current) >= (int)bytes) goto found;

	//no more space
	pthread_mutex_unlock(&blockmtx);
	return -1;

found:
	nb=amalloc(sizeof(*nb));
	nb->start=current;
	nb->len=bytes;
	//if last is NULL, this will put it in front of the list
	LLInsertAfter(&blocks,last,nb);

	//clear all newly allocated space
	Score *s;
	void *data;
	LOCK();
	FOR_EACH_SCORE_DATA(s,data,current) memset(data,0,bytes);
	UNLOCK();

	pthread_mutex_unlock(&blockmtx);

	return current;
}

local void FreeScore(Score *s)
{
	if(!s) return;
	DO_CBS(CB_PLAYERPOINTER,ScorePointerFunc,(s,FALSE));

	LOCK();
	LLRemove(&scd->scorelist,s);
	idmap[s->id].s=NULL;
	idmap[s->id].available=time(NULL)+ID_REUSE_DELAY;
	idmap[s->id].next=firstfreeid;
	firstfreeid=s->id;
	UNLOCK();

	afree(s);
}

local void FreeScoreData(int key)
{
	Link *l;
	pthread_mutex_lock(&blockmtx);
	for(l=LLGetHead(&blocks); l; l=l->next)
	{
		struct block *b=l->data;
		if(b->start == key)
		{
			LLRemove(&blocks,b);
			afree(b);
			break;
		}
	}
	pthread_mutex_unlock(&blockmtx);
}

local Score* FindScoreByID(int id)
{
	LOCK();
	if(id >= 0 && id < idmapsize)
	{
		Score *s=idmap[id].s;
		UNLOCK();
		return s;
	}
	UNLOCK();
	return NULL;
}

local Score* FindScore(Zone* z, Player *p)
{
	if(!p || !z) return NULL;
	Score *s;
	Link *link;
	LOCK();
	FOR_EACH_SCORE(s)
		if((s->z == z) && (s->p == p))
		{
			UNLOCK();
			return s;
		}
	UNLOCK();
	return NULL;
}

local void ScoreCB(Player* p, Zone* z, PlayerScore* score)
{
	if(!p || !z || !score) return;
	
	Score *s=FindScore(z,p);
	if(!s)
	{
		lm->Log(L_WARN,"<scores> got score for %s from %s but they are not in score list",p->name,p->z->name);
		return;
	}
	
	memcpy(&(s->Score),score,sizeof(PlayerScore));
	s->lastupdatetime=current_ticks();
	
	lm->Log(L_DRIVEL,"<scores> saved score for [%s]: %u/%u (%u,%u,%u)",
		p->name,s->Score.Kills,s->Score.Deaths,
		s->Score.Flags,s->Score.Score,s->Score.FlagScore);
}

local void LoadScoreResponse(Score *olds, bool loaded)
{
	Score *s=FindScore(olds->z,olds->p);
	if(!s)
	{
		{ DO_CBS(CB_SCOREACTION,ScoreActionFunc,(olds,SCA_CREATE)); }
		{ DO_CBS(CB_LOADSCORE,LoadScoreFunc,(olds,LoadScoreResponse)); } //TODO FIXME: loop
	}
	
	//noop, score is now loaded
	
	FreeScore(olds);
}

local void PlayerActionCB(Player *p, PlayerAction action)
{
	if(action == PA_CONNECT)
	{
		//player connecting, look for their score
		if(!FindScore(p->z,p)) //check if loaded
		{
			Score *s=NewScore();
			s->p=p;
			s->z=p->z;
			s->lastupdatetime=current_ticks();
			{ DO_CBS(CB_LOADSCORE,LoadScoreFunc,(s,LoadScoreResponse)); }
		}
	}
}

local Iscores scdint=
{
	INTERFACE_HEAD_INIT(I_SCORES,"scores"),
	NewScore,FreeScore,
	AllocateScoreData,FreeScoreData,
	Lock,Unlock,
	FindScoreByID,FindScore
};

EXPORT char* info_scores=
"Scores Module\n"
"by Cheese\n"
"Allows modules to access score data.";

EXPORT ModReturn MM_scores(Imodman *mm2, ModAction action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		if(!lm) return MM_FAIL;
		
		pthread_mutexattr_init(&recmtxattr);
		pthread_mutexattr_settype(&recmtxattr,PTHREAD_MUTEX_RECURSIVE);
		pthread_mutexattr_settype(&recmtxattr,PTHREAD_MUTEX_RECURSIVE);
		pthread_mutex_init(&lockmtx,&recmtxattr);
		pthread_mutex_init(&blockmtx,NULL);

		idmapsize=256;
		idmap=amalloc(idmapsize*sizeof(idmap[0]));
		int i;
		for(i=0; i < idmapsize; i++)
		{
			idmap[i].s=NULL;
			idmap[i].next=i + 1;
			idmap[i].available=0;
		}
		idmap[idmapsize-1].next=-1;
		firstfreeid=0;

		LLInit(&scd->scorelist);
		LLInit(&blocks);

		Iconfig *cfg=mm->GetInterface(I_CONFIG);
		perunitspace=cfg ? cfg->GetInt(GLOBAL,"General","PerScoreBytes",4000) : 4000;
		mm->ReleaseInterface(cfg);

		dummykey=AllocateScoreData(sizeof(unsigned));
		if(dummykey == -1) return MM_FAIL;
		magickey=AllocateScoreData(sizeof(unsigned));
		if(magickey == -1) return MM_FAIL;

		mm->RegCallback(CB_PLAYERACTION,PlayerActionCB);
		mm->RegCallback(CB_SCORE,ScoreCB);
		
		mm->RegInterface(&scdint);

		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&scdint)) return MM_FAIL;

		mm->UnregCallback(CB_SCORE,ScoreCB);
		mm->UnregCallback(CB_PLAYERACTION,PlayerActionCB);
		
		pthread_mutexattr_destroy(&recmtxattr);
		FreeScoreData(dummykey);
		FreeScoreData(magickey);
		afree(idmap);
		LLEnum(&scd->scorelist,afree);
		LLEmpty(&scd->scorelist);
		
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}
