
#ifndef __EXECFG_H
#define __EXECFG_H

#define EXE_NAME		"SubSpace Continuum Biller"
#define EXE_VERSION		"1.0.1"
#define L_EXE			'B'

//activate verbose logging for troubleshooting
//useful to prevent log spam
// #define MAKE_NET_PRINT_PACKETS
// #define MAKE_NET_PRINT_EXTRA_INFO
// #define MAKE_CORE_PRINT_PACKETS
// #define MAKE_SQL_LOG_QUERIES
#define MAKE_CMDMAN_SHOW_COMMAND_DENIALS
// #define MAKE_BILLING_PRINT_PACKETS
// #define LOG_CHAT_MESSAGES
// #define LOG_PRIVATE_MESSAGES
// #define LOG_SQUAD_MESSAGES
// #define LOG_PERSONAL_MESSAGES

#endif
