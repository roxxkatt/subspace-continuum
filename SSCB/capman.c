
#include "ssc.h"

local Imodman *mm;
local Iplayers *pd;
local Ilogman *lm;
local Iconfig *cfg;

#define DEFAULT "default"

typedef struct
{
	char group[MAXGROUPLEN];
	enum
	{
		src_default,
		src_global,
		src_temp
	} source;
} pdata;

local ConfigHandle groupdef;
local ConfigHandle staff_conf;
local int pdkey;

local void update_group(Player *p, pdata *pdata, int log)
{
	char *g;
	if((g=cfg->GetStr(staff_conf,"Global",p->name)))
	{
		astrncpy(pdata->group,g,MAXGROUPLEN);
		pdata->source=src_global;
		if(log) lm->Log(L_DRIVEL,"<capman> %s assigned to group '%s'",p->name,pdata->group);
	}
	else
	{
		astrncpy(pdata->group,DEFAULT,MAXGROUPLEN);
		pdata->source=src_default;
	}
}

local void paction(Player *p, int action)
{
	pdata *pdata=GETDATA(p,pdkey);
	if(action == PA_CONNECT)
		update_group(p,pdata,TRUE);
	else if(action == PA_DISCONNECT)
		astrncpy(pdata->group,"none",MAXGROUPLEN);
}

local char *GetGroup(Player *p)
{
	return ((pdata*)GETDATA(p,pdkey))->group;
}

local void SetTempGroup(Player *p, char *newgroup)
{
	pdata *pdata=GETDATA(p,pdkey);
	if(newgroup)
	{
		astrncpy(pdata->group,newgroup,MAXGROUPLEN);
		pdata->source=src_temp;
	}
}

local void SetPermGroup(Player *p, char *group, int global, char *info)
{
	pdata *pdata=GETDATA(p,pdkey);

	//first set it for the current session
	astrncpy(pdata->group,group,MAXGROUPLEN);

	if(global)
	{
		//set it permanently
		cfg->SetStr(staff_conf,"Global",p->name,group,info,TRUE);
		pdata->source=src_global;
	}
}

local void RemoveGroup(Player *p, char *info)
{
	pdata *pdata=GETDATA(p,pdkey);

	//in all cases,set current group to default
	astrncpy(pdata->group,DEFAULT,MAXGROUPLEN);

	switch(pdata->source)
	{
		case src_default:
			//player is in the default group already, nothing to do
			break;
		case src_global:
			cfg->SetStr(staff_conf,"Global",p->name,DEFAULT,info,TRUE);
			break;
		case src_temp:
			break;
	}
}

local int CheckGroupPassword(char *group, char *pw)
{
	char *correctpw=cfg->GetStr(staff_conf,"GroupPasswords",group);
	return correctpw ? (strcmp(correctpw,pw) == 0) : 0;
}

local int HasCapability(Player *p, char *cap)
{
	char *group=((pdata*)GETDATA(p,pdkey))->group;
	return (cfg->GetStr(groupdef,group,cap) != NULL);
}

local int HasCapabilityByName(char *name, char *cap)
{
	char *group=cfg->GetStr(staff_conf,"Global",name);
	if(!group) group=DEFAULT;
	return (cfg->GetStr(groupdef,group,cap) != NULL);
}

local int HigherThan(Player *a, Player *b)
{
	char cap[MAXGROUPLEN+16];
	char *bgrp=((pdata*)GETDATA(b,pdkey))->group;
	snprintf(cap,sizeof(cap),"higher_than_%s",bgrp);
	return HasCapability(a,cap);
}

local Icapman capint=
{
	INTERFACE_HEAD_INIT(I_CAPMAN,"capman-groups"),
	HasCapability, HasCapabilityByName, HigherThan
};

local Igroupman grpint=
{
	INTERFACE_HEAD_INIT(I_GROUPMAN,"groupman"),
	GetGroup, SetPermGroup, SetTempGroup, RemoveGroup, CheckGroupPassword
};

EXPORT char* info_capman=
"Capabilities Manager Module\n"
"by Cheese\n"
"Allows modules to determine if a certain entity has a certain permission.";

EXPORT ModReturn MM_capman(Imodman *mm2, ModAction action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		cfg=mm->GetInterface(I_CONFIG);
		pd=mm->GetInterface(I_PLAYERS);
		if(!lm || !cfg || !pd) return MM_FAIL;

		pdkey=pd->AllocatePlayerData(sizeof(pdata));
		if(pdkey == -1) return MM_FAIL;

		mm->RegCallback(CB_PLAYERACTION,paction);

		groupdef=cfg->OpenConfigFile(NULL,"groupdef.conf",NULL,NULL);
		staff_conf=cfg->OpenConfigFile(NULL,"staff.conf",NULL,NULL);

		mm->RegInterface(&capint);
		mm->RegInterface(&grpint);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&capint)) return MM_FAIL;
		if(mm->UnregInterface(&grpint)) return MM_FAIL;
			
		cfg->CloseConfigFile(groupdef);
		cfg->CloseConfigFile(staff_conf);
		
		mm->UnregCallback(CB_PLAYERACTION,paction);
		
		pd->FreePlayerData(pdkey);
		
		mm->ReleaseInterface(pd);
		mm->ReleaseInterface(cfg);
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	
	return MM_FAIL;
}
