
#ifndef __CAPMAN_H
#define __CAPMAN_H

#define I_CAPMAN "capman"

typedef struct Icapman
{
	INTERFACE_HEAD_DECL;

	int (*HasCapability)(Player *p, char *cap);
	int (*HasCapabilityByName)(char *name, char *cap);
	int (*HigherThan)(Player *a, Player *b);
	
} Icapman;

#define I_GROUPMAN "groupman"

typedef struct Igroupman
{
	INTERFACE_HEAD_DECL;
	
	char *(*GetGroup)(Player *p);
	void (*SetPermGroup)(Player *p, char *group, int global, char *info);
	void (*SetTempGroup)(Player *p, char *group);
	void (*RemoveGroup)(Player *p, char *info);
	int (*CheckGroupPassword)(char *group, char *pwd);
	
} Igroupman;

#define MAXGROUPLEN 32

/* some standard capability names */
/** if a player can see mod chat messages */
#define CAP_SEEMODCHAT            "seemodchat"
/** if a player can send mod chat messages */
#define CAP_SENDMODCHAT           "sendmodchat"
/** if a player can see mod chat messages */
#define CAP_SEEHIGHMODCHAT        "seehighmodchat"
/** if a player can send mod chat messages */
#define CAP_SENDHIGHMODCHAT       "sendhighmodchat"
/** if a player can send voice messages */
#define CAP_SOUNDMESSAGES         "sendsoundmessages"
/** if a player can upload files (note that this is separate from
 ** cmd_putfile, and both are required to use ?putfile) */
#define CAP_UPLOADFILE            "uploadfile"
/** if a player can see urgent log messages from all arenas */
#define CAP_SEESYSOPLOGALL        "seesysoplogall"
/** if a player can see urgent log messages from the arena he's in */
#define CAP_SEESYSOPLOGARENA      "seesysoplogarena"
/** if a player can see private arenas (in ?arena, ?listmod, etc.) */
#define CAP_SEEPRIVARENA          "seeprivarena"
/** if a player can see private freqs */
#define CAP_SEEPRIVFREQ           "seeprivfreq"
/** if a player can stay connected despite security checksum failures */
#define CAP_BYPASS_SECURITY       "bypasssecurity"
/** if a client can send object change broadcast packets (as some bots
 ** might want to do) */
#define CAP_BROADCAST_BOT         "broadcastbot"
/** if a client can send arbitrary broadcast packets (shouldn't ever
 ** give this out) */
#define CAP_BROADCAST_ANY         "broadcastany"
/** if a player can avoid showing up in ?spec output */
#define CAP_INVISIBLE_SPECTATOR   "invisiblespectator"
/** if a client can escape chat flood detection */
#define CAP_CANSPAM               "unlimitedchat"
/** if a client can use the settings change packet (note this is
 ** separate from cmd_quickfix/cmd_getsettings and both are required to
 ** use ?quickfix/?getsettings) */
#define CAP_VIEWSETTINGS          "viewsettings"
#define CAP_CHANGESETTINGS        "changesettings"
/** if a player shows up in ?listmod output */
#define CAP_IS_STAFF              "isstaff"
/** if a player can sees all non-group-default players even if they lack isstaff */
#define CAP_SEE_ALL_STAFF         "seeallstaff"
/** if a player always forces a change with setship or setfreq instead of going by the arena freqman */
#define CAP_FORCE_SHIPFREQCHANGE  "forceshipfreqchange"

#endif

