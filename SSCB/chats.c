
#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;
local Iplayers *pd;
local Icmdman *cmd;
local Itext *txt;

#define ID_REUSE_DELAY 10

local int dummykey;
local int magickey;

local pthread_mutex_t lockmtx;
#define LOCK() pthread_mutex_lock(&lockmtx)
#define UNLOCK() pthread_mutex_unlock(&lockmtx)

local struct
{
	Chat *c;
	int next;
	time_t available;
} *idmap;

local int firstfreeid;
local int idmapsize;
local int perunitspace;
local pthread_mutexattr_t recmtxattr;

local LinkedList blocks;
local pthread_mutex_t blockmtx;
struct block
{
	int start;
	int len;
};

local Ichats cdint;
#define cd (&cdint)

local void Lock(void)
{
	LOCK();
}

local void Unlock(void)
{
	UNLOCK();
}

local Chat* alloc_chat(void)
{
	Chat *c=amalloc(sizeof(*c)+perunitspace);
	*(unsigned*)GETDATA(c,magickey)=MODMAN_MAGIC;
	return c;
}

local Chat* NewChat(void)
{
	LOCK();
	time_t now=time(NULL);

	//find a free id that's available
	int *ptr=&firstfreeid;
	int id=firstfreeid;
	while(id != -1 && idmap[id].available > now)
	{
		ptr=&idmap[id].next;
		id=idmap[id].next;
	}

	if(id == -1)
	{
		//no available ids
		int newsize=idmapsize*2;
		idmap=arealloc(idmap,newsize*sizeof(idmap[0]));
		for(id=idmapsize; id < newsize; id++)
		{
			idmap[id].c=NULL;
			idmap[id].next=id+1;
			idmap[id].available=0;
		}
		idmap[newsize-1].next=firstfreeid;
		firstfreeid=idmapsize;
		ptr=&firstfreeid;
		id=firstfreeid;
		idmapsize=newsize;
	}

	*ptr=idmap[id].next;

	Chat *c=idmap[id].c;
	if(!c) c=idmap[id].c=alloc_chat();

	c->id=id;
	LLAddLast(&cd->chatlist,c);
	UNLOCK();

	DO_CBS(CB_CHATPOINTER,ChatPointerFunc,(c,TRUE));

	return c;
}

local Size AllocateChatData(Size bytes)
{
	struct block *b, *nb;
	int current=0;

	//round up to next multiple of word size
	bytes=(bytes+(sizeof(int)-1)) & (~(sizeof(int)-1));

	pthread_mutex_lock(&blockmtx);

	//first try before between two blocks (or at the beginning)
	Link *link;
	Link *last=NULL;
	for(link=LLGetHead(&blocks); link; link=link->next)
	{
		b=link->data;
		if((b->start - current) >= (int)bytes) goto found;
		else current=b->start + b->len;
		last=link;
	}

	//if we couldn't get in between two blocks, try at the end
	if((perunitspace-current) >= (int)bytes) goto found;

	//no more space
	pthread_mutex_unlock(&blockmtx);
	return -1;

found:
	nb=amalloc(sizeof(*nb));
	nb->start=current;
	nb->len=bytes;
	//if last is NULL, this will put it in front of the list
	LLInsertAfter(&blocks,last,nb);

	//clear all newly allocated space
	Chat *c;
	void *data;
	LOCK();
	FOR_EACH_CHAT_DATA(c,data,current) memset(data,0,bytes);
	UNLOCK();

	pthread_mutex_unlock(&blockmtx);

	return current;
}

local void FreeChat(Chat *c)
{
	if(!c) return;
	DO_CBS(CB_PLAYERPOINTER,ChatPointerFunc,(c,FALSE));

	LOCK();
	LLRemove(&cd->chatlist,c);
	idmap[c->id].c=NULL;
	idmap[c->id].available=time(NULL)+ID_REUSE_DELAY;
	idmap[c->id].next=firstfreeid;
	firstfreeid=c->id;
	UNLOCK();

	afree(c);
}

local void FreeChatData(int key)
{
	Link *l;
	pthread_mutex_lock(&blockmtx);
	for(l=LLGetHead(&blocks); l; l=l->next)
	{
		struct block *b=l->data;
		if(b->start == key)
		{
			LLRemove(&blocks,b);
			afree(b);
			break;
		}
	}
	pthread_mutex_unlock(&blockmtx);
}

local Chat* FindChatByID(int id)
{
	LOCK();
	if(id >= 0 && id < idmapsize)
	{
		Chat *c=idmap[id].c;
		UNLOCK();
		return c;
	}
	UNLOCK();
	return NULL;
}

local Chat* FindChatByName(char *name)
{
	if(!name) return NULL;
	Chat *c;
	Link *link;
	LOCK();
	FOR_EACH_CHAT(c)
		if(strcasecmp(c->name,name) == 0)
		{
			UNLOCK();
			return c;
		}
	UNLOCK();
	return NULL;
}

#define MAXCHATS 20
#define CHATLISTLEN CHAT_NAME_LEN*MAXCHATS
typedef struct Pdata
{
	char chatlist[CHATLISTLEN];
} Pdata;
local int pdkey;

local void ChatMsg(Player* sender, char* channel, char* text)
{
	if(!sender || !channel || !text) return;
	
	Pdata *pdata=GETDATA(sender,pdkey);
	char *chats=pdata->chatlist;
	int chnum=aatoi(channel);
	char match[32]; match[0]=0;
	char *next=chats;
	int i;
	for(i=0; next && (i < chnum); i++) next=delimcpy(match,next,sizeof(match),',');

// lm->Log(L_DRIVEL,"<chats> s=%s c=%s t=%s m=%s",sender->name,channel,text,match);
	
	Player *p;
	Pdata *pdata2;
	Link *link;
	pd->Lock();
	FOR_EACH_PLAYER_DATA(p,pdata2,pdkey)
	{
		if(p == sender) continue;
		
		int c=0;
		char match2[32];
		char *chats2=pdata2->chatlist;
		next=chats2;
		while(next)
		{
			next=delimcpy(match2,next,sizeof(match2),',');
			c++;
// lm->Log(L_DRIVEL,"<chats> p=%s c=%u m=%s m2=%s",p->name,c,match,match2);
			if(!strcasecmp(match,match2))
			{
// lm->Log(L_DRIVEL,"<chats> sending to %s after matching %s with %s",p->name,match,match2);
				txt->SendChat(p,c,"%s> %s",sender->name,text);
			}
		}
	}
	pd->Unlock();
	
#ifdef LOG_CHAT_MESSAGES
	lm->Log(L_DRIVEL,"<chats> chat from [%s] on [%s]: %s",sender->name,match,text);
#endif
}

local helptext_t chatcreate_help=
"Args: <name>\n"
"Creates a public persistent chat channel.\n";
local void Cchatcreate(Player *p, char *cmd, char *params)
{
	if(!params || !*params)
	{
		txt->SendCommandResponse(p,"Syntax error, try ?bman chatcreate.");
		return;
	}
	
	if(strlen(params) >= CHAT_NAME_LEN)
	{
		txt->SendCommandResponse(p,"Names must be less than %u letters.",CHAT_NAME_LEN);
		return;
	}
	
	Chat *cc=FindChatByName(params);
	if(cc)
	{
		txt->SendCommandResponse(p,"That chat already exists.");
		return;
	}
	
	//TODO FIXME: load from db, will overwrite offline chats
	
	Chat *c=NewChat();
	strncpy(c->name,params,CHAT_NAME_LEN);
	strncpy(c->owner,p->name,USER_NAME_LEN);
	c->created=current_ticks();
	{ DO_CBS(CB_CHATACTION,ChatActionFunc,(c,CA_CREATE)); }
	
	//TODO: set chat

	txt->SendCommandResponse(p,"Successfully created and joined %s.",c->name);
	lm->Log(L_INFO,"<chats> %s created %s",p->name,c->name);
}

local helptext_t chatpassword_help=
"Args: <name>:<password | none>\n"
"Sets a chat channel password and makes the channel private.\n"
"Setting password to none makes the channel a public channel.\n";
local void Cchatpassword(Player *p, char *cmd, char *params)
{
	if(!params || !*params)
	{
		txt->SendCommandResponse(p,"Syntax error, try ?bman chatpassword.");
		return;
	}
	
	char *origline=strdup(params);
	char *line=origline;
	char *name=NULL;
	char *pw=NULL;

	char *ch=strchr(line,':');
	if(!ch)
	{
		txt->SendCommandResponse(p,"Syntax error, try ?bman chatpassword.");
		return;
	}
	*ch=0;
	line=ch+1;
	if(!*line)
	{
		txt->SendCommandResponse(p,"Syntax error, try ?bman chatpassword.");
		return;
	}
	name=origline;
	pw=line;
	
	if(strlen(name) >= CHAT_NAME_LEN)
	{
		txt->SendCommandResponse(p,"Names must be less than %u letters.",CHAT_NAME_LEN);
		return;
	}
	
	if(strlen(pw) >= CHAT_PW_LEN)
	{
		txt->SendCommandResponse(p,"Passwords must be less than %u letters.",CHAT_PW_LEN);
		return;
	}
	
	//TODO FIXME: load from db
	Chat *c=FindChatByName(name);
	if(!c)
	{
		txt->SendCommandResponse(p,"Your chat is not loaded, try reconnecting.");
		return;
	}
	
	if(!strcasecmp(pw,"none")) pw="";
	int pwlen=strlen(pw);

	strncpy(c->password,pw,CHAT_PW_LEN);
	if(pwlen) txt->SendCommandResponse(p,"You have changed the password of %s to %s",c->name,c->password);
	else txt->SendCommandResponse(p,"You have removed the password of %s",c->name);
	lm->Log(L_INFO,"<chats> %s changed the password of %s to %s",p->name,c->name,c->password);
}

local helptext_t chatgive_help=
"Args: <name>:<user>\n"
"Gives ownership of your chat channel.\n";
local void Cchatgive(Player *p, char *cmd, char *params)
{
	if(!params || !*params)
	{
		txt->SendCommandResponse(p,"Syntax error, try ?bman chatgive.");
		return;
	}
	
	char *origline=strdup(params);
	char *line=origline;
	char *name=NULL;
	char *user=NULL;

	char *ch=strchr(line,':');
	if(!ch)
	{
		txt->SendCommandResponse(p,"Syntax error, try ?bman chatgive.");
		return;
	}
	*ch=0;
	line=ch+1;
	if(!*line)
	{
		txt->SendCommandResponse(p,"Syntax error, try ?bman chatgive.");
		return;
	}
	name=origline;
	user=line;
	
	if(strlen(name) >= CHAT_NAME_LEN)
	{
		txt->SendCommandResponse(p,"Names must be less than %u letters.",CHAT_NAME_LEN);
		return;
	}
	
	if(strlen(params) >= USER_NAME_LEN)
	{
		txt->SendCommandResponse(p,"Userames must be less than %u letters.",USER_NAME_LEN);
		return;
	}
	
	//TODO FIXME: load from db
	Chat *c=FindChatByName(name);
	if(!c)
	{
		txt->SendCommandResponse(p,"Your chat is not loaded, try reconnecting.");
		return;
	}
	
	//TODO FIXME: check player and load from db
    strncpy(c->owner,user,USER_NAME_LEN);
	txt->SendCommandResponse(p,"You have given ownership of %s to %s",c->name,user);
	lm->Log(L_INFO,"<chats> %s gave ownership of %s to %s",p->name,p->name,user);
}

local helptext_t chatjoin_help=
"Args: <name>[:<password>]\n"
"Joins a persistent chat channel.\n";
local void Cchatjoin(Player *p, char *cmd, char *params)
{
	if(!params || !*params)
	{
		txt->SendCommandResponse(p,"Syntax error, try ?bman chatjoin.");
		return;
	}
	
	char *origline=strdup(params);
	char *line=origline;
	char *name=NULL;
	char *pw=NULL;

	char *ch=strchr(line,':');
	if(ch)
	{
		*ch=0;
		line=ch+1;
		if(!*line)
		{
			txt->SendCommandResponse(p,"Syntax error, try ?bman chatjoin.");
			return;
		}
		pw=line;
	}
	name=origline;
	
	if(strlen(params) >= CHAT_NAME_LEN)
	{
		txt->SendCommandResponse(p,"Names must be less than %u letters.",CHAT_NAME_LEN);
		return;
	}
	
	Chat *c=FindChatByName(name);
	if(!c)
	{
		//TODO FIXME: load from db
		txt->SendCommandResponse(p,"That chat does not exist or is not loaded.");
		return;
	}

	if(strcasecmp(c->password,pw) != 0)
	{
		txt->SendCommandResponse(p,"Incorrect password for %s.",c->name);
		return;
	}
	
	//TODO FIXME: addchat()
	//strncpy(p->chat,name,CHAT_NAME_LEN);
	txt->SendCommandResponse(p,"You have successfully joined %s.",c->name);
	lm->Log(L_INFO,"<chats> %s joined %s",p->name,c->name);
	
	afree(origline);
}

local helptext_t chatleave_help=
"Args: <name>\n"
"Leaves a persistent chat channel.\n";
local void Cchatleave(Player *p, char *cmd, char *params)
{
	if(!params || !*params) return;
	/*
	if(strlen(p->chat) > 0)
	{
		txt->SendCommandResponse(p,"You are already in a chat.");
		return;
	}*/
	
	if(strlen(params) >= CHAT_NAME_LEN)
	{
		txt->SendCommandResponse(p,"Names must be less than %u letters.",CHAT_NAME_LEN);
		return;
	}
	
	Chat *c=FindChatByName(params);
	if(!c)
	{
		//TODO FIXME: load from db
		txt->SendCommandResponse(p,"That chat does not exist or is not loaded, wait for an online member.");
		return;
	}

	//TODO FIXME: removechat()
	txt->SendCommandResponse(p,"You have successfully left %s.",c->name);
	lm->Log(L_INFO,"<chats> %s left %s",p->name,c->name);
}

local helptext_t chatkick_help=
"Args: <name>:<user>\n"
"Kicks a user from your chat channel.\n"
"You probably want to change the password first.\n";
local void Cchatkick(Player *p, char *cmd, char *params)
{
	if(!params || !*params) return;
	/*
	if(strlen(p->chat) > 0)
	{
		txt->SendCommandResponse(p,"You are already in a chat.");
		return;
	}*/
	
	char *origline=strdup(params);
	char *line=origline;
	char *name=NULL;
	char *user=NULL;

	char *ch=strchr(line,':');
	if(!ch)
	{
		txt->SendCommandResponse(p,"Syntax error, try ?bman chatkick.");
		return;
	}
	*ch=0;
	line=ch+1;
	if(!*line)
	{
		txt->SendCommandResponse(p,"Syntax error, try ?bman chatkick.");
		return;
	}
	name=origline;
	user=line;
	
	if(strlen(name) >= CHAT_NAME_LEN)
	{
		txt->SendCommandResponse(p,"Names must be less than %u letters.",CHAT_NAME_LEN);
		return;
	}
	
	Chat *c=FindChatByName(name);
	if(!c)
	{
		//TODO FIXME: load from db
		txt->SendCommandResponse(p,"That chat does not exist or is not loaded.");
		return;
	}

	//TODO FIXME: check player and load from db
	
	txt->SendCommandResponse(p,"You have successfully kicked %s from %s.",c->name);
	lm->Log(L_INFO,"<chats> %s kicked %s from %s",p->name,c->name);
	
	afree(origline);
}

local helptext_t chat_help=
"Args: none\n"
"Joins a series of temporary chat channels.\n"
"These chats will be numbered after your persistent chat channels.\n";
local void Cchat(Player *p, char *cmd, char *params)
{
	Pdata *pdata=GETDATA(p,pdkey);
	if(!params || !*params) txt->SendCommandResponse(p,"Chats: %s",pdata->chatlist);
	else
	{
		strncpy(pdata->chatlist,params,CHATLISTLEN);
		txt->SendCommandResponse(p,"Chats set to: %s",params);
	}
}

local Ichats cdint=
{
	INTERFACE_HEAD_INIT(I_CHATS,"chats"),
	NewChat,FreeChat,
	AllocateChatData,FreeChatData,
	Lock,Unlock,
	FindChatByID,FindChatByName
};

EXPORT char* info_chats=
"Chats Module\n"
"by Cheese\n"
"Allows users to send and recieve chat messages.";

EXPORT ModReturn MM_chats(Imodman *mm2, ModAction action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		pd=mm->GetInterface(I_PLAYERS);
		cmd=mm->GetInterface(I_CMDMAN);
		txt=mm->GetInterface(I_TEXT);
		if(!lm || !pd || !cmd || !txt) return MM_FAIL;
		
		pthread_mutexattr_init(&recmtxattr);
		pthread_mutexattr_settype(&recmtxattr,PTHREAD_MUTEX_RECURSIVE);
		pthread_mutexattr_settype(&recmtxattr,PTHREAD_MUTEX_RECURSIVE);
		pthread_mutex_init(&lockmtx,&recmtxattr);
		pthread_mutex_init(&blockmtx,NULL);

		idmapsize=256;
		idmap=amalloc(idmapsize*sizeof(idmap[0]));
		int i;
		for(i=0; i < idmapsize; i++)
		{
			idmap[i].c=NULL;
			idmap[i].next=i + 1;
			idmap[i].available=0;
		}
		idmap[idmapsize-1].next=-1;
		firstfreeid=0;

		LLInit(&cd->chatlist);
		LLInit(&blocks);

		Iconfig *cfg=mm->GetInterface(I_CONFIG);
		perunitspace=cfg ? cfg->GetInt(GLOBAL,"General","PerChatBytes",4000) : 4000;
		mm->ReleaseInterface(cfg);

		dummykey=AllocateChatData(sizeof(unsigned));
		if(dummykey == -1) return MM_FAIL;
		magickey=AllocateChatData(sizeof(unsigned));
		if(magickey == -1) return MM_FAIL;

		pdkey=pd->AllocatePlayerData(sizeof(Pdata));
		if(pdkey == -1) return MM_FAIL;

		// mm->RegCallback(CB_PLAYERACTION,PlayerActionCB);
		mm->RegCallback(CB_CHATMSG,ChatMsg);
		
		cmd->AddCommand("chat",Cchat,chat_help);
		cmd->AddCommand("chatcreate",Cchatcreate,chatcreate_help);
		cmd->AddCommand("chatpassword",Cchatpassword,chatpassword_help);
		cmd->AddCommand("chatgive",Cchatgive,chatgive_help);
		cmd->AddCommand("chatjoin",Cchatjoin,chatjoin_help);
		cmd->AddCommand("chatleave",Cchatleave,chatleave_help);
		cmd->AddCommand("chatkick",Cchatkick,chatkick_help);
		
		mm->RegInterface(&cdint);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&cdint)) return MM_FAIL;

		cmd->RemoveCommand("chat",Cchat);
		cmd->RemoveCommand("chatcreate",Cchatcreate);
		cmd->RemoveCommand("chatpassword",Cchatpassword);
		cmd->RemoveCommand("chatgive",Cchatgive);
		cmd->RemoveCommand("chatjoin",Cchatjoin);
		cmd->RemoveCommand("chatleave",Cchatleave);
		cmd->RemoveCommand("chatkick",Cchatkick);
		
		mm->UnregCallback(CB_CHATMSG,ChatMsg);
		// mm->UnregCallback(CB_PLAYERACTION,PlayerActionCB);
		
		pd->FreePlayerData(pdkey);
		
		pthread_mutexattr_destroy(&recmtxattr);
		FreeChatData(dummykey);
		FreeChatData(magickey);
		afree(idmap);
		LLEnum(&cd->chatlist,afree);
		LLEmpty(&cd->chatlist);
		
		mm->ReleaseInterface(txt);
		mm->ReleaseInterface(cmd);
		mm->ReleaseInterface(pd);
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}
