
#ifndef __PLAYER_H
#define __PLAYER_H

typedef struct Player
{
	int id;
	int db_id;
	
	char name[USER_NAME_LEN];
	char password[USER_PW_LEN];
	char squad[SQUAD_NAME_LEN];
	
	u32 IPAddress;
	// u32 ConnectionID; //TODO FIXME: pid in each zone
	u32 MachineID;
	i32 Timezone;
	u16 ClientVersion;
	
	u8 Banner[BANNER_SIZE];
	// PlayerScore Score;
	Demographics dg;
	
	// int userid;
	bool online;
	// int authenticated;
	Ticks connecttime;
	// Ticks lastdatatime;
	LinkedList connections;
	Zone* z; //last zone connected to for fast access
	bool banned;
	
	Byte extradata[0];
} Player;

typedef enum
{
	PA_CREATE,
	PA_CONNECT,
	PA_DISCONNECT,
	PA_DESTROY
} PlayerAction;

#define CB_PLAYERACTION "playeraction"
typedef void (*PlayerActionFunc)(Player *p, PlayerAction action);

#define CB_LOADPLAYER "loadplayer"
typedef void (*LoadPlayerResponseFunc)(Player *p, int cid, bool loaded);
typedef void (*LoadPlayerFunc)(Player *p, int cid, LoadPlayerResponseFunc f);

#define CB_PLAYERPOINTER "playerpointer"
typedef void (*PlayerPointerFunc)(Player *p, bool isnew);

#define I_PLAYERS "players"

typedef struct Iplayers
{
	INTERFACE_HEAD_DECL;

	Player* (*NewPlayer)(void);
	void (*FreePlayer)(Player *p);
	Size (*AllocatePlayerData)(Size bytes);
	void (*FreePlayerData)(int key);
	void (*Lock)(void);
	void (*Unlock)(void);
	
	Player* (*FindPlayerByID)(int id);
	Player* (*FindPlayerByName)(char *name);
	// Player* (*FindPlayerByCID)(Zone* z, int cid);
	void (*KickPlayer)(Player *p);
	
	// LinkedList waitingforauth;
	LinkedList playerlist;
	
} Iplayers;

#define GETDATA(x,key) ((void*)((x)->extradata+key))

#define FOR_EACH_PLAYER(p) \
	for( \
		link=LLGetHead(&pd->playerlist); \
		link && ((p=link->data, link=link->next) || 1); )

#define FOR_EACH_PLAYER_DATA(p,d,key) \
	for( \
		link=LLGetHead(&pd->playerlist); \
		link && ((p=link->data, \
		          d=GETDATA(p,key), \
		          link=link->next) || 1); )

#endif
