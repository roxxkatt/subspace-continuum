
#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;

local void DemographicsCB(Player* p, Demographics* dg)
{
	if(!p || !dg) return;

	memcpy(&(p->dg),dg,sizeof(Demographics));
	
	lm->Log(L_DRIVEL,"<demographics> saved demographics for [%s]",p->name);
}

EXPORT char* info_demographics=
"Scores Module\n"
"by Cheese\n"
"Handles scores.";

EXPORT ModReturn MM_demographics(Imodman *mm2, ModAction action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		if(!lm) return MM_FAIL;
		
		mm->RegCallback(CB_DEMOGRAPHICS,DemographicsCB);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		mm->UnregCallback(CB_DEMOGRAPHICS,DemographicsCB);
		
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}
