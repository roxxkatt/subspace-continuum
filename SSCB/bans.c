
#include "ssc.h"

#define ID_REUSE_DELAY 10

local Imodman *mm;
local Ilogman *lm;
local Icmdman *cmd;
local Itext *txt;
local Iplayers *pd;

local int dummykey;
local int magickey;

local pthread_mutex_t lockmtx;
#define LOCK() pthread_mutex_lock(&lockmtx)
#define UNLOCK() pthread_mutex_unlock(&lockmtx)

local struct
{
	Ban *b;
	int next;
	time_t available;
} *idmap;

local int firstfreeid;
local int idmapsize;
local int perunitspace;
local pthread_mutexattr_t recmtxattr;

local LinkedList blocks;
local pthread_mutex_t blockmtx;
struct block
{
	int start;
	int len;
};

local Ibans bdint;
#define bd (&bdint)

local void Lock(void)
{
	LOCK();
}

local void Unlock(void)
{
	UNLOCK();
}

local Ban* alloc_ban(void)
{
	Ban *b=amalloc(sizeof(*b)+perunitspace);
	*(unsigned*)GETDATA(b,magickey)=MODMAN_MAGIC;
	return b;
}

local Ban* NewBan(void)
{
	LOCK();
	time_t now=time(NULL);

	//find a free id that's available
	int *ptr=&firstfreeid;
	int id=firstfreeid;
	while(id != -1 && idmap[id].available > now)
	{
		ptr=&idmap[id].next;
		id=idmap[id].next;
	}

	if(id == -1)
	{
		//no available ids
		int newsize=idmapsize*2;
		idmap=arealloc(idmap,newsize*sizeof(idmap[0]));
		for(id=idmapsize; id < newsize; id++)
		{
			idmap[id].b=NULL;
			idmap[id].next=id+1;
			idmap[id].available=0;
		}
		idmap[newsize-1].next=firstfreeid;
		firstfreeid=idmapsize;
		ptr=&firstfreeid;
		id=firstfreeid;
		idmapsize=newsize;
	}

	*ptr=idmap[id].next;

	Ban *b=idmap[id].b;
	if(!b) b=idmap[id].b=alloc_ban();

	b->id=id;
	LLAddLast(&bd->banlist,b);
	UNLOCK();

	DO_CBS(CB_BANPOINTER,BanPointerFunc,(b,TRUE));

	return b;
}

local Size AllocateBanData(Size bytes)
{
	struct block *b, *nb;
	int current=0;

	//round up to next multiple of word size
	bytes=(bytes+(sizeof(int)-1)) & (~(sizeof(int)-1));

	pthread_mutex_lock(&blockmtx);

	//first try before between two blocks (or at the beginning)
	Link *link;
	Link *last=NULL;
	for(link=LLGetHead(&blocks); link; link=link->next)
	{
		b=link->data;
		if((b->start - current) >= (int)bytes) goto found;
		else current=b->start + b->len;
		last=link;
	}

	//if we couldn't get in between two blocks, try at the end
	if((perunitspace-current) >= (int)bytes) goto found;

	//no more space
	pthread_mutex_unlock(&blockmtx);
	return -1;

found:
	nb=amalloc(sizeof(*nb));
	nb->start=current;
	nb->len=bytes;
	//if last is NULL, this will put it in front of the list
	LLInsertAfter(&blocks,last,nb);

	//clear all newly allocated space
	Ban *bb;
	void *data;
	LOCK();
	FOR_EACH_BAN_DATA(bb,data,current) memset(data,0,bytes);
	UNLOCK();

	pthread_mutex_unlock(&blockmtx);

	return current;
}

local void FreeBan(Ban *b)
{
	if(!b) return;
	DO_CBS(CB_PLAYERPOINTER,BanPointerFunc,(b,FALSE));

	LOCK();
	LLRemove(&bd->banlist,b);
	idmap[b->id].b=NULL;
	idmap[b->id].available=time(NULL)+ID_REUSE_DELAY;
	idmap[b->id].next=firstfreeid;
	firstfreeid=b->id;
	UNLOCK();

	afree(b);
}

local void FreeBanData(int key)
{
	Link *l;
	pthread_mutex_lock(&blockmtx);
	for(l=LLGetHead(&blocks); l; l=l->next)
	{
		struct block *b=l->data;
		if(b->start == key)
		{
			LLRemove(&blocks,b);
			afree(b);
			break;
		}
	}
	pthread_mutex_unlock(&blockmtx);
}

local Ban* FindBanByID(int id)
{
	LOCK();
	if(id >= 0 && id < idmapsize)
	{
		Ban *b=idmap[id].b;
		UNLOCK();
		return b;
	}
	UNLOCK();
	return NULL;
}

local Ban* FindBanByName(char *name)
{
	if(!name) return NULL;
	Ban *b;
	Link *link;
	LOCK();
	FOR_EACH_BAN(b)
		if(strcasecmp(b->name,name) == 0)
		{
			UNLOCK();
			return b;
		}
	UNLOCK();
	return NULL;
}

local Ban* FindBanByMachine(u32 MachineID)
{
	if(!MachineID) return NULL;
	Ban *b;
	Link *link;
	LOCK();
	FOR_EACH_BAN(b)
		if(b->MachineID == MachineID)
		{
			UNLOCK();
			return b;
		}
	UNLOCK();
	return NULL;
}

local Ban* FindBanByIP(u32 IPAddress)
{
	if(!IPAddress) return NULL;
	Ban *b;
	Link *link;
	LOCK();
	FOR_EACH_BAN(b)
		if(b->IPAddress == IPAddress)
		{
			UNLOCK();
			return b;
		}
		else if(b->IPStart && b->IPEnd
				&& (IPAddress >= b->IPStart) && (IPAddress <= b->IPEnd))
		{
			UNLOCK();
			return b;
		}
	UNLOCK();
	return NULL;
}

local helptext_t netban_help=
"Args: <colon seperated argument list>\n"
"Arg: <key>=<data>\n"
"n | name: Ban by name\n"
"m | mac | macid: Ban by network card ID\n"
"i | ip: Ban by IP\n"
"rs | start: Ban by IP address range\n"
"re | end: Ban by IP address range\n"
"t | time: Ban duration in hours\n"
"temp: 0=temp 1=perm\n"
"a | active: 0=off 1=on\n"
"c | r | comment | reason: why\n"
"id: Modify existing ban by ID\n"
"Creates or modifies a netban.\n"
"Each constraint makes ban more specific.\n";
local void Cnetban(Player *p, char *cmd, char *params)
{
	if(!params || !*params)
	{
		txt->SendCommandResponse(p,"Syntax error, try ?bman netban.");
		return;
	}
	
	char name[USER_NAME_LEN];
	amemzero(name,sizeof(name));
	int mac=0;
	// int rs=0;
	// int re=0;
	int t=0;
	int temporary=0;
	int active=0;
	int id=0;
	char comment[250];
	
// #define _ip sin_addr.s_addr
// #define INET_ADDRSTRLEN 16
// #if PLATFORM == PLATFORM_WINDOWS
// #define inet_pton InetPton
// #endif

	u32 IPAddress;
	u32 MachineID;
	u32 IPStart;
	u32 IPEnd;
	// struct sockaddr_in ip;
	// struct sockaddr_in rs;
	// struct sockaddr_in re;
	// amemzero(&ip,sizeof(ip));
	// amemzero(&rs,sizeof(rs));
	// amemzero(&re,sizeof(re));
	// char rsstr[INET_ADDRSTRLEN];
	// char restr[INET_ADDRSTRLEN];
	// inet_pton(AF_INET,"192.0.2.33",&(sa.sin_addr));
	// inet_ntop(AF_INET,&(sa.sin_addr),str,INET_ADDRSTRLEN);
	
	int c=0;
	char phrase[250];
	char *next=params;
	while(next)
	{
		next=delimcpy(phrase,next,sizeof(phrase),':');
		
		char *word1=phrase;
		char *word2=NULL;
		char *ch=strchr(phrase,'=');
		if(!ch)
		{
			txt->SendCommandResponse(p,"Syntax error near %s, try ?bman netban.",word1);
			return;
		}
		*ch=0;
		word2=ch+1;
		if(!*word2)
		{
			txt->SendCommandResponse(p,"Syntax error near %s, try ?bman netban.",word1);
			return;
		}
		
		if(!strcasecmp(word1,"n") || !strcasecmp(word1,"name"))
		{
			if(strlen(word2) >= USER_NAME_LEN)
			{
				txt->SendCommandResponse(p,"Names must be less than %u letters.",USER_NAME_LEN);
				return;
			}
			
			Player *p2=pd->FindPlayerByName(word2);
			if(!p2)
			{
				//TODO FIXME: load from db
				txt->SendCommandResponse(p,"%s is offline or does not exist.",word2);
				return;
			}
			
			strncpy(name,word2,USER_NAME_LEN);
		}
		else if(!strcasecmp(word1,"m") || !strcasecmp(word1,"mac") || !strcasecmp(word1,"macid"))
		{
			MachineID=aatoi(word2);
		}
		else if(!strcasecmp(word1,"i") || !strcasecmp(word1,"ip"))
		{
			IPAddress=aatoi(word2);
		}
		else if(!strcasecmp(word1,"rs") || !strcasecmp(word1,"start"))
		{
			IPStart=aatoi(word2);
		}
		else if(!strcasecmp(word1,"re") || !strcasecmp(word1,"end"))
		{
			IPEnd=aatoi(word2);
		}
		else if(!strcasecmp(word1,"t") || !strcasecmp(word1,"time"))
		{
			t=aatoi(word2);
		}
		else if(!strcasecmp(word1,"a") || !strcasecmp(word1,"active"))
		{
			if(aatoi(word2)) active=TRUE;
		}
		else if(!strcasecmp(word1,"temp") || !strcasecmp(word1,"temporary"))
		{
			if(aatoi(word2)) temporary=TRUE;
		}
		else if(!strcasecmp(word1,"id"))
		{
			id=aatoi(word2);
		}
		else if(!strcasecmp(word1,"c") || !strcasecmp(word1,"comment")
				|| !strcasecmp(word1,"r") || !strcasecmp(word1,"reason"))
		{
			strncpy(comment,word2,sizeof(comment));
		}
		else txt->SendCommandResponse(p,"Unrecognized identifier: %s",word1);
		
		c++;
	}

	if(!c)
	{
		txt->SendCommandResponse(p,"Syntax error, try ?bman netban.");
		return;
	}
	
	if((IPStart > IPEnd) ||(IPEnd < IPStart))
	{
		txt->SendCommandResponse(p,"IP range must be logical. (%u vs %u)",IPStart,IPEnd);
		return;
	}
	
	//TODO FIXME: load from db
	Ban *b2=FindBanByName(name);
	if(b2 && b2->active)
	{
		txt->SendCommandResponse(p,"A ban for %s already exists.");
		return;
	}
	
	if(id)
	{
		Ban *b3=FindBanByID(id);
		if(!b3)
		{
			txt->SendCommandResponse(p,"ID #%u does not exist.",id);
			return;
		}
	}
	
	Ban *b=NewBan();
	strncpy(b->name,name,USER_NAME_LEN);
	strncpy(b->creator,p->name,USER_NAME_LEN);
	b->IPAddress=IPAddress;
	b->IPStart=IPStart;
	b->IPEnd=IPEnd;
	b->MachineID=MachineID;
	b->active=active;
	b->temporary=temporary;
	strncpy(b->reason,comment,sizeof(b->reason));
	b->created=current_ticks();
	{ DO_CBS(CB_BANACTION,BanActionFunc,(b,BA_CREATE)); }

	txt->SendCommandResponse(p,"Successfully created netban.");
	lm->Log(L_INFO,"<bans> %s created netban #%u",p->name,b->id);
}

local Ibans bdint=
{
	INTERFACE_HEAD_INIT(I_BANS,"bans"),
	NewBan,FreeBan,
	AllocateBanData,FreeBanData,
	Lock,Unlock,
	FindBanByID,FindBanByName,FindBanByMachine,FindBanByIP
};

EXPORT char* info_bans=
"Bans Module\n"
"by Cheese\n"
"Allows modules to access ban data.";

EXPORT ModReturn MM_bans(Imodman *mm2, ModAction action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		pd=mm->GetInterface(I_PLAYERS);
		txt=mm->GetInterface(I_TEXT);
		cmd=mm->GetInterface(I_CMDMAN);
		if(!lm || !pd || !txt || !cmd) return MM_FAIL;

		pthread_mutexattr_init(&recmtxattr);
		pthread_mutexattr_settype(&recmtxattr,PTHREAD_MUTEX_RECURSIVE);
		pthread_mutexattr_settype(&recmtxattr,PTHREAD_MUTEX_RECURSIVE);
		pthread_mutex_init(&lockmtx,&recmtxattr);
		pthread_mutex_init(&blockmtx,NULL);

		idmapsize=256;
		idmap=amalloc(idmapsize*sizeof(idmap[0]));
		int i;
		for(i=0; i < idmapsize; i++)
		{
			idmap[i].b=NULL;
			idmap[i].next=i + 1;
			idmap[i].available=0;
		}
		idmap[idmapsize-1].next=-1;
		firstfreeid=0;

		LLInit(&bd->banlist);
		LLInit(&blocks);

		Iconfig *cfg=mm->GetInterface(I_CONFIG);
		perunitspace=cfg ? cfg->GetInt(GLOBAL,"General","PerBanBytes",4000) : 4000;
		mm->ReleaseInterface(cfg);

		dummykey=AllocateBanData(sizeof(unsigned));
		if(dummykey == -1) return MM_FAIL;
		magickey=AllocateBanData(sizeof(unsigned));
		if(magickey == -1) return MM_FAIL;

		mm->RegInterface(&bdint);
		
		cmd->AddCommand("netban",Cnetban,netban_help);

		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&bdint)) return MM_FAIL;

		cmd->RemoveCommand("netban",Cnetban);
		
		pthread_mutexattr_destroy(&recmtxattr);
		FreeBanData(dummykey);
		FreeBanData(magickey);
		afree(idmap);
		LLEnum(&bd->banlist,afree);
		LLEmpty(&bd->banlist);
		
		mm->ReleaseInterface(cmd);
		mm->ReleaseInterface(txt);
		mm->ReleaseInterface(pd);
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	
	return MM_FAIL;
}
