
#ifndef __SSC_H
#define __SSC_H

//include order is sensitive due to dependencies

#include "execfg.h"
#include "defs.h"

#include "util.h"
#include "app.h"
#include "module.h"
#include "cmod.h"

#include "packets.h"
#include "net.h"
#include "encrypt.h"
#include "proto_core.h"

#include "mainloop.h"
#include "logman.h"
#include "config.h"
#include "sql.h"

#include "zones.h"
#include "players.h"
#include "bans.h"
#include "scores.h"

#include "capman.h"
#include "cmdman.h"
#include "text.h"
#include "billing.h"

#include "squads.h"
#include "chats.h"

#endif
