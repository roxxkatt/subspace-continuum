
#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;
local Iconfig *cfg;
local Icapman *cm;
local Itext *txt;

typedef struct cmddata_t
{
	CommandFunc func;
	helptext_t helptext;
} cmddata_t;

local pthread_mutex_t cmdmtx;
local HashTable *cmds;
local HashTable *dontlog_table;
local CommandFunc defaultfunc;

void AddCommand(char *cmd, CommandFunc f, helptext_t helptext)
{
// lm->Log(L_INFO,"<cmd> ac [%s]",cmd);
	if(!cmd) defaultfunc=f;
	else
	{
		cmddata_t *data=amalloc(sizeof(*data));
		data->func=f;
		data->helptext=helptext;
		pthread_mutex_lock(&cmdmtx);
		HashAdd(cmds,cmd,data);
		pthread_mutex_unlock(&cmdmtx);
	}
}

void RemoveCommand(char *cmd, CommandFunc f)
{
	if(!cmd)
	{
		if(defaultfunc == f) defaultfunc=NULL;
	}
	else
	{
		LinkedList lst=LL_INITIALIZER;
		pthread_mutex_lock(&cmdmtx);
		HashGetAppend(cmds,cmd,&lst);
		Link *l;
		for(l=LLGetHead(&lst); l; l=l->next)
		{
			cmddata_t *data=l->data;
			if(data->func == f)
			{
				HashRemove(cmds,cmd,data);
				pthread_mutex_unlock(&cmdmtx);
				LLEmpty(&lst);
				afree(data);
				return;
			}
		}
		pthread_mutex_unlock(&cmdmtx);
		LLEmpty(&lst);
	}
}

local void init_dontlog()
{
	dontlog_table=HashAlloc();

	/* billing commands that shouldn't be logged */
	// HashAdd(dontlog_table,"chat",(void*)1);
	// HashAdd(dontlog_table,"password",(void*)1);
	// HashAdd(dontlog_table,"squadcreate",(void*)1);
	// HashAdd(dontlog_table,"squadjoin",(void*)1);
	// HashAdd(dontlog_table,"addop",(void*)1);
	// HashAdd(dontlog_table,"adduser",(void*)1);
	// HashAdd(dontlog_table,"changepassword",(void*)1);
	// HashAdd(dontlog_table,"login",(void*)1);
	// HashAdd(dontlog_table,"blogin",(void*)1);
	// HashAdd(dontlog_table,"bpassword",(void*)1);
	// HashAdd(dontlog_table,"squadpassword",(void*)1);
	// HashAdd(dontlog_table,"message",(void*)1);
}

local void uninit_dontlog()
{
	HashFree(dontlog_table);
}

local inline int dontlog(char *cmd)
{
	void *result=HashGetOne(dontlog_table,cmd);
	return result == (void*)1;
}

local void AddUnlogged(char *cmdname)
{
	HashAdd(dontlog_table,cmdname,(void*)1);
}

local void RemoveUnlogged(char *cmdname)
{
	HashRemove(dontlog_table,cmdname,(void*)1);
}

local void log_command(Player *p, char *cmd, char *params)
{
	char t[32];

	if(!lm) return;

	if(dontlog(cmd)) params="..."; //don't log the params to some commands

	lm->Log(L_INFO,"<cmdman> command by %s: %s %s",p->name,cmd,params);
}

local int allowed(Player *p, char *cmd)
{
	char cap[40];

	if(!cm)
	{
		lm->Log(L_WARN,"<cmdman> the capability manager isn't loaded, disallowing all commands");
		return FALSE;
	}

	snprintf(cap,sizeof(cap),"cmd_%s",cmd);
	
	return cm->HasCapability(p,cap);
}

void Command(char *line, Player *p, int sound)
{
	char *origline=line;

	/* find end of command */
	char cmdbuf[40];
	char *t=cmdbuf;
	while(*line && *line != ' ' && *line != '=' && (t-cmdbuf) < 30) *t++=*line++;
	/* close it off and add sound hack */
	*t++=0;
	*t++=(char)sound;
	/* skip spaces */
	while(*line && (*line == ' ' || *line == '=')) line++;
	
	char *cmd=cmdbuf;
	if(*cmd == '?') cmd++;

	LinkedList lst=LL_INITIALIZER;
	pthread_mutex_lock(&cmdmtx);
	HashGetAppend(cmds,cmd,&lst);

// lm->Log(L_INFO,"<cmd> cmd=[%s]",cmd);
	if(LLIsEmpty(&lst))
	{
		//no cmds registered for text, use a registered default if any exist
		if(defaultfunc) defaultfunc(p,cmd,origline);
	}
	else if(allowed(p,cmd))
	{
		Link *l;
		for(l=LLGetHead(&lst); l; l=l->next)
		{
			cmddata_t *data=l->data;
			if(data->func) data->func(p,cmd,line);
		}
		log_command(p,cmd,line);
	}
#ifdef MAKE_CMDMAN_SHOW_COMMAND_DENIALS
	else
	{
		txt->SendCommandResponse(p,"You are not allowed to use ?%s.",cmd);
		lm->Log(L_DRIVEL,"<cmdman> [%s] permission denied for ?%s",p->name,cmd);
	}
#endif

	pthread_mutex_unlock(&cmdmtx);
	LLEmpty(&lst);
}

helptext_t GetHelpText(char *cmd)
{
	helptext_t ret=NULL;
	LinkedList lst=LL_INITIALIZER;
	pthread_mutex_lock(&cmdmtx);
	HashGetAppend(cmds,cmd,&lst);
	Link *l;
	for(l=LLGetHead(&lst); l; l=l->next)
	{
		cmddata_t *cd=l->data;
		ret=cd->helptext;
	}
	pthread_mutex_unlock(&cmdmtx);

	return ret;
}

local Icmdman cmdint=
{
	INTERFACE_HEAD_INIT(I_CMDMAN,"cmdman"),
	AddCommand, RemoveCommand,
	AddUnlogged, RemoveUnlogged,
	Command, GetHelpText
};

EXPORT char* info_cmdman=
"Command Manager Module\n"
"by Cheese\n"
"Allows users to use commands.";

EXPORT ModReturn MM_cmdman(Imodman *mm2, ModAction action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		cfg=mm->GetInterface(I_CONFIG);
		cm=mm->GetInterface(I_CAPMAN);
		txt=mm->GetInterface(I_TEXT);
		if(!lm || !cfg || !cm || !txt) return MM_FAIL;

		pthread_mutexattr_t attr;
		pthread_mutexattr_init(&attr);
		pthread_mutexattr_settype(&attr,PTHREAD_MUTEX_RECURSIVE);
		pthread_mutex_init(&cmdmtx,&attr);
		pthread_mutexattr_destroy(&attr);

		cmds=HashAlloc();
		init_dontlog();
		defaultfunc=NULL;

		mm->RegInterface(&cmdint);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&cmdint)) return MM_FAIL;
		
		uninit_dontlog();
		
		HashFree(cmds);
		pthread_mutex_destroy(&cmdmtx);
		
		mm->ReleaseInterface(txt);
		mm->ReleaseInterface(lm);
		mm->ReleaseInterface(cm);
		mm->ReleaseInterface(cfg);
		
		return MM_OK;
	}
	return MM_FAIL;
}
