
#include "ssc.h"

#define ID_REUSE_DELAY 10

local Imodman *mm;
local int dummykey;
local int magickey;

local pthread_mutex_t lockmtx;
#define LOCK() pthread_mutex_lock(&lockmtx)
#define UNLOCK() pthread_mutex_unlock(&lockmtx)

local struct
{
	Player *p;
	int next;
	time_t available;
} *idmap;

local int firstfreeid;
local int idmapsize;
local int perunitspace;
local pthread_mutexattr_t recmtxattr;

local LinkedList blocks;
local pthread_mutex_t blockmtx;
struct block
{
	int start;
	int len;
};

local Iplayers pdint;
#define pd (&pdint)

local void Lock(void)
{
	LOCK();
}

local void Unlock(void)
{
	UNLOCK();
}

local Player* alloc_player(void)
{
	Player *p=amalloc(sizeof(*p)+perunitspace);
	*(unsigned*)GETDATA(p,magickey)=MODMAN_MAGIC;
	return p;
}

local Player* NewPlayer(void)
{
	LOCK();
	time_t now=time(NULL);

	//find a free id that's available
	int *ptr=&firstfreeid;
	int id=firstfreeid;
	while(id != -1 && idmap[id].available > now)
	{
		ptr=&idmap[id].next;
		id=idmap[id].next;
	}

	if(id == -1)
	{
		//no available ids
		int newsize=idmapsize*2;
		idmap=arealloc(idmap,newsize*sizeof(idmap[0]));
		for(id=idmapsize; id < newsize; id++)
		{
			idmap[id].p=NULL;
			idmap[id].next=id + 1;
			idmap[id].available=0;
		}
		idmap[newsize-1].next=firstfreeid;
		firstfreeid=idmapsize;
		ptr=&firstfreeid;
		id=firstfreeid;
		idmapsize=newsize;
	}

	*ptr=idmap[id].next;

	Player *p=idmap[id].p;
	if(!p) p=idmap[id].p=alloc_player();

	p->id=id;
	p->connecttime=current_ticks();
	LLInit(&(p->connections));
	//do not add to player list until after auth
	UNLOCK();

	DO_CBS(CB_PLAYERPOINTER,PlayerPointerFunc,(p,TRUE));

	return p;
}

local Size AllocatePlayerData(Size bytes)
{
	struct block *b, *nb;
	int current=0;

	//round up to next multiple of word size
	bytes=(bytes+(sizeof(int)-1)) & (~(sizeof(int)-1));

	pthread_mutex_lock(&blockmtx);

	//first try before between two blocks (or at the beginning)
	Link *link;
	Link *last=NULL;
	for(link=LLGetHead(&blocks); link; link=link->next)
	{
		b=link->data;
		if((b->start - current) >= (int)bytes) goto found;
		else current=b->start + b->len;
		last=link;
	}

	//if we couldn't get in between two blocks, try at the end
	if((perunitspace-current) >= (int)bytes) goto found;

	//no more space
	pthread_mutex_unlock(&blockmtx);
	return -1;

found:
	nb=amalloc(sizeof(*nb));
	nb->start=current;
	nb->len=bytes;
	//if last is NULL, this will put it in front of the list
	LLInsertAfter(&blocks,last,nb);

	//clear all newly allocated space
	Player *p;
	void *data;
	LOCK();
	FOR_EACH_PLAYER_DATA(p,data,current) memset(data,0,bytes);
	UNLOCK();

	pthread_mutex_unlock(&blockmtx);

	return current;
}

local void FreePlayer(Player *p)
{
	DO_CBS(CB_PLAYERPOINTER,PlayerPointerFunc,(p,FALSE));

	LOCK();
	//LLRemove(&pd->waitingforauth,p);
	LLRemove(&pd->playerlist,p);
	idmap[p->id].p=NULL;
	idmap[p->id].available=time(NULL)+ID_REUSE_DELAY;
	idmap[p->id].next=firstfreeid;
	firstfreeid=p->id;
	UNLOCK();

	afree(p);
}

local void FreePlayerData(int key)
{
	Link *l;
	pthread_mutex_lock(&blockmtx);
	for(l=LLGetHead(&blocks); l; l=l->next)
	{
		struct block *b=l->data;
		if(b->start == key)
		{
			LLRemove(&blocks,b);
			afree(b);
			break;
		}
	}
	pthread_mutex_unlock(&blockmtx);
}

local Player* FindPlayerByID(int id)
{
	LOCK();
	if(id >= 0 && id < idmapsize)
	{
		Player *p=idmap[id].p;
		UNLOCK();
		return p;
	}
	UNLOCK();
	return NULL;
}

local Player* FindPlayerByName(char *name)
{
	Link *link;
	Player *p;

	LOCK();
	FOR_EACH_PLAYER(p)
		if(strcasecmp(name,p->name) == 0)
		{
			UNLOCK();
			return p;
		}
	UNLOCK();
	return NULL;
}

local void KickPlayer(Player *p)
{
	Lock();
	Unlock();
}

local Iplayers pdint=
{
	INTERFACE_HEAD_INIT(I_PLAYERS,"players"),
	NewPlayer,FreePlayer,
	AllocatePlayerData,FreePlayerData,
	Lock,Unlock,
	FindPlayerByID,FindPlayerByName,
	KickPlayer
};

EXPORT char* info_players=
"Players Module\n"
"by Cheese\n"
"Allows modules to access player data.";

EXPORT ModReturn MM_players(Imodman *mm2, ModAction action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;

		pthread_mutexattr_init(&recmtxattr);
		pthread_mutexattr_settype(&recmtxattr,PTHREAD_MUTEX_RECURSIVE);
		pthread_mutexattr_settype(&recmtxattr,PTHREAD_MUTEX_RECURSIVE);
		pthread_mutex_init(&lockmtx,&recmtxattr);
		pthread_mutex_init(&blockmtx,NULL);

		idmapsize=256;
		idmap=amalloc(idmapsize*sizeof(idmap[0]));
		int i;
		for(i=0; i < idmapsize; i++)
		{
			idmap[i].p=NULL;
			idmap[i].next=i + 1;
			idmap[i].available=0;
		}
		idmap[idmapsize-1].next=-1;
		firstfreeid=0;

		//LLInit(&pd->waitingforauth);
		LLInit(&pd->playerlist);
		LLInit(&blocks);

		Iconfig *cfg=mm->GetInterface(I_CONFIG);
		perunitspace=cfg ? cfg->GetInt(GLOBAL,"General","PerPlayerBytes",4000) : 4000;
		mm->ReleaseInterface(cfg);

		dummykey=AllocatePlayerData(sizeof(unsigned));
		if(dummykey == -1) return MM_FAIL;
		magickey=AllocatePlayerData(sizeof(unsigned));
		if(magickey == -1) return MM_FAIL;

		mm->RegInterface(&pdint);

		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&pdint)) return MM_FAIL;

		pthread_mutexattr_destroy(&recmtxattr);

		FreePlayerData(dummykey);
		FreePlayerData(magickey);

		afree(idmap);
//		LLEnum(&pd->waitingforauth,afree);
//		LLEmpty(&pd->waitingforauth);
		LLEnum(&pd->playerlist,afree);
		LLEmpty(&pd->playerlist);
		
		return MM_OK;
	}
	
	return MM_FAIL;
}
