
#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;
local Iplayers *pd;
local Itext *txt;

local void PrivMsg(int sound, char* pm)
{
	if(!pm) return;
	if(*pm != ':') goto malformed;
	
	char recipient[32];
	char sender[32];
	
	char *text=delimcpy(recipient,pm+1,sizeof(recipient),':');

	if(!text || (*text != '(')) goto malformed;

	char *t=strstr(text+1,")>");
	if(!t || (t - text) > 30) goto malformed;
	
	memset(sender,0,sizeof(sender));
	memcpy(sender,text+1,t-text-1);
	text=t+2;

	Player* p=pd->FindPlayerByName(sender);
	
	if(recipient[0] == '#')
	{
		if(!recipient[1]) goto malformed; //maybe abusable by players, but zone shouldnt send
		
		//if(p) //this way zones can send updates
		{ DO_CBS(CB_PRIVSQUADMSG,PrivSquadMsgFunc,(sender,recipient+1,sound,text)); }
		
		return;
	}
	else
	{
		Player* p2=pd->FindPlayerByName(recipient);
		if(p2)
		{
// lm->Log(L_DRIVEL,"<privs> sending to %s",p2->name);
			txt->SendPrivate(p2,sound,"(%s)> %s",sender,text);
#ifdef LOG_PRIVATE_MESSAGES
			lm->Log(L_DRIVEL,"<privs> priv to [%s]: %s> %s",p2->name,sender,text);
#endif
			return;
		}
		else
		{
			if(p) txt->SendCommandResponse(p,"The user '%s' is offline.",recipient);
			return;
		}
	}
	
malformed:
		lm->Log(L_MALICIOUS,"<privs> malformed private message: %s",pm);
}

EXPORT char* info_privs=
"Private Messages Module\n"
"by Cheese\n"
"Allows users to send and recieve private messages.";

EXPORT ModReturn MM_privs(Imodman *mm2, ModAction action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		pd=mm->GetInterface(I_PLAYERS);
		txt=mm->GetInterface(I_TEXT);
		if(!lm || !pd || !txt) return MM_FAIL;
		
		mm->RegCallback(CB_PRIVMSG,PrivMsg);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		mm->UnregCallback(CB_PRIVMSG,PrivMsg);
		
		mm->ReleaseInterface(txt);
		mm->ReleaseInterface(pd);
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}
