
#include "ssc.h"

local Imodman *mm;
local Imainloop *ml;
local Ilogman *lm;
local Iconfig *cfg;
local Icapman *cm;
local Igroupman *gm;
local Icmdman *cmd;
local Itext *txt;
local Izones *zd;
local Iplayers *pd;

local ticks_t startedat;

local helptext_t buptime_help=
"Args: none\n"
"Displays how long the server has been running.\n";
local void Cbuptime(Player *p, char *command, char *params)
{
	ticks_t secs=TICK_DIFF(current_ticks(),startedat) / 100;
	int days, hours, mins;
	char day_string[20];
	char hour_string[20];
	char min_string[20];
	char sec_string[20];

	days=secs / 86400;
	secs %= 86400;
	hours=secs / 3600;
	secs %= 3600;
	mins=secs / 60;
	secs %= 60;

	day_string[0]=hour_string[0]=min_string[0]=sec_string[0]=0;

	if(days)
	{
		if(days == 1) sprintf(day_string,"1 day,");
		else sprintf(day_string,"%d days,",days);
	}
	if(hours || days)
	{
		if(hours == 1) sprintf(hour_string,"1 hour,");
		else sprintf(hour_string,"%d hours,",hours);
	}
	if(mins || hours || days)
	{
		if(mins == 1) sprintf(min_string,"1 minute and ");
		else sprintf(min_string,"%d minutes and ",mins);
	}
	if(secs || mins || hours || days)
	{
		if(secs == 1) sprintf(sec_string,"1 second.");
		else sprintf(sec_string,"%d seconds.",secs);
	}

	txt->SendCommandResponse(p,"This server has been online for %s%s%s%s",
			day_string,hour_string,min_string,sec_string);
}

local helptext_t bowner_help=
"Args: none\n"
"Displays the billing server owner.\n";
local void Cbowner(Player *p, char *command, char *params)
{
	char *owner=cfg->GetStr(GLOBAL,"Info","Owner");
	
	if(owner) txt->SendCommandResponse(p,"This billing server is owned by %s.",owner);
	else txt->SendCommandResponse(p,"This billing server has no listed owner.");
}

local helptext_t bname_help=
"Args: none\n"
"Displays the name of this billing server.\n";
local void Cbname(Player *p, char *command, char *params)
{
	char *name=cfg->GetStr(GLOBAL,"Info","BillerName");
	
	if(name) txt->SendCommandResponse(p,"Biller Name: %s",name);
	else txt->SendCommandResponse(p,"This billing server has no listed name.");
}

local helptext_t bversion_help=
"Args: none\n"
"Displays the user database version.\n";
local void Cbversion(Player *p, char *command, char *params)
{
	txt->SendCommandResponse(p,"[%s] Version %s, built on %s at %s, running on %s %s.",
		EXE_NAME,EXE_VERSION,COMPILE_DATE,COMPILE_TIME,PLATFORMNAME,ARCHITECTURENAME);
}

local helptext_t find_help=
"Args: <all or part of a user name>\n"
"Tells you where the specified user is right now. If you specify\n"
"only part of a user name, it will try to find a matching name\n"
"using a case insensitive substring search.\n";
local void Cfind(Player *p, char *command, char *params)
{
	Link *link;
	Player *i;
	Player *best=NULL;
	int score=INT_MAX; //lower is better

	pd->Lock();
	FOR_EACH_PLAYER(i)
	{
		if(strcasecmp(i->name,params) == 0)
		{
			best=i; //exact matches always win
			break;
		}
		
		char *pos=strcasestr(i->name,params);
		if(pos)
		{
			//if only a substring match, score based on distance from start of name.
			int newscore=pos - i->name;
			if(newscore < score)
			{
				best=i;
				score=newscore;
			}
		}
	}
	pd->Unlock();

	if(best)
	{
		txt->SendCommandResponse(p,"[%s] is in [%s].",best->name,best->z->name);
	}
	else //not found
	{
		txt->SendCommandResponse(p,"[%s] is currently offline.",params);
	}
}

///----------------------------------------------------------------------------------------------------

local helptext_t bwarn_help=
"Args: <user>:<message>\n"
"Send a red warning message to a user.\n";
local void Cbwarn(Player *p, char *command, char *params)
{
	char buf[250];
	char recipient[32];
	
	char *text=(char*)delimcpy(recipient,params,sizeof(recipient),':');
	Player* p2=pd->FindPlayerByName(recipient);
	if(!p2)
	{
		txt->SendCommandResponse(p,"The user '%s' is offline.",recipient);
		return;
	}
	
	if(cm->HasCapability(p,CAP_IS_STAFF))
		snprintf(buf,sizeof(buf),"WARNING: %s -%s",text,p->name);
	else snprintf(buf,sizeof(buf),"WARNING: %s",text);
	
	txt->SendColorMsgP(p2,CC_RED,buf);
	txt->SendCommandResponse(p,"Player [%s] warned.",p2->name);
}

local void send_msg_cb(char *line, void *clos)
{
	txt->SendCommandResponse((Player*)clos,"  %s",line);
}

local helptext_t breloadconf_help=
"Args: [-f] [path]\n"
"With no args, causes the server to reload any config files that have\n"
"been modifed since they were loaded. With {-f}, forces a reload of all\n"
"open files. With a string,forces a reload of all files whose pathnames\n"
"contain that string.\n";
local void Cbreloadconf(Player *p, char *command, char *params)
{
	if(strcmp(params,"-f") == 0)
	{
		txt->SendCommandResponse(p,"Reloading all config files.");
		cfg->ForceReload(NULL,send_msg_cb,p);
	}
	else if(*params)
	{
		txt->SendCommandResponse(p,"Reloading config files containing '%s':",params);
		cfg->ForceReload(params,send_msg_cb,p);
	}
	else
	{
		txt->SendCommandResponse(p,"Reloading all modified config files.");
		cfg->CheckModifiedFiles();
	}
}

local helptext_t bget_help=
"Args: <section>:<key>\n"
"Displays the value of a global setting.\n"
"Make sure there are no spaces around the colon.\n";
local void Cbget(Player *p, char *command, char *params)
{
	char *res=cfg->GetStr(GLOBAL,params,NULL);
	if(res) txt->SendCommandResponse(p,"%s=%s",params,res);
	else txt->SendCommandResponse(p,"%s not found.",params);
}

local helptext_t bset_help=
"Args: [-t] <section>:<key>=<value>\n"
"Sets the value of a global setting. Make sure there are no spaces around\n"
"either the colon or the equals sign. A {-t} makes the setting temporary.\n";
local void Cbset(Player *p, char *command, char *params)
{
	time_t tm=time(NULL);
	char info[128];
	char key[MAXSECTIONLEN+MAXKEYLEN+2];
	char *k=key;
	
	char *t=params;
	int perm=TRUE;
	int colons=0;

	snprintf(info,sizeof(info),"Set by %s on ",p->name);
	ctime_r(&tm,info+strlen(info));
	RemoveCRLF(info);

	if(strncmp(t,"-t",2) == 0)
	{
		perm=FALSE;
		t += 2;
		while(*t && *t == ' ') t++;
	}

	while(*t && *t != '=' && (*t != ':' || colons != 1) && (k-key) < (MAXSECTIONLEN+MAXKEYLEN))
		if((*k++=*t++) == ':') colons++;
		
	if(*t != '=' && (*t != ':' || colons != 1)) return;
	*k='\0'; /* terminate key */
	t++; /* skip over=or : */

	cfg->SetStr(GLOBAL,key,NULL,t,info,perm);
	txt->SendCommandResponse(p,"Set %s=%s",key,t);
}

local helptext_t bgetgroup_help=
"Args: [user]\n"
"Displays the group of the specified user, or if none specified, your own.\n";
local void Cbgetgroup(Player *p, char *command, char *params)
{
	Player* p2=NULL;
	if(!params || !*params)
	{
		txt->SendCommandResponse(p,"You are in '%s'.",gm->GetGroup(p));
		return;
	}
	else p2=pd->FindPlayerByName(params);
	
	if(p2) txt->SendCommandResponse(p,"[%s] is in '%s'.",p2->name,gm->GetGroup(p2));
	else txt->SendCommandResponse(p,"[%s] is offline.",params);
}

local helptext_t bsetgroup_help=
"Args: [-p] <group:user>\n"
"Assigns the group given as an argument to the target user. The user\n"
"must be in group {default}, or the server will refuse to change their\n"
"group. Additionally, the user giving the command must have the appropriate capability.\n"
"The optional -p means to assign the group permanently. Otherwise, when\n"
"the target user logs out or changes arenas, the group will be lost.\n";
local void Cbsetgroup(Player *p, char *command, char *params)
{
	char *line;
	char *origline;
	char *group;
	char *player;
	int permanent=0;

	if(!params || !*params) return;
	line=strdup(params);
	origline=line;

	while(*line && strchr(line,' '))
	{
		if(!strncmp(line,"permanent",4) || !strncmp(line,"perm",4) || !strncmp(line,"-p",2)) permanent=1;
		line=strchr(line,' ')+1;
	}
	if(!*line) return;

	while(*line && strchr(line,':'))
	{
		char *c=strchr(line,':');
		*c=0;
		group=line;
		line=c+1;
	}
	if(!*line) return;
	
	player=line;
	Player* p2=pd->FindPlayerByName(player);
	if(!p2)
	{
		txt->SendCommandResponse(p,"[%s] is offline.",player);
		return;
	}
	
	//make sure the setter has permissions to set people to this group 
	char cap[MAXGROUPLEN+16];
	snprintf(cap,sizeof(cap),"higher_than_%s",group);
	if(!cm->HasCapability(p,cap))
	{
		txt->SendCommandResponse(p,"You don't have permission to put users in '%s'.",group);
		lm->Log(L_WARN,"<usercmd> %s doesn't have permission to put users in '%s'",p->name,group);
		return;
	}

	//make sure the target isn't in a group already
	if(strcasecmp(gm->GetGroup(p2),"default"))
	{
		txt->SendCommandResponse(p,"[%s] already has a group. You need to use ?brmgroup first.",p2->name);
		lm->Log(L_WARN,"<usercmd> %s tried to put [%s] in '%s', but they are in '%s'",p->name,player,group,gm->GetGroup(p2));
		return;
	}

	if(permanent)
	{
		time_t tm=time(NULL);
		char info[128];

		snprintf(info,sizeof(info),"Set by %s on ",p->name);
		ctime_r(&tm,info+strlen(info));
		RemoveCRLF(info);

		gm->SetPermGroup(p2,group,TRUE,info);
		txt->SendCommandResponse(p,"[%s] is now in '%s'.",player,group);
		txt->SendCommandResponse(p2,"[%s] has assigned you to '%s'.",p->name,group);
	}
	else
	{
		gm->SetTempGroup(p2,group);
		txt->SendCommandResponse(p,"[%s] is now temporarily in '%s'.",player,group);
		txt->SendCommandResponse(p2,"[%s] has temporarily assigned you to '%s'.",p->name,group);
	}
	
	afree(origline);
}

local helptext_t bremovegroup_help=
"Args: <user>\n"
"Removes a group from a user, returning him to group 'default'. If\n"
"the group was assigned for this session only, then it will be removed\n"
"for this session; if it is a global group, it will be removed globally.\n";
local void Cbremovegroup(Player *p, char *command, char *params)
{
	if(!params || !*params) return;

	Player* p2=pd->FindPlayerByName(params);
	if(!p2)
	{
		txt->SendCommandResponse(p,"[%s] is offline.",params);
		return;
	}
	
	char *grp;
	char cap[MAXGROUPLEN+16];
	grp=gm->GetGroup(p2);
	snprintf(cap,sizeof(cap),"higher_than_%s",grp);

	if(!cm->HasCapability(p,cap))
	{
		txt->SendCommandResponse(p,"You don't have permission to remove from '%s'.",grp);
		lm->Log(L_WARN,"<usercmd> %s doesn't have permission to remove from '%s'",grp);
		return;
	}

	txt->SendCommandResponse(p,"[%s] has been removed from '%s'.",p2->name,grp);
	txt->SendCommandResponse(p2,"You have been removed '%s'.",grp);

	time_t tm=time(NULL);
	char info[128];
	snprintf(info,sizeof(info),"Set by %s on ",p->name);
	ctime_r(&tm,info+strlen(info));
	RemoveCRLF(info);

	//groupman keeps track of the source of the group, so we just have to call this.
	gm->RemoveGroup(p2,info);
}

local helptext_t bgrouplogin_help=
"Args: <group> <password>\n"
"Logs you in to the specified group,if the password is correct.\n";
local void Cbgrouplogin(Player *p, char *command, char *params)
{
	char grp[MAXGROUPLEN+1];
	char *pw=delimcpy(grp,params,MAXGROUPLEN,' ');
	if(grp[0] == '\0' || pw == NULL)
		txt->SendCommandResponse(p,"You must specify a group name and password.");
	else if(gm->CheckGroupPassword(grp,pw))
	{
		gm->SetTempGroup(p,grp);
		txt->SendCommandResponse(p,"You are now in '%s'.",grp);
	}
	else txt->SendCommandResponse(p,"Bad password for '%s'.",grp);
}

local helptext_t blistmod_help=
"Args: none\n"
"Lists all staff members online and which group they belong to.\n";
local void Cblistmod(Player *p, char *command, char *params)
{
	char *fmt=": %20s %s";
	txt->SendCommandResponse(p,fmt,"[Name]","[Occupation]");
	
	int seehid=cm->HasCapability(p,CAP_SEEPRIVARENA);
	int seeallstaff=cm->HasCapability(p,CAP_SEE_ALL_STAFF);
	
	int c=0;
	int c2=0;
	Player *i;
	Link *link;
	pd->Lock();
	FOR_EACH_PLAYER(i)
	{
		c++;
		char *grp=gm->GetGroup(i);
		if(!strcmp(grp,"default") && !strcmp(grp,"none")) continue;
		bool show=false;

		if(seeallstaff || cm->HasCapability(i,CAP_IS_STAFF))
		{
			c2++;
			txt->SendCommandResponse(p,fmt,i->name,grp);
		}
	}
	pd->Unlock();
	
	if(!c2) txt->SendCommandResponse(p,fmt,"(none)","(none)");
	
	txt->SendCommandResponse(p,"%u visible staff member%s online, managing %u users.",c2,(c2==1?"":"s"),c);
}

///----------------------------------------------------------------------------------------------------

struct lsmod_ctx
{
	LinkedList names;
	char *substr;
};

local void add_mod(char *name,char *info,void *clos)
{
	struct lsmod_ctx *ctx=clos;
	if((ctx->substr == NULL) || (strcasestr(name,ctx->substr) != NULL)) LLAddLast(&ctx->names,name);
}

local helptext_t blistmodules_help=
"Args: [-s] [text]\n"
"Lists all the modules currently loaded into the server. -s sorts by name.\n"
"Optional text limits module display to those containing the given text.\n";
local void Cblistmodules(Player *p, char *command, char *params)
{
	int sort=FALSE;
	struct lsmod_ctx ctx;
	char word[64];
	char substr[64];
	char *tmp=NULL;

	LLInit(&ctx.names);
	ctx.substr=NULL;

	while(strsplit(params," ",word,sizeof(word),&tmp))
	{
		if(strcmp(word,"-s") == 0) sort=TRUE;
		else
		{
			astrncpy(substr,word,sizeof(substr));
			ctx.substr=substr;
		}
	}

	mm->EnumModules(add_mod,(void*)&ctx);
	if(sort) LLSort(&ctx.names,LLSort_StringCompare);

	txt->SendCommandResponse(p,"Loaded modules:");

	int c=0;
	char buf[250];
	memset(buf,0,sizeof(buf));
	Link *l;
	for(l=LLGetHead(&ctx.names); l; l=l->next)
	{
		strcat(buf," ");
		strcat(buf,(char*)l->data);
		c++;
		
		if(c >= 10)
		{
			strcat(buf,"\n");
			c=0;
		}
	}
	txt->SendCommandResponse(p,buf);

	LLEmpty(&ctx.names);
}

local helptext_t binsertmodule_help=
"Args: <module>\n"
"Loads the specified module into the server.\n";
local void Cbinsertmodule(Player *p, char *command, char *params)
{
	int ret=mm->LoadModule(params);
	if(ret == MM_OK) txt->SendCommandResponse(p,"Loaded %s successfully.",params);
	else txt->SendCommandResponse(p,"Loading %s failed.",params);
}

local helptext_t bremovemodule_help=
"Args: <module>\n"
"Attempts to unload the specified module from the server.\n";
local void Cbremovemodule(Player *p, char *command, char *params)
{
	int ret=mm->UnloadModule(params);
	if(ret == MM_OK) txt->SendCommandResponse(p,"Unloaded %s successfully.",params);
	else txt->SendCommandResponse(p,"Unloading %s failed.",params);
}

///----------------------------------------------------------------------------------------------------

local helptext_t addzone_help=
"Args: <ServerID>:<password>\n"
"Adds a zone to the biller.\n"
"Useful for closed systems.\n";
local void Caddzone(Player *p, char *command, char *params)
{
	char *c=strchr(params,':');
	if(!c)
	{
		txt->SendCommandResponse(p,"Invalid syntax.");
		return;
	}
	*c=0; c++;
	
	//TODO FIXME: load it from db
	Zone* check=zd->FindZoneBySID(aatoi(params));
	if(check)
	{
		txt->SendCommandResponse(p,"The zone [%s:%u] has that ServerID.",check->name,check->ServerID);
		return;
	}
	
	Zone* z=zd->NewZone();
	strncpy(z->password,c,32);
	z->ServerID=aatoi(params);
	
	{ DO_CBS(CB_ZONEACTION,ZoneActionFunc,(z,ZA_CREATE)); }
	
	txt->SendCommandResponse(p,"Created ServerID %u.",z->ServerID);
}

local helptext_t adduser_help=
"Args: <name>:<password>\n"
"Adds a user to the biller.\n"
"Useful for closed systems.\n";
local void Cadduser(Player *p, char *command, char *params)
{
	char *c=strchr(params,':');
	if(!c)
	{
		txt->SendCommandResponse(p,"Invalid syntax.");
		return;
	}
	*c=0; c++;
	
	//TODO FIXME: load it from db
	Player* check=pd->FindPlayerByName(params);
	if(check)
	{
		txt->SendCommandResponse(p,"The user [%s] already exists.",check->name);
		return;
	}
	
	Player *np=pd->NewPlayer();
	strncpy(np->name,params,32);
	strncpy(np->password,c,32);
	
	{ DO_CBS(CB_PLAYERACTION,PlayerActionFunc,(np,PA_CREATE)); }
	
	txt->SendCommandResponse(p,"Created the user [%s].",np->name);
}

///----------------------------------------------------------------------------------------------------

local helptext_t shutdownbiller_help=
"Args: none\n"
"Shuts down the billing server.\n"
"Use with caution.\n";
local void Cshutdownbiller(Player *p, char *command, char *params)
{
	txt->SendColorMsgP(p,CC_RED,"Goodbye :(");
	ml->Quit(EXIT_OK);
}

local helptext_t recyclebiller_help=
"Args: none\n"
"Shuts down the billing server with a restart exit code for a resurrection script.\n";
local void Crecyclebiller(Player *p, char *command, char *params)
{
	txt->SendColorMsgP(p,CC_RED,"***rides off into sunset***");
	ml->Quit(EXIT_RECYCLE);
}

///----------------------------------------------------------------------------------------------------

EXPORT char* info_usercmd=
"User Command Module\n"
"by Cheese\n"
"Registers a large number of user commands.";

EXPORT ModReturn MM_usercmd(Imodman *mm2, ModAction action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		ml=mm->GetInterface(I_MAINLOOP);
		lm=mm->GetInterface(I_LOGMAN);
		cfg=mm->GetInterface(I_CONFIG);
		cm=mm->GetInterface(I_CAPMAN);
		gm=mm->GetInterface(I_GROUPMAN);
		cmd=mm->GetInterface(I_CMDMAN);
		txt=mm->GetInterface(I_TEXT);
		zd=mm->GetInterface(I_ZONES);
		pd=mm->GetInterface(I_PLAYERS);
		if(!ml || !lm || !cfg || !cm || !gm || !cmd || !txt || !zd || !pd) return MM_FAIL;
		
		startedat=current_ticks();

		cmd->AddCommand("recyclebiller",Crecyclebiller,recyclebiller_help);
		cmd->AddCommand("shutdownbiller",Cshutdownbiller,shutdownbiller_help);
		cmd->AddCommand("adduser",Cadduser,adduser_help);
		cmd->AddCommand("addzone",Caddzone,shutdownbiller_help);
		cmd->AddCommand("bremovemodule",Cbremovemodule,bremovemodule_help);
		cmd->AddCommand("binsertmodule",Cbinsertmodule,binsertmodule_help);
		cmd->AddCommand("blistmodules",Cblistmodules,blistmodules_help);
		cmd->AddCommand("blistmod",Cblistmod,blistmod_help);
		cmd->AddCommand("bgrouplogin",Cbgrouplogin,bgrouplogin_help);
		cmd->AddCommand("bremovegroup",Cbremovegroup,bremovegroup_help);
		cmd->AddCommand("bsetgroup",Cbsetgroup,bsetgroup_help);
		cmd->AddCommand("bgetgroup",Cbgetgroup,bgetgroup_help);
		cmd->AddCommand("bset",Cbset,bset_help);
		cmd->AddCommand("bget",Cbget,bget_help);
		cmd->AddCommand("breloadconf",Cbreloadconf,breloadconf_help);
		cmd->AddCommand("bwarn",Cbwarn,bversion_help);
		cmd->AddCommand("find",Cfind,bversion_help);
		cmd->AddCommand("bversion",Cbversion,bversion_help);
		cmd->AddCommand("bname",Cbname,bname_help);
		cmd->AddCommand("bowner",Cbowner,bowner_help);
		cmd->AddCommand("buptime",Cbuptime,buptime_help);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		cmd->RemoveCommand("recyclebiller",Crecyclebiller);
		cmd->RemoveCommand("shutdownbiller",Cshutdownbiller);
		cmd->RemoveCommand("adduser",Cadduser);
		cmd->RemoveCommand("addzone",Caddzone);
		cmd->RemoveCommand("bremovemodule",Cbremovemodule);
		cmd->RemoveCommand("binsertmodule",Cbinsertmodule);
		cmd->RemoveCommand("blistmodules",Cblistmodules);
		cmd->RemoveCommand("blistmod",Cblistmod);
		cmd->RemoveCommand("bgrouplogin",Cbgrouplogin);
		cmd->RemoveCommand("bremovegroup",Cbremovegroup);
		cmd->RemoveCommand("bsetgroup",Cbsetgroup);
		cmd->RemoveCommand("bgetgroup",Cbgetgroup);
		cmd->RemoveCommand("bset",Cbset);
		cmd->RemoveCommand("bget",Cbget);
		cmd->RemoveCommand("breloadconf",Cbreloadconf);
		cmd->RemoveCommand("bwarn",Cbwarn);
		cmd->RemoveCommand("find",Cfind);
		cmd->RemoveCommand("bversion",Cbversion);
		cmd->RemoveCommand("bname",Cbname);
		cmd->RemoveCommand("bowner",Cbowner);
		cmd->RemoveCommand("buptime",Cbuptime);
		
		mm->ReleaseInterface(pd);
		mm->ReleaseInterface(zd);
		mm->ReleaseInterface(txt);
		mm->ReleaseInterface(cmd);
		mm->ReleaseInterface(gm);
		mm->ReleaseInterface(cm);
		mm->ReleaseInterface(cfg);
		mm->ReleaseInterface(lm);
		mm->ReleaseInterface(ml);
		
		return MM_OK;
	}
	return MM_FAIL;
}

