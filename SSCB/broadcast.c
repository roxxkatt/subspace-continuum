
#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;
local Icapman *cm;
local Icmdman *cmd;
local Itext *txt;
local Izones *zd;

local void sendbroadcast(int sound, char* text)
{
	Zone* z;
	Link *link;
	zd->Lock();
	FOR_EACH_ZONE(z)
	{
		if(!z->online) continue;
		
		txt->SendBroadcast(z,sound,text);
	}
	zd->Unlock();
	
	lm->Log(L_INFO,"<broadcast> sent broadcast: %s",text);
}

local helptext_t broadcast_help=
"Args: <text>\n"
"Sends a broadcast to all connected zones.\n";
local void Cbroadcast(Player *p, char *command, char *params)
{
	int canuse=cm->HasCapability(p,"broadcast");
	if(!canuse)
	{
		txt->SendCommandResponse(p,"You do not have permission to send broadcasts.");
		return;
	}
	
	sendbroadcast(0,params);
}

local void Broadcast(Player* p, int sound, char* text)
{
	if(!p || !text) return;

	int canuse=cm->HasCapability(p,"broadcast");
	if(!canuse)
	{
		// txt->SendCommandResponse(p,"You do not have permission to send broadcasts.");
		lm->Log(L_MALICIOUS,"<broadcast> zone sending broadcast for [%s]: %s", p->name,text);
		return;
	}
	
	sendbroadcast(sound,text);
}

EXPORT char* info_broadcast=
"Broadcast Module\n"
"by Cheese\n"
"Handles sending and recieving broadcasts.";

EXPORT ModReturn MM_broadcast(Imodman *mm2, ModAction action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		cm=mm->GetInterface(I_CAPMAN);
		cmd=mm->GetInterface(I_CMDMAN);
		txt=mm->GetInterface(I_TEXT);
		zd=mm->GetInterface(I_ZONES);
		if(!lm || !cm || !cmd || !txt || !zd) return MM_FAIL;
		
		cmd->AddCommand("broadcast",Cbroadcast,broadcast_help);
		
		mm->RegCallback(CB_BROADCAST,Broadcast);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		mm->UnregCallback(CB_BROADCAST,Broadcast);
		
		cmd->RemoveCommand("broadcast",Cbroadcast);
		
		mm->ReleaseInterface(zd);
		mm->ReleaseInterface(txt);
		mm->ReleaseInterface(cmd);
		mm->ReleaseInterface(cm);
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}
