
#ifndef __PACKETS_H
#define __PACKETS_H

///----------------------------------------------------------------------------------------------------
//CORE PACKET TYPES
#define CORE_PACKET				0x00
#define CORE_LOGINREQUEST		0x01
#define CORE_LOGINRESPONSE		0x02
#define CORE_RELHEADER			0x03
#define CORE_RELACK				0x04 //apparently can send 0 to indicate no more data
#define CORE_SYNCREQUEST		0x05
#define CORE_SYNCRESPONSE		0x06
#define CORE_DISCONNECT			0x07
#define CORE_CHUNK				0x08
#define CORE_LASTCHUNK			0x09
#define CORE_STREAM				0x0A
#define CORE_STREAMCANCEL		0x0B
#define CORE_STREAMCANCELACK	0x0C
#define CORE_UNKNOWN			0x0D
#define CORE_CLUSTER			0x0E
#define CORE_UNKNOWN2			0x0F
#define CORE_ALTENCREQUEST		0x10
#define CORE_ALTENCRESPONSE		0x11

///----------------------------------------------------------------------------------------------------
//S2B PACKET TYPES
#define S2B_PING				0x01
#define S2B_SERVER_LOGIN		0x02
#define S2B_SERVER_LOGOFF		0x03
#define S2B_USER_LOGIN			0x04
#define S2B_USER_LOGOFF			0x05
#define S2B_UNKNOWN				0x06
#define S2B_USER_TEXT_PRIV		0x07
#define S2B_USER_DEMOGRAPHICS	0x0D
#define S2B_USER_BANNER			0x10
#define S2B_USER_SCORE			0x11
#define S2B_UNKNOWN2			0x12
#define S2B_USER_COMMAND		0x13
#define S2B_USER_TEXT_CHAT		0x14
#define S2B_SERVER_CAPABILITIES	0x15

///----------------------------------------------------------------------------------------------------
//B2S PACKET TYPES
#define B2S_USER_LOGIN			0x01
#define B2S_USER_TEXT_PRIV		0x03
#define B2S_USER_KICKOUT		0x08
#define B2S_USER_TEXT_CMD		0x09
#define B2S_USER_TEXT_CHAT		0x0A
#define B2S_SCORERESET			0x31
#define B2S_USER_PACKET			0x32
#define B2S_BILLING_IDENTITY	0x33
#define B2S_USER_TEXT_MCHAT		0x34
#define B2S_SERVER_LOGIN		0x41
#define B2S_SERVER_KICKOUT		0x42

///----------------------------------------------------------------------------------------------------

//push current alignment to stack
//set alignment to 1 byte boundary to remove padding
#pragma pack(push,1)

///----------------------------------------------------------------------------------------------------

typedef struct CoreLoginRequest
{
	u8 type; //0x00
	u8 subtype; //0x01
	u32 key;
	u16 protocol; //vie = 0x01, cont = 0x11
} CoreLoginRequest;

typedef struct CoreLoginResponse
{
	u8 type; //0x00
	u8 subtype; //0x02
	u32 key; //negative
} CoreLoginResponse;

typedef struct CoreRelHeader
{
	u8 type; //0x00
	u8 subtype; //0x03
	u32 packetid;
	//payload
} CoreRelHeader;

typedef struct CoreRelAck
{
	u8 type; //0x00
	u8 subtype; //0x04
	u32 packetid;
} CoreRelAck;

typedef struct CoreSyncRequest
{
	u8 type; //0x00
	u8 subtype; //0x05
	u32 localtime;
	u32 packetssent;
	u32 packetsreceived;
} CoreSyncRequest;

typedef struct CoreSyncResponse
{
	u8 type; //0x00
	u8 subtype; //0x06
	u32 elapsedtime; //since last 0x05
	u32 servertime;
} CoreSyncResponse;

typedef struct CoreDisconnect
{
	u8 type; //0x00
	u8 subtype; //0x07
} CoreDisconnect;

typedef struct CoreChunk
{
	u8 type; //0x00
	u8 subtype; //0x08
	u8 data[];
} CoreChunk;

typedef struct CoreChunkEnd
{
	u8 type; //0x00
	u8 subtype; //0x09
	u8 data[];
} CoreChunkEnd;

typedef struct CoreStreamHeader
{
	u8 type; //0x00
	u8 subtype; //0x0A
	u32 totalsize;
	u8 data[];
} CoreStreamHeader;

typedef struct CoreStreamCancel
{
	u8 type; //0x00
	u8 subtype; //0x0B
} CoreStreamCancel;

typedef struct CoreStreamCancelAck
{
	u8 type; //0x00
	u8 subtype; //0x0C
} CoreStreamCancelAck;

typedef struct CoreClusterHeader
{
	u8 type; //0x00
	u8 subtype; //0x0E
	u8 size;
	u8 data[];
} CoreClusterHeader;

typedef struct CoreAltEncRequest
{
	u8 type; //0x00
	u8 subtype; //0x10
	u8 data[10];
} CoreAltEncRequest;

typedef struct CoreAltEncResponse
{
	u8 type; //0x00
	u8 subtype; //0x11
	u8 data[6];
} CoreAltEncResponse;

///----------------------------------------------------------------------------------------------------

#define SERVER_NAME_LEN	126
#define SERVER_PW_LEN	32
#define USER_NAME_LEN	24
#define USER_PW_LEN		32
#define SQUAD_NAME_LEN	24
#define SQUAD_PW_LEN	40
#define CHAT_NAME_LEN	24
#define CHAT_PW_LEN		40
#define TEXT_LEN		250
#define BANNER_SIZE		96

typedef enum
{
	B2S_LOGIN_OK				=0,
	B2S_LOGIN_NEWUSER			=1,
	B2S_LOGIN_INVALIDPW			=2,
	B2S_LOGIN_BANNED			=3,
	B2S_LOGIN_NONEWCONNS		=4,
	B2S_LOGIN_BADUSERNAME		=5,
	B2S_LOGIN_DEMOVERSION		=6,
	B2S_LOGIN_SERVERBUSY		=7,
	B2S_LOGIN_ASKDEMOGRAPHICS	=8
} LoginCode;

typedef struct PlayerScore
{
	u16 Kills;
	u16 Deaths;
	u16 Flags;
	u32 Score;
	u32 FlagScore;
} PlayerScore;

typedef struct Demographics
{
	char RealName[32];
	char Email[64];
	char City[32];
	char State[24];
	char Gender;
	u8 Age;
	u8 Home;
	u8 Work;
	u8 School;
	u32 Processor;
	u16 Unknown1;
	u16 Unknown2;
	char WindowsName[40];
	char WindowsOrg[40];
	char Display[40];
	char Monitor[40];
	char Modem0[40];
	char Modem1[40];
	char Mouse[40];
	char Net0[40];
	char Net1[40];
	char Printer[40];
	char Media0[40];
	char Media1[40];
	char Media2[40];
	char Media3[40];
	char Media4[40];
} Demographics;

///----------------------------------------------------------------------------------------------------

typedef struct S2BServerLogin
{
	u8 Type; //0x02
	u32 ServerID;
	u32 GroupID;
	u32 ScoreID;
	char ServerName[SERVER_NAME_LEN];
	u16 Port;
	char Password[SERVER_PW_LEN];
} S2BServerLogin;

typedef struct S2BUserLogin
{
	u8 Type; //0x04
	u8 MakeNew;
	u32 IPAddress;
	char Name[32]; //FIXME: bigger than b2s limit, can only be fixed in protocol v2
	char Password[USER_PW_LEN];
	u32 ConnectionID;
	u32 MachineID;
	i32 Timezone;
	u8 Unused0; //unknown
	u8 Sysop;
	u16 ClientVersion;
	u8 ClientExtraData[256]; //up to 256
} S2BUserLogin;

typedef struct S2BUserLogoff
{
	u8 Type; //0x05
	u32 ConnectionID;
	u16 DisconnectReason;
	u16 Latency; //unknown order
	u16 Ping; //unknown order
	u16 PacketLossS2C; //unknown order
	u16 PacketLossC2S; //unknown order
	PlayerScore Score;
} S2BUserLogoff;

typedef struct S2BUserScore
{
	u8 Type; //0x11
	u32 ConnectionID;
	PlayerScore Score;
} S2BUserScore;

typedef struct S2BUserBanner
{
	u8 Type; //0x10
	u32 ConnectionID;
	u8 Data[BANNER_SIZE];
} S2BUserBanner;

typedef struct S2BUserTextPrivate
{
	u8 Type; //0x07
	u32 ConnectionID;
	u32 GroupID;
	u8 SubType;
	u8 Sound;
	char Text[TEXT_LEN]; //or less
} S2BUserTextPrivate;

typedef struct S2BUserTextChat
{
	u8 Type; //0x14
	u32 ConnectionID;
	char ChannelName[CHAT_NAME_LEN];
	char Text[TEXT_LEN]; //or less
} S2BUserTextChat;

typedef struct S2BUserCommand
{
	u8 Type; //0x13
	u32 ConnectionID;
	char Text[TEXT_LEN]; //or less
} S2BUserCommand;

typedef struct S2BUserDemographics
{
	u8 Type; //0x0D
	u32 ConnectionID;
	// u8 Data[765];
	Demographics dg;
} S2BUserDemographics;

typedef struct S2BServerCapabilities
{
	u8 Type; //0x15
	u32 MultiCastChat:1;
	u32 SupportDemographics:1;
	u32 Unused:30;
} S2BServerCapabilities;

///----------------------------------------------------------------------------------------------------

typedef struct B2SUserLogin
{
	u8 Type; //0x01
	u8 Result;
	u32 ConnectionID;
	u8 Name[USER_NAME_LEN];
	u8 Squad[SQUAD_NAME_LEN];
	u8 Banner[BANNER_SIZE];
	u32 SecondsPlayed;
	struct
	{
		u16 Year,Month,Day,Hour,Min,Sec;
	} FirstLogin;
	u32 Unused0; //unknown
	u32 UserID;
	u32 Unused1; //unknown
	PlayerScore Score; //present only if Result==OK
} B2SUserLogin;

typedef struct B2SUserTextPrivate
{
	u8 Type; //0x03
	u32 SourceServerID;
	u8 SubType; //2
	u8 Sound;
	char Text[TEXT_LEN]; //or less
} B2SUserTextPrivate;

typedef struct B2SUserKickout
{
	u8 Type; //0x08
	u32 ConnectionID;
	u16 Reason;
} B2SUserKickout;

typedef struct B2SUserTextCommand
{
	u8 Type; //0x09
	u32 ConnectionID;
	char Text[TEXT_LEN]; //or less
} B2SUserTextCommand;

typedef struct B2SUserTextChat
{
	u8 Type; //0x0A
	u32 ConnectionID;
	u8 ChannelNum;
	char Text[TEXT_LEN]; //or less
} B2SUserTextChat;

typedef struct B2SUserTextMultiChat
{
	u8 Type; //0x34
	u8 Count;
	struct Recipients
	{
		u32 ConnectionID;
		u8 ChanNr;
	} Recipient[45]; // repeated Count times, Text follows
	char Text[TEXT_LEN]; //or less
} B2SUserTextMultiChat;

typedef struct B2SScorereset
{
	u8 Type; //0x31
	u32 ScoreID;
	u32 ScoreIDNeg;
} B2SScorereset;

typedef struct B2SUserPacket
{
	u8 Type; //0x32
	u32 ConnectionID;
	u8 Data[1024];
} B2SUserPacket;

typedef struct B2SBillingIdentity
{
	u8 Type; //0x33
	u8 IDData[256];
} B2SBillingIdentity;

typedef struct B2SServerLogin
{ //newly added to politely inform zone of their login status
	u8 Type; //0x41
	u32 ServerID; //sid they sent
	u8 Result; //uses player LoginCode for now 
} B2SServerLogin;

typedef struct B2SServerKickout
{ //newly added to politely inform zone why they are being kicked
	u8 Type; //0x42
	u32 ServerID; //sid they sent
	// char Reason[100];
	u16 Reason;
} B2SServerKickout;

///----------------------------------------------------------------------------------------------------

//restore original alignment from stack
#pragma pack(pop)

#endif















