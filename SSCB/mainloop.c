
#include "ssc.h"

local Imodman *mm;

typedef struct TimerData
{
	TimerFunc func;
	ticks_t interval, when;
	void *param;
	void *key;
	int killme;
} TimerData;

typedef struct WorkData
{
	WorkFunc func;
	void *param;
} WorkData;

local int quit;
local TimerData *thistimer;
local LinkedList timers;

#define CFG_THREAD_POOL_WORKER_THREADS 5
local pthread_t worker_threads[CFG_THREAD_POOL_WORKER_THREADS];
local MPQueue work_queue;

local pthread_mutex_t tmrmtx=PTHREAD_MUTEX_INITIALIZER;
#define LOCK() pthread_mutex_lock(&tmrmtx)
#define UNLOCK() pthread_mutex_unlock(&tmrmtx)

int RunLoop(void)
{
	TimerData *td=NULL;
	ticks_t gtc;

	while(!quit)
	{
		/* call all funcs */
		DO_CBS(CB_MAINLOOP,MainLoopFunc,());

		/* do timers */
		LOCK();
startover:
		gtc=current_ticks();
		FOR_EACH_LINK(timers,td,l)
		{
			if(td->func && TICK_GT(gtc, td->when))
			{
				int ret;
				thistimer=td;
				UNLOCK();
				ret=td->func(td->param);
				LOCK();
				thistimer=NULL;
				if(td->interval == 0 || td->killme || !ret)
				{
					LLRemove(&timers, td);
					afree(td);
				}
				else td->when=gtc + td->interval;
				
				goto startover;
			}
		}
		UNLOCK();

		fullsleep(5); //rest a bit and allow OS to do other things
	}

	return quit;
}

void KillML(ml_code code)
{
	quit=code;
}

void StartTimer(TimerFunc f, int startint, int interval, void *param, void *key)
{
	TimerData *data=amalloc(sizeof(TimerData));

	data->func=f;
	data->interval=interval;
	data->when=TICK_MAKE(current_ticks() + startint);
	data->param=param;
	data->key=key;
	LOCK();
	LLAddLast(&timers, data);
	UNLOCK();
}

void CleanupTimer(TimerFunc func, void *key, CleanupFunc cleanup)
{
	LOCK();
	TimerData *td=NULL;
	FOR_EACH_LINK(timers,td,l)
	{
		if(td->func == func && (td->key == key || key == NULL))
		{
			if(cleanup) cleanup(td->param);
			if(td == thistimer) td->killme=true;
			else
			{
				LLRemove(&timers,td);
				afree(td);
			}
		}
	}
	UNLOCK();
}

void ClearTimer(TimerFunc f, void *key)
{
	CleanupTimer(f,key,NULL);
}

void RunInThread(WorkFunc func, void *param)
{
	WorkData *wd=amalloc(sizeof(*wd));
	wd->func=func;
	wd->param=param;
	MPAdd(&work_queue,wd);
}

local void *thread_main(void *dummy)
{
	for(;;)
	{
		WorkData *wd=MPRemove(&work_queue);
		if(!wd) return NULL;
		wd->func(wd->param);
		afree(wd);
	}
}

local Imainloop mlint=
{
	INTERFACE_HEAD_INIT(I_MAINLOOP,"mainloop"),
	StartTimer, ClearTimer, CleanupTimer, RunLoop, KillML, RunInThread
};

EXPORT char* info_mainloop=
"Main Loop Module\n"
"by Cheese\n"
"Runs the main executable loop.";

EXPORT ModReturn MM_mainloop(Imodman *mm2, ModAction action)
{
	int i;
	if(action == MM_LOAD)
	{
		mm=mm2;
		quit=0;
		LLInit(&timers);
		MPInit(&work_queue);
		for(i=0; i < CFG_THREAD_POOL_WORKER_THREADS; i++) pthread_create(&worker_threads[i], NULL, thread_main, NULL);
		
		mm->RegInterface(&mlint);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		LLEnum(&timers, afree);
		LLEmpty(&timers);
		
		for(i=0; i < CFG_THREAD_POOL_WORKER_THREADS; i++) MPAdd(&work_queue, NULL);
		for(i=0; i < CFG_THREAD_POOL_WORKER_THREADS; i++) pthread_join(worker_threads[i], NULL);
		
		MPDestroy(&work_queue);
		if(mm->UnregInterface(&mlint)) return MM_FAIL;
		
		return MM_OK;
	}
	return MM_FAIL;
}
