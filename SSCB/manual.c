
#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;
local Iconfig *cfg;
local Icapman *cm;
local Icmdman *cmd;
local Itext *txt;

local char *command_name=NULL;

local void do_cmd_help(Player *p, char *command)
{
	helptext_t ht=cmd->GetHelpText(command);
	
	char cap[40];
	snprintf(cap,sizeof(cap),"cmd_%s",cmd);
	int canuse=cm->HasCapability(p,cap);

	if(!ht) txt->SendCommandResponse(p,"?%s is not a registered command.",command);
	else if(!canuse)
		txt->SendCommandResponse(p,"You need permission to use ?%s to see its details.",command);
	else txt->SendCommandResponse(p,"Command information for ?%s:\n%s",command,ht);
}

local helptext_t manual_help=
"Args: <command> | <section:key>\n"
"Displays help on a command or config file setting. Use {section:}\n"
"to list known keys in that section. Use {:} to list known section names.\n";
local void Cmanual(Player *p, char *command, char *params)
{
	if(params[0] == '?' || params[0] == '*' || params[0] == '!') params++;
	if(params[0] == '\0') params=command_name;

	if(strchr(params,':'))
	{
		// char secname[MAXSECTIONLEN];
		// const char *keyname;

		// if(!cfghelp)
		{
			txt->SendCommandResponse(p,"Config file settings help isn't loaded.");
			return;
		}

		// keyname=delimcpy(secname,params,MAXSECTIONLEN,':');

		// if(secname[0] == '\0') do_list_sections(p);
		// else if(keyname[0] == '\0') do_list_keys(p,secname);
		// else do_setting_help(p,secname,keyname);
	}
	else do_cmd_help(p,params);
}

local helptext_t bmoduleinfo_help=
"Args: <module>\n"
"Displays information about the specified module.\n"
"This might include a version number and a general description of the module.\n";
local void Cbmoduleinfo(Player *p, char *command, char *params)
{
	int canuse=cm->HasCapability(p,"seemodules");
	if(!canuse)
	{
		txt->SendCommandResponse(p,"You do not have permission to see modules.");
		return;
	}
	
	char *info=mm->GetModuleInfo(params);
	char *loader=mm->GetModuleLoader(params);
	if(!info || !loader)
		txt->SendCommandResponse(p,"No information available for the '%s' module.",params);
	else
	{
		txt->SendCommandResponse(p,"Module: %s",params);
		txt->SendCommandResponse(p,"Loader: %s",loader);
		
		char buf[100];
		char *prefix="Info:";
		char *tmp=NULL;
		while(strsplit(info,"\n",buf,sizeof(buf),&tmp))
		{
			txt->SendCommandResponse(p,"%-5s %s",prefix,buf);
			prefix=":";
		}
	}
}

EXPORT char* info_manual=
"Manual Module\n"
"by Cheese\n"
"Allows users to get information about commands, settings, or modules.";

EXPORT ModReturn MM_manual(Imodman *mm2, ModAction action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		cfg=mm->GetInterface(I_CONFIG);
		cm=mm->GetInterface(I_CAPMAN);
		cmd=mm->GetInterface(I_CMDMAN);
		txt=mm->GetInterface(I_TEXT);
		if(!lm || !cfg || !cm || !cmd || !txt) return MM_FAIL;
		
		command_name=astrdup(cfg->GetStr(GLOBAL,"General","ManualCommand"));
		cmd->AddCommand(command_name,Cmanual,manual_help);
		cmd->AddCommand("bmoduleinfo",Cbmoduleinfo,bmoduleinfo_help);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		cmd->RemoveCommand(command_name,Cmanual);
		cmd->RemoveCommand("bmoduleinfo",Cbmoduleinfo);
		
		afree(command_name);
		
		mm->ReleaseInterface(txt);
		mm->ReleaseInterface(cmd);
		mm->ReleaseInterface(cm);
		mm->ReleaseInterface(cfg);
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}
