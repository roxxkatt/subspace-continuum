
#ifndef __SQUAD_H
#define __SQUAD_H

typedef struct Squad
{
	int id;
	int db_id;
	
	char name[SQUAD_NAME_LEN];
	char password[SQUAD_PW_LEN];
	char owner[USER_NAME_LEN];
	Ticks created;
	Ticks lastaction;
	
	Byte extradata[0];
} Squad;

typedef enum
{
	SQA_CREATE,
	SQA_LOAD,
	SQA_UNLOAD,
	SQA_DESTROY
} SquadAction;

#define CB_SQUADACTION "squadaction"
typedef void (*SquadActionFunc)(Squad *s, SquadAction action);

#define CB_LOADSQUAD "loadsquad"
typedef void (*LoadSquadResponseFunc)(Squad *s, bool loaded);
typedef void (*LoadSquadFunc)(Squad *s, LoadSquadResponseFunc f);

#define CB_SQUADPOINTER "squadpointer"
typedef void (*SquadPointerFunc)(Squad *s, bool isnew);

#define I_SQUADS "squads"

typedef struct Isquads
{
	INTERFACE_HEAD_DECL;

	Squad* (*NewSquad)(void);
	void (*FreeSquad)(Squad *s);
	Size (*AllocateSquadData)(Size bytes);
	void (*FreeSquadData)(int key);
	void (*Lock)(void);
	void (*Unlock)(void);
	
	Squad* (*FindSquadByID)(int id);
	Squad* (*FindSquadByName)(char *name);
	
	LinkedList squadlist;
	
} Isquads;

#define FOR_EACH_SQUAD(s) \
	for( \
		link=LLGetHead(&sqd->squadlist); \
		link && ((s=link->data, link=link->next) || 1); )

#define FOR_EACH_SQUAD_DATA(s,d,key) \
	for( \
		link=LLGetHead(&sqd->squadlist); \
		link && ((s=link->data, \
		          d=GETDATA(s,key), \
		          link=link->next) || 1); )

#endif
