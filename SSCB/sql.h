
#ifndef __SQL_H
#define __SQL_H


typedef struct db_res db_res;
typedef struct db_row db_row;

//be advised that you need to manage your pointers so that it is not freed before sql result comes back
typedef void (*query_callback)(int status, db_res *res, void *clos);

#define I_SQL "sql"
typedef struct Isql
{
	INTERFACE_HEAD_DECL;

	bool (*GetStatus)(void); //true if connected, false if not

	/* fmt may contain '?'s, which will be replaced by the corresponding
	 * argument as a properly escaped and quoted string, and '#'s, which
	 * will be replaced by the corresponding argument as an unsigned
	 * int. python users have to escape stuff themselves, using EscapeString. */
	int (*Query)(query_callback cb, void *clos, int notifyfail, char *fmt, ...);

	int (*GetRowCount)(db_res *res);
	int (*GetFieldCount)(db_res *res);
	db_row* (*GetRow)(db_res *res);
	char* (*GetField)(db_row *row, int fieldnum);

	/* returns the value generated for an AUTO_INCREMENT column for the
	 * previous INSERT or UPDATE. you must call this immediately after
	 * the query that you want the id from. if this returns -1, it means
	 * this database implementation doesn't support this functionality. */
	int (*GetLastInsertId)(void);

	//only useful to python. C users should use formatting characters in Query.
	int (*EscapeString)(char *str, char *buf, int buflen);
} Isql;



#endif

