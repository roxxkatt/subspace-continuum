
#include "ssc.h"

local Imodman *mm;

/* all module entry points must be of this type */
typedef ModReturn (*ModMain)(Imodman *mm, ModAction action);

typedef struct c_mod_data_t
{
	void *handle;
	ModMain main;
	int ismyself;
} c_mod_data_t;

local int load_c_module(char *spec_, mod_args_t *args)
{
	char buf[PATH_MAX];
	char spec[PATH_MAX];
	char *path;
	c_mod_data_t *cmd;
	Ilogman *lm=NULL;

#define LOG0(lev, fmt) \
	if(lm) lm->Log(L_SYNC | lev, fmt); \
	else fprintf(stderr, "%c%c " fmt "\n", L_EXE, lev);
#define LOG1(lev, fmt, a1) \
	if(lm) lm->Log(L_SYNC | lev, fmt, a1); \
	else fprintf(stderr, "%c%c " fmt "\n", L_EXE, lev, a1);
#define LOG2(lev, fmt, a1, a2) \
	if(lm) lm->Log(L_SYNC | lev, fmt, a1, a2); \
	else fprintf(stderr, "%c%c " fmt "\n", L_EXE, lev, a1, a2);

	/* make copy of specifier */
	astrncpy(spec, spec_, sizeof(spec));

	char *filename;
	char *modname=strchr(spec, ':');
	if(modname)
	{
		*modname=0;
		modname++;
		filename=spec;
	}
	else
	{
		modname=spec;
		filename="internal";
	}

	lm=mm->GetInterface(I_LOGMAN);

	if(strcasecmp(filename, "internal")) { LOG2(L_INFO, "<cmod> Loading C module '%s' from '%s'", modname, filename); }
	else { LOG2(L_INFO, "<cmod> Loading C module '%s'%s", modname, ""); }

	/* get a struct for our private data */
	cmd=amalloc(sizeof(c_mod_data_t));
	args->privdata=cmd;

	if(!strcasecmp(filename, "internal"))
	{
		path=NULL;
		cmd->ismyself=1;
	}
#ifdef CFG_RESTRICT_MODULE_PATH
	else if(strstr(filename, "..") || filename[0] == '/')
	{
		LOG1(L_ERROR, "<cmod> Refusing to load filename: %s", filename);
		goto die;
	}
#else
	else if(filename[0] == '/')
	{
		/* filename is an absolute path */
		path=filename;
	}
#endif
	else
	{
		char cwd[PATH_MAX], dir[NAME_MAX];
		char *tmp=NULL;
		getcwd(cwd, sizeof(cwd));
		path=NULL;
		while(strsplit(CFG_CMOD_SEARCH_PATH, ":", dir, sizeof(dir), &tmp))
		{
			if(snprintf(buf, sizeof(buf), "%s/%s/%s"
#ifndef WIN32
						".so",
#else
						".dll",
#endif
						cwd, dir, filename) > sizeof(buf))
				continue;
			if(access(buf, F_OK) == 0)
			{
				path=buf;
				break;
			}
		}
		if(!path)
		{
			LOG1(L_ERROR, "<cmod> Can't find file: %s", filename);
			goto die;
		}
	}

	cmd->handle=dlopen(path, RTLD_NOW | RTLD_GLOBAL);
	if(!cmd->handle)
	{
#ifndef WIN32
		LOG1(L_ERROR, "<cmod> Error in dlopen: %s", dlerror());
#else
		LPVOID lpMsgBuf;

		FormatMessage(
				FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL,GetLastError(),MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),(LPTSTR) &lpMsgBuf,0,NULL);
		LOG1(L_ERROR, "<cmod> Error in LoadLibrary: %s", (LPCTSTR)lpMsgBuf);
		LocalFree(lpMsgBuf);
#endif
		goto die;
	}

	snprintf(buf, sizeof(buf), "MM_%s", modname);
	cmd->main=(ModMain)dlsym(cmd->handle, buf);
	if(!cmd->main)
	{
#ifndef WIN32
		LOG1(L_ERROR, "<cmod> Error in dlsym: %s", dlerror());
#else
		LPVOID lpMsgBuf;
		FormatMessage(
				FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL,GetLastError(),MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),(LPTSTR)&lpMsgBuf,0,NULL);
		LOG1(L_ERROR, "<cmod> Error in GetProcAddress: %s", (LPCTSTR)lpMsgBuf);
		LocalFree(lpMsgBuf);
#endif
		if(!cmd->ismyself) dlclose(cmd->handle);
		goto die;
	}

	/* load info if it exists */
	snprintf(buf, sizeof(buf), "info_%s", modname);
	args->info=dlsym(cmd->handle, buf);

	astrncpy(args->name, modname, sizeof(args->name));

	int ret=cmd->main(mm,MM_LOAD);

	if(ret != MM_OK)
	{
		LOG1(L_ERROR, "<cmod> Error loading module '%s'", modname);
		if(!cmd->ismyself) dlclose(cmd->handle);
		goto die;
	}

	if(lm) mm->ReleaseInterface(lm);

	return MM_OK;

die:
	afree(cmd);
	if(lm) mm->ReleaseInterface(lm);
	return MM_FAIL;

#undef LOG0
#undef LOG1
#undef LOG2
}


local int unload_c_module(mod_args_t *args)
{
	c_mod_data_t *cmd=args->privdata;
	if(cmd->main) if((cmd->main)(mm, MM_UNLOAD) == MM_FAIL) return MM_FAIL;
	if(cmd->handle && !cmd->ismyself) dlclose(cmd->handle);
	afree(cmd);
	return MM_OK;
}


local ModReturn loader(ModAction action, mod_args_t *args, char *line)
{
	c_mod_data_t *cmd=args->privdata;

	switch(action)
	{
		case MM_LOAD:
			return load_c_module(line,args);

		case MM_UNLOAD:
			return unload_c_module(args);

		case MM_ATTACH:
		case MM_DETACH:
		case MM_POSTLOAD:
		case MM_PREUNLOAD:
			return cmd->main(mm,action);

		default:
			return MM_FAIL;
	}
}


void RegCModLoader(Imodman *mm2)
{
	mm=mm2;
	mm->RegModuleLoader("c",loader);
}

void UnregCModLoader(void)
{
	mm->UnregModuleLoader("c",loader);
}

