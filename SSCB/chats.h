
#ifndef __CHAT_H
#define __CHAT_H

typedef struct Chat
{
	int id;
	int db_id;
	
	char name[CHAT_NAME_LEN];
	char password[CHAT_PW_LEN];
	char owner[USER_NAME_LEN];
	Ticks created;
	Ticks lastaction;
	
	Byte extradata[0];
} Chat;

typedef enum
{
	CA_CREATE,
	CA_LOAD,
	CA_UNLOAD,
	CA_DESTROY
} ChatAction;

#define CB_CHATACTION "chataction"
typedef void (*ChatActionFunc)(Chat *c, ChatAction action);

#define CB_LOADCHAT "loadchat"
typedef void (*LoadChatResponseFunc)(Chat *c, bool loaded);
typedef void (*LoadChatFunc)(Chat *c, LoadChatResponseFunc f);

#define CB_CHATPOINTER "chatpointer"
typedef void (*ChatPointerFunc)(Chat *c, bool isnew);

#define I_CHATS "chats"

typedef struct Ichats
{
	INTERFACE_HEAD_DECL;

	Chat* (*NewChat)(void);
	void (*FreeChat)(Chat *s);
	Size (*AllocateChatData)(Size bytes);
	void (*FreeChatData)(int key);
	void (*Lock)(void);
	void (*Unlock)(void);
	
	Chat* (*FindChatByID)(int id);
	Chat* (*FindChatByName)(char *name);
	
	LinkedList chatlist;
	
} Ichats;

#define FOR_EACH_CHAT(s) \
	for( \
		link=LLGetHead(&cd->chatlist); \
		link && ((c=link->data, link=link->next) || 1); )

#define FOR_EACH_CHAT_DATA(c,d,key) \
	for( \
		link=LLGetHead(&cd->chatlist); \
		link && ((c=link->data, \
		          d=GETDATA(c,key), \
		          link=link->next) || 1); )

#endif
