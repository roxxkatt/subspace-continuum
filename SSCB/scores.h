
#ifndef __SCORE_H
#define __SCORE_H

typedef struct Score
{
	int id;
	int db_id;
	
	Zone* z; //TODO FIXME: replace with score group id
	Player *p;
	PlayerScore Score;
	Ticks lastupdatetime;
	
	Byte extradata[0];
} Score;

typedef enum
{
	SCA_CREATE,
	SCA_LOAD,
	SCA_UNLOAD,
	SCA_DESTROY
} ScoreAction;

#define CB_SCOREACTION "scoreaction"
typedef void (*ScoreActionFunc)(Score *s, ScoreAction action);

#define CB_LOADSCORE "loadscore"
typedef void (*LoadScoreResponseFunc)(Score *s, bool loaded);
typedef void (*LoadScoreFunc)(Score *s, LoadScoreResponseFunc f);

#define CB_SCOREPOINTER "scorepointer"
typedef void (*ScorePointerFunc)(Score *s, bool isnew);

#define I_SCORES "scores"

typedef struct Iscores
{
	INTERFACE_HEAD_DECL;

	Score* (*NewScore)(void);
	void (*FreeScore)(Score *s);
	Size (*AllocateScoreData)(Size bytes);
	void (*FreeScoreData)(int key);
	void (*Lock)(void);
	void (*Unlock)(void);
	
	Score* (*FindScoreByID)(int id);
	Score* (*FindScore)(Zone* z, Player *p);
	
	LinkedList scorelist;
	
} Iscores;

#define FOR_EACH_SCORE(s) \
	for( \
		link=LLGetHead(&scd->scorelist); \
		link && ((s=link->data, link=link->next) || 1); )

#define FOR_EACH_SCORE_DATA(s,d,key) \
	for( \
		link=LLGetHead(&scd->scorelist); \
		link && ((s=link->data, \
		          d=GETDATA(s,key), \
		          link=link->next) || 1); )

#endif
