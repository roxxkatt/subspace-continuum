
#ifndef __NET_H
#define __NET_H

typedef enum
{
	//0 means unset
	//1-99 reserved for listening ports
	//100 and up for disposable locations for connecting servers, clients, etc
	LOC_NOWHERE		=0,
	LOC_CLIENT		=1,
	LOC_SERVER		=2,
	LOC_BILLER		=3,
	LOC_DIRECTORY	=4,
	LOC_REPOSITORY	=5,
	LOC_UPDATER		=6,
	LOC_PING		=7,
	LOC_OTHER		=100
} NetLoc;

typedef struct sockaddr_in WebInfo;
typedef int SocketID;

#define CB_PACKETARRIVED "PacketArrived"
typedef void (*PacketArrivedFunc)(NetLoc dest, void* pkt, int len, WebInfo* wi);

//for working with protocols or reporting uncaught packets
//also for handling packets that stupid people were too stupid to add a type byte to
//return true for handled, false for unhandled
#define CB_UNHANDLEDPACKET "UnhandledPacket"
typedef bool (*UnhandledPacketFunc)(NetLoc dest, void* pkt, int len, WebInfo* wi);

//return true for handled, false for unhandled
typedef bool (*PacketFunc)(NetLoc loc, Byte* data, int length, WebInfo* wi);

#define I_NET "net-1"

typedef struct Inet
{
	INTERFACE_HEAD_DECL;

	//sockets
	NetLoc (*CreateNewLocation)(void); //create an exclusive location to communicate with
	void (*CreateNewSocket)(NetLoc loc, bool listenonly); //then create a socket
	void (*CreateNewEndpoint)(NetLoc loc, char* ip, int port); //then create an address, ip="" for any, port=0 for random unused port
	bool (*ConnectToLocation)(NetLoc loc); //then connect and start talking
	void (*SetLocationEndpoint)(NetLoc loc, WebInfo* wi); //or use an existing location
	
	//packet handlers
	void (*AddPacket)(NetLoc loc, Byte type, PacketFunc func);
	void (*RemovePacket)(NetLoc loc, Byte type, PacketFunc func);
	
	bool (*SendPacket)(NetLoc loc, void* pkt, int len); //send packet normally
	void (*ReceivePacket)(NetLoc loc, void* pkt, int len, WebInfo* wi); //spoof recieving a packet
	bool (*SendPacketToFrom)(WebInfo* to, NetLoc from, void* pkt, int len); //to send from specific port
	
	// void (*RegisterEncryption)(Iencrypt* enc);
	void (*RegisterEncryption)(void* enc); //resolve include circular dependency
	
} Inet;

#endif
