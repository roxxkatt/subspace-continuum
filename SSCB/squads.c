
#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;
local Iplayers *pd;
local Itext *txt;
local Icmdman *cmd;

#define ID_REUSE_DELAY 10

local int dummykey;
local int magickey;

local pthread_mutex_t lockmtx;
#define LOCK() pthread_mutex_lock(&lockmtx)
#define UNLOCK() pthread_mutex_unlock(&lockmtx)

local struct
{
	Squad *s;
	int next;
	time_t available;
} *idmap;

local int firstfreeid;
local int idmapsize;
local int perunitspace;
local pthread_mutexattr_t recmtxattr;

local LinkedList blocks;
local pthread_mutex_t blockmtx;
struct block
{
	int start;
	int len;
};

local Isquads sqdint;
#define sqd (&sqdint)

local void Lock(void)
{
	LOCK();
}

local void Unlock(void)
{
	UNLOCK();
}

local Squad* alloc_squad(void)
{
	Squad *s=amalloc(sizeof(*s)+perunitspace);
	*(unsigned*)GETDATA(s,magickey)=MODMAN_MAGIC;
	return s;
}

local Squad* NewSquad(void)
{
	LOCK();
	time_t now=time(NULL);

	//find a free id that's available
	int *ptr=&firstfreeid;
	int id=firstfreeid;
	while(id != -1 && idmap[id].available > now)
	{
		ptr=&idmap[id].next;
		id=idmap[id].next;
	}

	if(id == -1)
	{
		//no available ids
		int newsize=idmapsize*2;
		idmap=arealloc(idmap,newsize*sizeof(idmap[0]));
		for(id=idmapsize; id < newsize; id++)
		{
			idmap[id].s=NULL;
			idmap[id].next=id+1;
			idmap[id].available=0;
		}
		idmap[newsize-1].next=firstfreeid;
		firstfreeid=idmapsize;
		ptr=&firstfreeid;
		id=firstfreeid;
		idmapsize=newsize;
	}

	*ptr=idmap[id].next;

	Squad *s=idmap[id].s;
	if(!s) s=idmap[id].s=alloc_squad();

	s->id=id;
	LLAddLast(&sqd->squadlist,s);
	UNLOCK();

	DO_CBS(CB_SQUADPOINTER,SquadPointerFunc,(s,TRUE));

	return s;
}

local Size AllocateSquadData(Size bytes)
{
	struct block *b, *nb;
	int current=0;

	//round up to next multiple of word size
	bytes=(bytes+(sizeof(int)-1)) & (~(sizeof(int)-1));

	pthread_mutex_lock(&blockmtx);

	//first try before between two blocks (or at the beginning)
	Link *link;
	Link *last=NULL;
	for(link=LLGetHead(&blocks); link; link=link->next)
	{
		b=link->data;
		if((b->start - current) >= (int)bytes) goto found;
		else current=b->start + b->len;
		last=link;
	}

	//if we couldn't get in between two blocks, try at the end
	if((perunitspace-current) >= (int)bytes) goto found;

	//no more space
	pthread_mutex_unlock(&blockmtx);
	return -1;

found:
	nb=amalloc(sizeof(*nb));
	nb->start=current;
	nb->len=bytes;
	//if last is NULL, this will put it in front of the list
	LLInsertAfter(&blocks,last,nb);

	//clear all newly allocated space
	Squad *s;
	void *data;
	LOCK();
	FOR_EACH_SQUAD_DATA(s,data,current) memset(data,0,bytes);
	UNLOCK();

	pthread_mutex_unlock(&blockmtx);

	return current;
}

local void FreeSquad(Squad *s)
{
	if(!s) return;
	DO_CBS(CB_PLAYERPOINTER,SquadPointerFunc,(s,FALSE));

	LOCK();
	LLRemove(&sqd->squadlist,s);
	idmap[s->id].s=NULL;
	idmap[s->id].available=time(NULL)+ID_REUSE_DELAY;
	idmap[s->id].next=firstfreeid;
	firstfreeid=s->id;
	UNLOCK();

	afree(s);
}

local void FreeSquadData(int key)
{
	Link *l;
	pthread_mutex_lock(&blockmtx);
	for(l=LLGetHead(&blocks); l; l=l->next)
	{
		struct block *b=l->data;
		if(b->start == key)
		{
			LLRemove(&blocks,b);
			afree(b);
			break;
		}
	}
	pthread_mutex_unlock(&blockmtx);
}

local Squad* FindSquadByID(int id)
{
	LOCK();
	if(id >= 0 && id < idmapsize)
	{
		Squad *s=idmap[id].s;
		UNLOCK();
		return s;
	}
	UNLOCK();
	return NULL;
}

local Squad* FindSquadByName(char *name)
{
	if(!name) return NULL;
	Squad *s;
	Link *link;
	LOCK();
	FOR_EACH_SQUAD(s)
		if(strcasecmp(s->name,name) == 0)
		{
			UNLOCK();
			return s;
		}
	UNLOCK();
	return NULL;
}

local void PrivSquadMsg(char* sender, char* squad, int sound, char* text)
{
	if(!sender || !squad || !text) return;
	int num=0;
	Player *p;
	Link *link;
	pd->Lock();
	FOR_EACH_PLAYER(p)
	{
		if(!strcasecmp(p->squad,squad))
		{
// lm->Log(L_DRIVEL,"<squads> sending to %s",p->name);
			txt->SendChat(p,0,"(%s) %s> %s",squad,sender,text);
			num++;
		}
	}
	pd->Unlock();
#ifdef LOG_SQUAD_MESSAGES
	lm->Log(L_DRIVEL,"<squads> incoming squad pm to %u users: (%s) %s> %s",num,squad,sender,text);
#endif
}

local helptext_t squadcreate_help=
"Args: <name>:<password | none>\n"
"Creates a squad.\n";
local void Csquadcreate(Player *p, char *cmd, char *params)
{
	if(!params || !*params)
	{
		txt->SendCommandResponse(p,"Syntax error, try ?bman squadcreate.");
		return;
	}
	
	if(strlen(p->squad) > 0)
	{
		txt->SendCommandResponse(p,"You are already in a squad.");
		return;
	}
	
	char *origline=strdup(params);
	char *line=origline;
	char *name=NULL;
	char *pw=NULL;

	char *c=strchr(line,':');
	if(!c)
	{
		txt->SendCommandResponse(p,"Syntax error, try ?bman squadjoin.");
		return;
	}
	*c=0;
	line=c+1;
	if(!*line)
	{
		txt->SendCommandResponse(p,"Syntax error, try ?bman squadjoin.");
		return;
	}
	name=origline;
	pw=line;
	
	if(strlen(name) >= SQUAD_NAME_LEN)
	{
		txt->SendCommandResponse(p,"Names must be less than %u letters.",SQUAD_NAME_LEN);
		return;
	}
	
	if(strlen(pw) >= SQUAD_PW_LEN)
	{
		txt->SendCommandResponse(p,"Passwords must be less than %u letters.",SQUAD_PW_LEN);
		return;
	}
	
	if(!strcasecmp(pw,"none")) pw="";

	//TODO FIXME: load from db, will overwrite offline squads
	Squad *ss=FindSquadByName(name);
	if(ss)
	{
		txt->SendCommandResponse(p,"That squad already exists.");
		return;
	}
	
	Squad *s=NewSquad();
	strncpy(s->name,name,SQUAD_NAME_LEN);
	strncpy(s->password,pw,SQUAD_PW_LEN);
	strncpy(s->owner,pw,USER_NAME_LEN);
	s->created=current_ticks();
	{ DO_CBS(CB_SQUADACTION,SquadActionFunc,(s,SQA_CREATE)); }

	strncpy(p->squad,params,SQUAD_NAME_LEN);
	txt->SendCommandResponse(p,"Successfully created and joined %s.",s->name);
	lm->Log(L_INFO,"<squads> %s created %s",p->name,s->name);
}

local helptext_t squadpassword_help=
"Args: <password>\n"
"Changes your squad password.\n";
local void Csquadpassword(Player *p, char *cmd, char *params)
{
	if(!params || !*params)
	{
		txt->SendCommandResponse(p,"Syntax error, try ?bman squadpassword.");
		return;
	}
	
	char *pw=params;
	
	if(strlen(p->squad) < 1)
	{
		txt->SendCommandResponse(p,"You are not in a squad.");
		return;
	}
	
	if(strlen(pw) >= SQUAD_PW_LEN)
	{
		txt->SendCommandResponse(p,"Passwords must be less than %u letters.",SQUAD_PW_LEN);
		return;
	}
	
	if(!strcasecmp(pw,"none")) pw="";
	int pwlen=strlen(pw);

	Squad *s=FindSquadByName(p->squad);
	if(!s)
	{
		txt->SendCommandResponse(p,"Your squad is not loaded, try reconnecting.");
		return;
	}

	strncpy(s->password,pw,SQUAD_PW_LEN);
	if(pwlen) txt->SendCommandResponse(p,"You have changed the password of %s to %s",s->name,s->password);
	else txt->SendCommandResponse(p,"You have removed the password of %s",s->name);
	lm->Log(L_INFO,"<squads> %s changed the password of %s to %s",p->name,s->name,s->password);
}

local helptext_t squadgive_help=
"Args: <name>\n"
"Gives ownership of your squad.\n";
local void Csquadgive(Player *p, char *cmd, char *params)
{
	if(!params || !*params)
	{
		txt->SendCommandResponse(p,"Syntax error, try ?bman squadgive.");
		return;
	}
	
	if(strlen(p->squad) < 1)
	{
		txt->SendCommandResponse(p,"You are not in a squad.");
		return;
	}
	
	Squad *s=FindSquadByName(p->squad);
	if(!s)
	{
		txt->SendCommandResponse(p,"Your squad is not loaded, try reconnecting.");
		return;
	}

	if(strlen(params) >= USER_NAME_LEN)
	{
		txt->SendCommandResponse(p,"Names must be less than %u letters.",USER_NAME_LEN);
		return;
	}
	
	Player *p2=pd->FindPlayerByName(params);
	if(!p2)
	{
		//TODO FIXME: load from db
		txt->SendCommandResponse(p,"%s is offline or does not exist.",params);
		return;
	}
	else if(strcasecmp(p->squad,p2->squad) != 0)
	{
		txt->SendCommandResponse(p,"%s is not in your squad.",p2->name);
		return;
	}
	
	strncpy(s->owner,params,USER_NAME_LEN);
	txt->SendCommandResponse(p,"You have given ownership of %s to %s",p->squad,p2->name);
	lm->Log(L_INFO,"<squads> %s gave ownership of %s to %s",p->name,p->squad,p2->name);
}

local helptext_t squadjoin_help=
"Args: <name>[:<password>]\n"
"Joins a squad.\n";
local void Csquadjoin(Player *p, char *cmd, char *params)
{
	if(!params || !*params)
	{
		txt->SendCommandResponse(p,"Syntax error, try ?bman squadjoin.");
		return;
	}
	
	if(strlen(p->squad) > 0)
	{
		txt->SendCommandResponse(p,"You are already in a squad.");
		return;
	}
	
	char *origline=strdup(params);
	char *line=origline;
	char *name=NULL;
	char *pw=NULL;

	char *c=strchr(line,':');
	if(c)
	{
		*c=0;
		line=c+1;
		if(!*line)
		{
			txt->SendCommandResponse(p,"Syntax error, try ?bman squadjoin.");
			return;
		}
		pw=line;
	}
	name=origline;
	
	if(strlen(name) >= SQUAD_NAME_LEN)
	{
		txt->SendCommandResponse(p,"Names must be less than %u letters.",SQUAD_NAME_LEN);
		return;
	}
	
	Squad *s=FindSquadByName(name);
	if(!s)
	{
		//TODO FIXME: load from db
		txt->SendCommandResponse(p,"That squad does not exist or is not loaded, wait for an online member.");
		return;
	}

	if(strcasecmp(s->password,pw) != 0)
	{
		txt->SendCommandResponse(p,"Incorrect password for %s.",s->name);
		return;
	}
	
	strncpy(p->squad,name,SQUAD_NAME_LEN);
	txt->SendCommandResponse(p,"You have successfully joined %s.",p->squad);
	lm->Log(L_INFO,"<squads> %s joined %s",p->name,p->squad);
	
	afree(origline);
}

local helptext_t squad_help=
"Args: <name>\n"
"Displays a user's squad.\n";
local void Csquad(Player *p, char *cmd, char *params)
{
	if(!params || !*params)
	{
		txt->SendCommandResponse(p,"Syntax error, try ?bman squad.");
		return;
	}
	
	//TODO FIXME: load from db
	Player* p2=pd->FindPlayerByName(params);
	if(p2)
	{
		if(*(p2->squad)) txt->SendCommandResponse(p,"%s is in %s.",p2->name,p2->squad);
		else txt->SendCommandResponse(p,"%s is not in a squad.",p2->name);
	}
	else txt->SendCommandResponse(p,"%s is offline.",params);
	
}

local void LoadSquadResponse(Squad *s, bool loaded)
{
	//noop
}

local void playeraction(Player *p, PlayerAction action)
{
	if(action == PA_CONNECT)
	{
		//player connecting, load their squad from db
		if(!FindSquadByName(p->squad)) //check if loaded
		{
			Squad *s=NewSquad(); //create temp to check against
			strncpy(s->name,p->squad,SQUAD_NAME_LEN);
			{ DO_CBS(CB_LOADSQUAD,LoadSquadFunc,(s,LoadSquadResponse)); }
		}
	}
}

local Isquads sqdint=
{
	INTERFACE_HEAD_INIT(I_SQUADS,"squads"),
	NewSquad,FreeSquad,
	AllocateSquadData,FreeSquadData,
	Lock,Unlock,
	FindSquadByID,FindSquadByName
};

EXPORT char* info_squads=
"Squads Module\n"
"by Cheese\n"
"Allows users to join squads.";

EXPORT ModReturn MM_squads(Imodman *mm2, ModAction action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		pd=mm->GetInterface(I_PLAYERS);
		txt=mm->GetInterface(I_TEXT);
		cmd=mm->GetInterface(I_CMDMAN);
		if(!lm || !pd || !txt || !cmd) return MM_FAIL;
		
		pthread_mutexattr_init(&recmtxattr);
		pthread_mutexattr_settype(&recmtxattr,PTHREAD_MUTEX_RECURSIVE);
		pthread_mutexattr_settype(&recmtxattr,PTHREAD_MUTEX_RECURSIVE);
		pthread_mutex_init(&lockmtx,&recmtxattr);
		pthread_mutex_init(&blockmtx,NULL);

		idmapsize=256;
		idmap=amalloc(idmapsize*sizeof(idmap[0]));
		int i;
		for(i=0; i < idmapsize; i++)
		{
			idmap[i].s=NULL;
			idmap[i].next=i + 1;
			idmap[i].available=0;
		}
		idmap[idmapsize-1].next=-1;
		firstfreeid=0;

		LLInit(&sqd->squadlist);
		LLInit(&blocks);

		Iconfig *cfg=mm->GetInterface(I_CONFIG);
		perunitspace=cfg ? cfg->GetInt(GLOBAL,"General","PerSquadBytes",4000) : 4000;
		mm->ReleaseInterface(cfg);

		dummykey=AllocateSquadData(sizeof(unsigned));
		if(dummykey == -1) return MM_FAIL;
		magickey=AllocateSquadData(sizeof(unsigned));
		if(magickey == -1) return MM_FAIL;

		mm->RegCallback(CB_PLAYERACTION,playeraction);
		mm->RegCallback(CB_PRIVSQUADMSG,PrivSquadMsg);
		
		cmd->AddCommand("squadcreate",Csquadcreate,squadcreate_help);
		cmd->AddCommand("squadpassword",Csquadpassword,squadpassword_help);
		cmd->AddCommand("squadgive",Csquadgive,squadgive_help);
		cmd->AddCommand("squadjoin",Csquadjoin,squadjoin_help);
		cmd->AddCommand("squad",Csquad,squad_help);
		
		mm->RegInterface(&sqdint);

		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&sqdint)) return MM_FAIL;

		cmd->RemoveCommand("squadcreate",Csquadcreate);
		cmd->RemoveCommand("squadpassword",Csquadpassword);
		cmd->RemoveCommand("squadgive",Csquadgive);
		cmd->RemoveCommand("squadjoin",Csquadjoin);
		cmd->RemoveCommand("squad",Csquad);
		
		mm->UnregCallback(CB_PRIVSQUADMSG,PrivSquadMsg);
		mm->UnregCallback(CB_PLAYERACTION,playeraction);
		
		pthread_mutexattr_destroy(&recmtxattr);
		FreeSquadData(dummykey);
		FreeSquadData(magickey);
		afree(idmap);
		LLEnum(&sqd->squadlist,afree);
		LLEmpty(&sqd->squadlist);
		
		mm->ReleaseInterface(cmd);
		mm->ReleaseInterface(txt);
		mm->ReleaseInterface(pd);
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}
