
#ifndef __BILLING_H
#define __BILLING_H

typedef struct Connection
{
	Zone* z;
	Player *p;
	u32 ConnectionID;
} Connection;

#define CB_CHATMSG "chatmsg"
typedef void (*ChatMsgFunc)(Player* p, char* channel, char* text);

#define CB_PRIVMSG "privmsg"
typedef void (*PrivMsgFunc)(int sound, char* text);

#define CB_BROADCAST "broadcast"
typedef void (*BroadcastFunc)(Player* p, int sound, char* text);

#define CB_PRIVSQUADMSG "privsquadmsg"
typedef void (*PrivSquadMsgFunc)(char* sender, char* squad, int sound, char* text);

#define CB_SCORE "score"
typedef void (*ScoreFunc)(Player* p, Zone* z, PlayerScore* score);

#define CB_DEMOGRAPHICS "demographics"
typedef void (*DemographicsFunc)(Player* p, Demographics* dg);

#endif
