
#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;
local Inet *net;
local Icore *core;
local Iconfig *cfg;
local Izones *zd;
local Iplayers *pd;
local Icmdman *cmd;

local Player* getplayerbycid(Zone* z, int cid)
{
	Connection *c=NULL;
	FOR_EACH_LINK(z->connections,c,l)
	{
		if((c->z == z) && (c->ConnectionID == cid)) return c->p;
	}
	return NULL;
}

local void sendzoneloginresponse(Zone* z, LoginCode code)
{
	if(!z) return;

	B2SServerLogin sl;
	int size=sizeof(B2SServerLogin);
	memset(&sl,0,size);
	sl.Type=B2S_SERVER_LOGIN;
	sl.Result=code;
	sl.ServerID=z->ServerID;
	
	net->SendPacketToFrom(&(z->wi),LOC_BILLER,&sl,size);
}

local void sendplayerloginresponse(Player* p, int cid, LoginCode code)
{
	if(!p) return;

	B2SUserLogin ul;
	int size=sizeof(B2SUserLogin);
	memset(&ul,0,size);
	ul.Type=B2S_USER_LOGIN;
	ul.Result=code;
	ul.ConnectionID=cid;
	strncpy(ul.Name,p->name,24);
	
	switch(code) //magic switch
	{
		case B2S_LOGIN_OK:
		case B2S_LOGIN_NEWUSER:
		case B2S_LOGIN_ASKDEMOGRAPHICS:
		
			strncpy(ul.Squad,p->squad,24);
			memcpy(ul.Banner,p->Banner,BANNER_SIZE);
			
			//TODO FIXME: add the rest
		
		case B2S_LOGIN_INVALIDPW:
		case B2S_LOGIN_BANNED:
		case B2S_LOGIN_NONEWCONNS:
		case B2S_LOGIN_BADUSERNAME:
		case B2S_LOGIN_DEMOVERSION:
		case B2S_LOGIN_SERVERBUSY:
			//other stuff here, idk
		break;
	}
	
	net->SendPacketToFrom(&(p->z->wi),LOC_BILLER,&ul,size);
}

local void zoneauth(Zone* nz)
{
	//either this must be set or first zone has to be manually added to db
	//to allow biller op to connect and add more zones
	int bootstrap=cfg->GetInt(GLOBAL,"Security","InitialZone",0);
	int allowallzones=cfg->GetInt(GLOBAL,"Security","AllowAllZones",0);
	
	Zone* z=zd->FindZoneBySID(nz->ServerID);
	bool pwcorrect=false;
	if(z && (strcmp(nz->password,z->password) == 0)) pwcorrect=true;
	
	if(z && pwcorrect && z->banned)
	{
		lm->Log(L_MALICIOUS,"<billing> zone [%s:%u] is banned, access denied",nz->name,nz->ServerID);
		
		sendzoneloginresponse(z,B2S_LOGIN_BANNED);
	}
	if(z && pwcorrect)
	{
		//copy over old info
		strncpy(z->name,nz->name,SERVER_NAME_LEN);
		z->ServerID=nz->ServerID;
		z->ScoreID=nz->ScoreID;
		z->GroupID=nz->GroupID;
		z->reportedport=nz->reportedport;
		z->wi=nz->wi;
		z->lastdatatime=nz->lastdatatime;
		
		z->online=true;
		LLInit(&(z->connections)); //TODO FIXME: memory leak if zone drops out
		
		lm->Log(L_INFO,"<billing> access granted for zone [%s:%u]",z->name,z->ServerID);
		
		sendzoneloginresponse(z,B2S_LOGIN_OK);
		
		{ DO_CBS(CB_ZONEACTION,ZoneActionFunc,(z,ZA_CONNECT)); }
		
		zd->FreeZone(nz); //kill temp struct
	}
	else if(z)
	{
		lm->Log(L_MALICIOUS,"<billing> zone [%s:%u] failed password check, access denied",nz->name,nz->ServerID);
		
		sendzoneloginresponse(z,B2S_LOGIN_INVALIDPW);
	}
	else if(!z && !bootstrap && !allowallzones)
	{
		lm->Log(L_MALICIOUS,"<billing> zone [%s:%u] is not in db, access denied.",nz->name,nz->ServerID);
		
		sendzoneloginresponse(z,B2S_LOGIN_NONEWCONNS);
	}
	else if(!z && ((nz->ServerID == bootstrap) || allowallzones))
	{
		zd->Lock();
		LLAddLast(&zd->zonelist,nz);
		zd->Unlock();
		
		nz->online=true;
		
		lm->Log(L_INFO,"<billing> access granted for new zone [%s:%u]",nz->name,nz->ServerID);
		
		sendzoneloginresponse(z,B2S_LOGIN_NEWUSER);
		
		{ DO_CBS(CB_ZONEACTION,ZoneActionFunc,(nz,ZA_CREATE)); }
		{ DO_CBS(CB_ZONEACTION,ZoneActionFunc,(nz,ZA_CONNECT)); }
	}
}

local void playerauth(Player* np, int cid)
{
	char* firstplayer=cfg->GetStr(GLOBAL,"Security","InitialPlayer");
	int allowallplayers=cfg->GetInt(GLOBAL,"Security","AllowAllPlayers",1);
	
	bool isfirstplayer=(!strcasecmp(np->name,firstplayer));
	
	Player* p=pd->FindPlayerByName(np->name);
	bool pwcorrect=false;
	if(p && (strcmp(np->password,p->password) == 0)) pwcorrect=true;
	
	if(p && pwcorrect && p->banned)
	{
		lm->Log(L_MALICIOUS,"<billing> player [%s] is banned, access denied",np->name);
		
		sendplayerloginresponse(p,cid,B2S_LOGIN_BANNED);
	}
	else if(p && pwcorrect)
	{
		//copy over old info
		strncpy(p->name,np->name,USER_NAME_LEN);
		p->IPAddress=np->IPAddress;
		// p->ConnectionID=np->ConnectionID;
		p->MachineID=np->MachineID;
		p->Timezone=np->Timezone;
		p->ClientVersion=np->ClientVersion;
		
		// if(!p->online) //latest zone better than oldest
		p->z=np->z;
		
		Connection *c=amalloc(sizeof(Connection));
		c->p=p;
		c->z=p->z;
		c->ConnectionID=cid;
		LLAddFirst(&(p->connections),c);
		LLAddFirst(&(p->z->connections),c);
		
		p->online=true;
		
		lm->Log(L_INFO,"<billing> access granted to [%s]",p->name);
		
		sendplayerloginresponse(p,cid,B2S_LOGIN_OK);
		
		{ DO_CBS(CB_PLAYERACTION,PlayerActionFunc,(p,PA_CONNECT)); }
	
		pd->FreePlayer(np); //kill temp struct
	}
	else if(p)
	{
		lm->Log(L_MALICIOUS,"<billing> player [%s] failed password check, access denied",np->name);
		
		sendplayerloginresponse(p,cid,B2S_LOGIN_INVALIDPW);
	}
	else if(!p && !isfirstplayer && !allowallplayers)
	{
		lm->Log(L_MALICIOUS,"<billing> player [%s] is not in db, access denied",np->name);
		
		sendplayerloginresponse(p,cid,B2S_LOGIN_NONEWCONNS);
	}
	else if(!p && (isfirstplayer || allowallplayers))
	{
		pd->Lock();
		LLAddLast(&pd->playerlist,np);
		pd->Unlock();
		
		Connection *c=amalloc(sizeof(Connection));
		c->p=np;
		c->z=np->z;
		c->ConnectionID=cid;
		LLAddFirst(&(np->connections),c);
		LLAddFirst(&(np->z->connections),c);
		
		np->online=true;
		
		lm->Log(L_INFO,"<billing> access granted to new player %s",np->name);
		
		sendplayerloginresponse(p,cid,B2S_LOGIN_NEWUSER);
		
		{ DO_CBS(CB_PLAYERACTION,PlayerActionFunc,(np,PA_CREATE)); }
		{ DO_CBS(CB_PLAYERACTION,PlayerActionFunc,(np,PA_CONNECT)); }
	}
}

local void zonedbresponse(Zone* oldz, bool loaded)
{
	if(!loaded) return; //default handler for no db rejects all connections
	
	zoneauth(oldz);
}

local void playerdbresponse(Player* oldp, int cid, bool loaded)
{
	if(!loaded) return; //default handler for no db rejects all connections
	
	playerauth(oldp,cid);
}

local void Disconnect(NetLoc loc, WebInfo* wi)
{
	if(loc != LOC_BILLER)
	{
		lm->Log(L_MALICIOUS,"<billing> got disconnect on bad loc somehow: %u",loc);
		return;
	}
	
	Zone* z=zd->FindZoneByWI(wi);
	if(z)
	{
		lm->Log(L_INFO,"<billing> disconnect from [%s:%u]",z->name,z->ServerID);
		
		z->lastdatatime=current_ticks();
		z->online=false;
		{ DO_CBS(CB_ZONEACTION,ZoneActionFunc,(z,ZA_DISCONNECT)); }
		//TODO FIXME: will double fire on zones that arent dicks and use S2B_SERVER_LOGOFF properly
	}
	else lm->Log(L_DRIVEL,"<billing> disconnect from unconnected zone");
}

local bool packethandler(NetLoc loc, Byte *pkt, int len, WebInfo* wi)
{
	if(loc != LOC_BILLER)
	{
		lm->Log(L_MALICIOUS,"<billing> got packet on bad loc somehow: %u",loc);
		return false;
	}
	
	switch(pkt[0])
	{
		case S2B_PING:
		{
#ifdef MAKE_BILLING_PRINT_PACKETS
			// lm->Log(L_DRIVEL,"<billing> ping len=%u",len);
#endif
			Zone* z=zd->FindZoneByWI(wi);
			if(z) z->lastdatatime=current_ticks();
			
			return true;
		}
		break;
		case S2B_SERVER_LOGIN:
		{
#ifdef MAKE_BILLING_PRINT_PACKETS
			lm->Log(L_DRIVEL,"<billing> connect len=%u",len);
#endif
			if(len < sizeof(S2BServerLogin))
			{
				lm->Log(L_MALICIOUS,"<billing> got server connect request on %u but is too small %u < %u",loc,len,sizeof(S2BServerLogin));
				return false;
			}
			
			S2BServerLogin *sc=(S2BServerLogin*)pkt;
			
			// Zone z2;
			// Zone* z=&z2;
			// memset(z,0,sizeof(Zone));
			Zone* z=zd->NewZone(); //create temp struct to check against
			
			strncpy(z->name,sc->ServerName,SERVER_NAME_LEN);
			strncpy(z->password,sc->Password,SERVER_PW_LEN);
			z->ServerID=sc->ServerID;
			z->ScoreID=sc->ScoreID;
			z->GroupID=sc->GroupID;
			z->reportedport=sc->Port;
			
			memcpy(&(z->wi),wi,sizeof(WebInfo));
			z->lastdatatime=current_ticks();
			
			lm->Log(L_INFO,"<billing> connect attempt: [%s:%u]",z->name,z->ServerID);
			
			Zone* check=zd->FindZoneBySID(z->ServerID);
			if(!check)
			{
				//zone not loaded yet, load from db and check again
				{ DO_CBS(CB_LOADZONE,LoadZoneFunc,(z,zonedbresponse)); }
			}
			else zoneauth(z);
			
			return true;
		}
		break;
		case S2B_SERVER_LOGOFF:
		{
#ifdef MAKE_BILLING_PRINT_PACKETS
			lm->Log(L_DRIVEL,"<billing> disconnect len=%u",len);
#endif
			Zone* z=zd->FindZoneByWI(wi);
			if(!z)
			{
				lm->Log(L_MALICIOUS,"<billing> got server logoff request from unconnected zone");
				return true;
			}
			
			z->lastdatatime=current_ticks();
			z->online=false;
			
			lm->Log(L_DRIVEL,"<billing> zone [%s:%u] is disconnecting",z->name,z->ServerID);
			
			{ DO_CBS(CB_ZONEACTION,ZoneActionFunc,(z,ZA_DISCONNECT)); }
			
			return true;
		}
		break;
		case S2B_USER_LOGIN:
		{
#ifdef MAKE_BILLING_PRINT_PACKETS
			lm->Log(L_DRIVEL,"<billing> login len=%u",len);
#endif
			int sizecheck=offsetof(S2BUserLogin,ClientExtraData);
			if(len < sizecheck)
			{
				lm->Log(L_MALICIOUS,"<billing> got user login request on %u but is too small %u < %u",loc,len,sizecheck);
				return false;
			}
			
			Zone* z=zd->FindZoneByWI(wi);
			if(!z)
			{
				lm->Log(L_MALICIOUS,"<billing> got user login request from unconnected zone");
				return true;
			}
			
			S2BUserLogin *ul=(S2BUserLogin*)pkt;
			
			Player *p=pd->NewPlayer(); //create temp struct to check against
			
			strncpy(p->name,ul->Name,USER_NAME_LEN);
			strncpy(p->password,ul->Password,USER_PW_LEN);
			p->IPAddress=ul->IPAddress;
			// p->ConnectionID=ul->ConnectionID;
			p->MachineID=ul->MachineID;
			p->Timezone=ul->Timezone;
			p->ClientVersion=ul->ClientVersion;
			
			lm->Log(L_INFO,"<billing> login attempt for [%s] as %u",p->name,ul->ConnectionID);
				
			p->z=z; //TODO FIXME: user can connect to multiple zones
			
			Player* check=pd->FindPlayerByName(p->name);
			if(!check)
			{
				//player not loaded yet, load from db and check again
				{ DO_CBS(CB_LOADPLAYER,LoadPlayerFunc,(p,ul->ConnectionID,playerdbresponse)); }
			}
			else playerauth(p,ul->ConnectionID);
			
			// ConnectionID++;
			
			return true;
		}
		break;
		case S2B_USER_LOGOFF:
		{
#ifdef MAKE_BILLING_PRINT_PACKETS
			lm->Log(L_DRIVEL,"<billing> user logoff len=%u",len);
#endif
			int sizecheck=offsetof(S2BUserLogoff,Score);
			if(len < sizecheck)
			{
				lm->Log(L_MALICIOUS,"<billing> got user logoff request on %u but is too small %u < %u",loc,len,sizecheck);
				return false;
			}
			
			Zone* z=zd->FindZoneByWI(wi);
			if(!z)
			{
				lm->Log(L_MALICIOUS,"<billing> got user logoff request from unconnected zone");
				return true;
			}
			
			S2BUserLogoff *ul=(S2BUserLogoff*)pkt;
			
			Player* p=getplayerbycid(z,ul->ConnectionID);
			if(!p)
			{
				lm->Log(L_MALICIOUS,"<billing> got user logoff request from unconnected user",ul->ConnectionID);
				return true;
			}
			
			if(len == sizeof(S2BUserLogoff))
				{ DO_CBS(CB_SCORE,ScoreFunc,(p,z,&(ul->Score))); }
			
			lm->Log(L_INFO,"<billing> user logoff reason=%u",ul->DisconnectReason);
				
			LLRemoveAll(&(p->connections),z);
			if(LLIsEmpty(&(p->connections))) p->online=false;
			
			return true;
		}
		break;
		case S2B_USER_TEXT_PRIV:
		{
#ifdef MAKE_BILLING_PRINT_PACKETS
			lm->Log(L_DRIVEL,"<billing> broadcast len=%u",len);
#endif
			int sizecheck=offsetof(S2BUserTextPrivate,Text);
			if(len < sizecheck)
			{
				lm->Log(L_MALICIOUS,"<billing> got broadcast on %u but is too small %u < %u",loc,len,sizecheck);
				return false;
			}
			
			Zone* z=zd->FindZoneByWI(wi);
			if(!z)
			{
				lm->Log(L_MALICIOUS,"<billing> got broadcast from unconnected zone");
				return true;
			}
			
			S2BUserTextPrivate *upc=(S2BUserTextPrivate*)pkt;
			upc->Text[TEXT_LEN-1]=0; //just in case

			//TODO: figure out the mystery of connectionid
			//TODO: implement groupid zone groups

			int cid=upc->ConnectionID;
			if(cid >= 0)
			{
				Player* p=getplayerbycid(z,cid);
				if(!p)
				{
					lm->Log(L_MALICIOUS,"<billing> got broadcast from unconnected user %u",cid);
					return true;
				}
				
				{ DO_CBS(CB_BROADCAST,BroadcastFunc,(p,upc->Sound,upc->Text)); }
			}
			else
			{
				// lm->Log(L_DRIVEL,"<billing> priv: cid=%u gid=%u st=%u s=%u \nmsg=%s",
					// upc->ConnectionID,upc->GroupID,upc->SubType,upc->Sound,upc->Text);
					
				{ DO_CBS(CB_PRIVMSG,PrivMsgFunc,(upc->Sound,upc->Text)); }
			}
			
			return true;
		}
		break;
		case S2B_USER_DEMOGRAPHICS:
		{
#ifdef MAKE_BILLING_PRINT_PACKETS
			lm->Log(L_DRIVEL,"<billing> demographics len=%u",len);
#endif
			if(len < sizeof(S2BUserDemographics))
			{
				lm->Log(L_MALICIOUS,"<billing> got score on %u but is too small %u < %u",loc,len,sizeof(S2BUserDemographics));
				return false;
			}
			
			Zone* z=zd->FindZoneByWI(wi);
			if(!z)
			{
				lm->Log(L_MALICIOUS,"<billing> got demographics from unconnected zone");
				return true;
			}
			
			S2BUserDemographics *ud=(S2BUserDemographics*)pkt;
			
			{ DO_CBS(CB_DEMOGRAPHICS,DemographicsFunc,(getplayerbycid(z,ud->ConnectionID),&ud->dg)); }
			
			return true;
		}
		break;
		case S2B_USER_BANNER:
		{
#ifdef MAKE_BILLING_PRINT_PACKETS
			lm->Log(L_DRIVEL,"<billing> banner len=%u",len);
#endif
			if(len < sizeof(S2BUserBanner))
			{
				lm->Log(L_MALICIOUS,"<billing> got banner on %u but is too small %u < %u",loc,len,sizeof(S2BUserBanner));
				return false;
			}
			
			Zone* z=zd->FindZoneByWI(wi);
			if(!z)
			{
				lm->Log(L_MALICIOUS,"<billing> got banner from unconnected zone");
				return true;
			}
			
			S2BUserBanner *ub=(S2BUserBanner*)pkt;
			
			Player* p=getplayerbycid(z,ub->ConnectionID);
			if(!p)
			{
				lm->Log(L_MALICIOUS,"<billing> got banner from unconnected user %u",ub->ConnectionID);
				return true;
			}
			
			memcpy(p->Banner,ub->Data,BANNER_SIZE);
			lm->Log(L_INFO,"<billing> updated banner for %s",p->name);
			
			return true;
		}
		break;
		case S2B_USER_SCORE:
		{
#ifdef MAKE_BILLING_PRINT_PACKETS
			lm->Log(L_DRIVEL,"<billing> score len=%u",len);
#endif
			if(len < sizeof(S2BUserScore))
			{
				lm->Log(L_MALICIOUS,"<billing> got score on %u but is too small %u < %u",loc,len,sizeof(S2BUserScore));
				return false;
			}
			
			Zone* z=zd->FindZoneByWI(wi);
			if(!z)
			{
				lm->Log(L_MALICIOUS,"<billing> got score from unconnected zone");
				return true;
			}
			
			S2BUserScore *us=(S2BUserScore*)pkt;
			
			Player* p=getplayerbycid(z,us->ConnectionID);
			if(!p)
			{
				lm->Log(L_MALICIOUS,"<billing> got score from unconnected user %u",us->ConnectionID);
				return true;
			}
			
			// lm->Log(L_INFO,"<billing> score for [%s]: %u/%u (%u,%u,%u)",
				// p->name,us->Score.Kills,us->Score.Deaths,
				// us->Score.Flags,us->Score.Score,us->Score.FlagScore);
			
			{ DO_CBS(CB_SCORE,ScoreFunc,(p,z,&(us->Score))); }
			
			return true;
		}
		break;
		case S2B_USER_COMMAND:
		{
#ifdef MAKE_BILLING_PRINT_PACKETS
			lm->Log(L_DRIVEL,"<billing> user command len=%u",len);
#endif
			int sizecheck=offsetof(S2BUserCommand,Text);
			if(len < sizecheck)
			{
				lm->Log(L_MALICIOUS,"<billing> got command on %u but is too small %u < %u",loc,len,sizecheck);
				return false;
			}
			
			Zone* z=zd->FindZoneByWI(wi);
			if(!z)
			{
				lm->Log(L_MALICIOUS,"<billing> got command from unconnected zone");
				return true;
			}
			
			S2BUserCommand *uc=(S2BUserCommand*)pkt;
			uc->Text[TEXT_LEN-1]=0; //just in case
			
			Player* p=getplayerbycid(z,uc->ConnectionID);
			if(!p)
			{
				lm->Log(L_MALICIOUS,"<billing> got command from unconnected user %u",uc->ConnectionID);
				return true;
			}
			
			lm->Log(L_INFO,"<billing> command attempt from [%s]: %s",
				p->name,
				uc->Text);
				
			cmd->Command(uc->Text,p,0);
			
			return true;
		}
		break;
		case S2B_USER_TEXT_CHAT:
		{
#ifdef MAKE_BILLING_PRINT_PACKETS
			lm->Log(L_DRIVEL,"<billing> chat len=%u",len);
#endif
			int sizecheck=offsetof(S2BUserTextChat,Text);
			if(len < sizecheck)
			{
				lm->Log(L_MALICIOUS,"<billing> got chat on %u but is too small %u < %u",loc,len,sizecheck);
				return false;
			}
			
			Zone* z=zd->FindZoneByWI(wi);
			if(!z)
			{
				lm->Log(L_MALICIOUS,"<billing> got chat from unconnected zone");
				return true;
			}
			
			S2BUserTextChat *utc=(S2BUserTextChat*)pkt;
			utc->Text[TEXT_LEN-1]=0; //just in case
			utc->ChannelName[CHAT_NAME_LEN-1]=0; //just in case
			
			Player* p=getplayerbycid(z,utc->ConnectionID);
			if(!p)
			{
				lm->Log(L_MALICIOUS,"<billing> got chat from unconnected user %u",utc->ConnectionID);
				return true;
			}
			
			// lm->Log(L_DRIVEL,"<billing> chat from [%s] on (%s): %s",p->name,utc->ChannelName,utc->Text);
				
			{ DO_CBS(CB_CHATMSG,ChatMsgFunc,(p,utc->ChannelName,utc->Text)); }
			
			return true;
		}
		break;
		case S2B_SERVER_CAPABILITIES:
		{
#ifdef MAKE_BILLING_PRINT_PACKETS
			lm->Log(L_DRIVEL,"<billing> capabilities len=%u",len);
#endif
			if(len < sizeof(S2BServerCapabilities))
			{
				lm->Log(L_MALICIOUS,"<billing> got capabilities on %u but is too small %u < %u",loc,len,sizeof(S2BServerCapabilities));
				return false;
			}
			
			Zone* z=zd->FindZoneByWI(wi);
			if(!z) return true;
			
			z->lastdatatime=current_ticks();
			
			S2BServerCapabilities *sc=(S2BServerCapabilities*)pkt;
			z->MultiCastChat=sc->MultiCastChat;
			z->SupportDemographics=sc->SupportDemographics;
			
			lm->Log(L_DRIVEL,"<billing> got capabilities from [%s:%u]",z->name,z->ServerID);
			
			return true;
		}
		break;
		default:
			lm->Log(L_DRIVEL,"<billing> unhandled packet type: type=%02x len=%u",pkt[0],len);
	}
	
	return false;
}

EXPORT char* info_billing=
"Billing Protocol Module\n"
"by Cheese\n"
"Dispatches the billing protocol.";

EXPORT int MM_billing(Imodman *mm2, int action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		net=mm->GetInterface(I_NET);
		core=mm->GetInterface(I_CORE);
		cfg=mm->GetInterface(I_CONFIG);
		zd=mm->GetInterface(I_ZONES);
		pd=mm->GetInterface(I_PLAYERS);
		cmd=mm->GetInterface(I_CMDMAN);
		if(!lm || !net || !core || !cfg || !zd || !pd || !cmd) return MM_FAIL;
		
		int port=cfg->GetInt(GLOBAL,"Listen","ServersPort",9001);
		net->CreateNewSocket(LOC_BILLER,true);
		net->CreateNewEndpoint(LOC_BILLER,cfg->GetStr(GLOBAL,"Listen","ServersIP"),port);
		net->ConnectToLocation(LOC_BILLER);
		core->UseCoreProtocol(LOC_BILLER);
		
		mm->RegCallback(CB_DISCONNECT,Disconnect);
		
		net->AddPacket(LOC_BILLER,S2B_PING,packethandler);
		net->AddPacket(LOC_BILLER,S2B_SERVER_LOGIN,packethandler);
		net->AddPacket(LOC_BILLER,S2B_SERVER_LOGOFF,packethandler);
		net->AddPacket(LOC_BILLER,S2B_USER_LOGIN,packethandler);
		net->AddPacket(LOC_BILLER,S2B_USER_LOGOFF,packethandler);
		net->AddPacket(LOC_BILLER,S2B_USER_TEXT_PRIV,packethandler);
		net->AddPacket(LOC_BILLER,S2B_USER_DEMOGRAPHICS,packethandler);
		net->AddPacket(LOC_BILLER,S2B_USER_BANNER,packethandler);
		net->AddPacket(LOC_BILLER,S2B_USER_SCORE,packethandler);
		net->AddPacket(LOC_BILLER,S2B_USER_COMMAND,packethandler);
		net->AddPacket(LOC_BILLER,S2B_USER_TEXT_CHAT,packethandler);
		net->AddPacket(LOC_BILLER,S2B_SERVER_CAPABILITIES,packethandler);
		
		lm->Log(L_INFO,"<billing> Listening for servers on port %u",port);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		core->StopUsingCoreProtocol(LOC_BILLER);
		
		net->RemovePacket(LOC_BILLER,S2B_PING,packethandler);
		net->RemovePacket(LOC_BILLER,S2B_SERVER_LOGIN,packethandler);
		net->RemovePacket(LOC_BILLER,S2B_SERVER_LOGOFF,packethandler);
		net->RemovePacket(LOC_BILLER,S2B_USER_LOGIN,packethandler);
		net->RemovePacket(LOC_BILLER,S2B_USER_LOGOFF,packethandler);
		net->RemovePacket(LOC_BILLER,S2B_USER_TEXT_PRIV,packethandler);
		net->RemovePacket(LOC_BILLER,S2B_USER_DEMOGRAPHICS,packethandler);
		net->RemovePacket(LOC_BILLER,S2B_USER_BANNER,packethandler);
		net->RemovePacket(LOC_BILLER,S2B_USER_SCORE,packethandler);
		net->RemovePacket(LOC_BILLER,S2B_USER_COMMAND,packethandler);
		net->RemovePacket(LOC_BILLER,S2B_USER_TEXT_CHAT,packethandler);
		net->RemovePacket(LOC_BILLER,S2B_SERVER_CAPABILITIES,packethandler);
		
		mm->UnregCallback(CB_DISCONNECT,Disconnect);
		
		mm->ReleaseInterface(cmd);
		mm->ReleaseInterface(pd);
		mm->ReleaseInterface(zd);
		mm->ReleaseInterface(cfg);
		mm->ReleaseInterface(core);
		mm->ReleaseInterface(net);
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}
