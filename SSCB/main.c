
#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;
local Imainloop *ml;

local int finder(char *dest, int destlen, char *ar, char *name)
{
	astrncpy(dest,name,destlen);
	return 0;
}

local void error(char *err)
{
	Error(EXIT_MODLOAD,"Error in modules: %s",err);
}

int main(int argc, char *argv[])
{
	printf("\n%s\nVersion %s\n\n",EXE_NAME,EXE_VERSION);
	
	int code=0;

	srand(current_ticks());

	mm=InitModuleManager();
	RegCModLoader(mm);

	printf("%c%c <main> Loading modules\n",L_EXE,L_INFO);
	
	char line[256];
	int ret;
	APPContext *ctx;
	ctx=APPInitContext(finder,error,NULL);
	APPAddFile(ctx,"conf/modules.conf");
	while(APPGetLine(ctx,line,256))
	{
		ret=mm->LoadModule(line);
		if(ret == MM_FAIL) Error(EXIT_MODLOAD,"Error in loading module '%s'",line);
	}
	APPFreeContext(ctx);
	
	lm=mm->GetInterface(I_LOGMAN);
	ml=mm->GetInterface(I_MAINLOOP);

	if(!lm) Error(EXIT_MODLOAD,"<main> Logman module missing");
	if(!ml) Error(EXIT_MODLOAD,"<main> Mainloop module missing");

	lm->Log(L_SYNC|L_INFO,"<main> Running main loop");

	code=ml->RunLoop();

	lm->Log(L_SYNC|L_INFO,"<main> Exited main loop, unloading modules");

	mm->ReleaseInterface(lm);
	mm->ReleaseInterface(ml);

	mm->UnloadAllModules();

	UnregCModLoader();
	DeInitModuleManager(mm);
	
#if PLATFORM == PLATFORM_WINDOWS
	//so console window stays open
	system("PAUSE");
#endif

	return code;
}

