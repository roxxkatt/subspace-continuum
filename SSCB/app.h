
#ifndef __APP_H
#define __APP_H

typedef struct APPContext APPContext;

typedef int (*APPFileFinderFunc)(char *dest, int destlen, char *arena, char *name);
typedef void (*APPReportErrFunc)(char *error);


APPContext *APPInitContext(APPFileFinderFunc finder, APPReportErrFunc err, char *arena);
void APPFreeContext(APPContext *ctx);

void APPAddDef(APPContext *ctx, char *key, char *val);
void APPRemoveDef(APPContext *ctx, char *key);

void APPAddFile(APPContext *ctx, char *name);

/* returns false on eof */
int APPGetLine(APPContext *ctx, char *buf, int buflen);

#endif

