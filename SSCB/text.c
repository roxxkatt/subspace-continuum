
#include "ssc.h"

local Imodman *mm;
local Inet *net;
// local Izonedata *zd;
// local Iplayerdata *pd;

#define BUFSIZE 4096

local void SendCommandResponse(Player *p, const char *format, ...)
{
	if(!p || !format) return;
	
	va_list args;
	va_start(args,format);
	
	char buf[BUFSIZE];
	vsnprintf(buf,BUFSIZE,format,args);
	
	char *line=strtok(buf,"\n");
	while(line)
	{
		if(strlen(line))
		{
			B2SUserTextCommand utc;
			int size=sizeof(B2SUserTextCommand);
			memset(&utc,0,size);
			
			Connection* c=(LLMGetHead(&(p->connections)))->data; //TODO FIXME HACK: will crash if empty
			utc.Type=B2S_USER_TEXT_CMD;
			utc.ConnectionID=c->ConnectionID;
			strncpy(utc.Text,line,256);
			
			net->SendPacketToFrom(&(p->z->wi),LOC_BILLER,&utc,size);
		}
		line=strtok(NULL,"\n");
	}
	
	va_end(args);
}

local void SendPrivate(Player *p, int sound, const char *format, ...)
{
	if(!p || !format) return;
	
	va_list args;
	va_start(args,format);
	
	char buf[BUFSIZE];
	vsnprintf(buf,BUFSIZE,format,args);
	
	char *line=strtok(buf,"\n");
	while(line)
	{
		if(strlen(line))
		{
			B2SUserTextPrivate utp;
			int size=sizeof(B2SUserTextPrivate);
			memset(&utp,0,size);
			
			//TODO: test different subtypes, unblock subtype in asss first
			utp.Type=B2S_USER_TEXT_PRIV;
			utp.SourceServerID=0;  //FIXME
			utp.SubType=2;
			utp.Sound=sound;
			strncpy(utp.Text,line,256);
	
			net->SendPacketToFrom(&(p->z->wi),LOC_BILLER,&utp,size);
		}
		line=strtok(NULL,"\n");
	}
	
	va_end(args);
}

local void SendChat(Player *p, int chnum, const char *format, ...)
{
	if(!p || !format) return;
	
	va_list args;
	va_start(args,format);
	
	char buf[BUFSIZE];
	vsnprintf(buf,BUFSIZE,format,args);
	
	char *line=strtok(buf,"\n");
	while(line)
	{
		if(strlen(line))
		{
			B2SUserTextChat utc;
			int size=sizeof(B2SUserTextChat);
			memset(&utc,0,size);
			
			Connection* c=(LLMGetHead(&(p->connections)))->data; //TODO FIXME HACK: will crash if empty
			utc.Type=B2S_USER_TEXT_CHAT;
			utc.ConnectionID=c->ConnectionID;
			utc.ChannelNum=chnum;
			strncpy(utc.Text,line,256);
			
			net->SendPacketToFrom(&(p->z->wi),LOC_BILLER,&utc,size);
		}
		line=strtok(NULL,"\n");
	}
	
	va_end(args);
}

local void SendColorMsgP(Player *p, ColorCode color, const char *format, ...)
{
	if(!p || !format) return;
	
	va_list args;
	va_start(args,format);
	
	char buf[BUFSIZE];
	vsnprintf(buf,BUFSIZE,format,args);
	
	char *line=strtok(buf,"\n");
	while(line)
	{
		if(strlen(line))
		{
			B2SUserPacket up;
			int size=offsetof(B2SUserPacket,Data)+sizeof(ChatPacket);
			memset(&up,0,size);
			
			Connection* c=(LLMGetHead(&(p->connections)))->data; //TODO FIXME HACK: will crash if empty
			up.Type=B2S_USER_TEXT_CMD;
			up.ConnectionID=c->ConnectionID;
			
			ChatPacket *cp=(ChatPacket*)up.Data;
			cp->type=S2C_CHAT;
			cp->kind=color;
			cp->sound=0; //TODO add
			cp->pid=0;
			strncpy(cp->text,line,256);
			
			size+=strlen(cp->text);
			
			net->SendPacketToFrom(&(p->z->wi),LOC_BILLER,&up,size);
		}
		line=strtok(NULL,"\n");
	}
	
	va_end(args);
}

local void SendColorMsgZ(Zone* z, ColorCode color, const char *format, ...)
{
	//TODO implement
}

local void SendBroadcast(Zone* z, int sound, const char *format, ...)
{
	if(!z || !format) return;
	
	va_list args;
	va_start(args,format);
	
	char buf[BUFSIZE];
	vsnprintf(buf,BUFSIZE,format,args);
	
	char *line=strtok(buf,"\n");
	while(line)
	{
		if(strlen(line))
		{
			B2SUserTextPrivate utp;
			int size=sizeof(B2SUserTextPrivate);
			memset(&utp,0,size);
			
			//TODO FIXME: replace with its own packet
			utp.Type=B2S_USER_TEXT_PRIV;
			utp.SourceServerID=0;  //TODO FIXME: implement
			utp.SubType=2;
			utp.Sound=sound;
			strncpy(utp.Text,line,256);
	
			net->SendPacketToFrom(&(z->wi),LOC_BILLER,&utp,size);
		}
		line=strtok(NULL,"\n");
	}
	
	va_end(args);
}

local Itext textint=
{
	INTERFACE_HEAD_INIT(I_TEXT,"text"),
	SendCommandResponse,SendPrivate,SendChat,
	SendColorMsgP,SendColorMsgZ,
	SendBroadcast
};

EXPORT char* info_text=
"Text Module\n"
"by Cheese\n"
"Allows modules to send text to users.";

EXPORT ModReturn MM_text(Imodman *mm2, ModAction action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		// lm=mm->GetInterface(I_LOGMAN);
		net=mm->GetInterface(I_NET);
		if(!net) return MM_FAIL;
		
		mm->RegInterface(&textint);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&textint)) return MM_FAIL;
		
		mm->ReleaseInterface(net);
		
		return MM_OK;
	}
	return MM_FAIL;
}

