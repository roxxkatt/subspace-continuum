
#ifndef __BAN_H
#define __BAN_H

typedef struct Ban
{
	int id;
	int db_id;
	
	bool active;
	bool temporary; //no save to db
	Ticks created;
	Ticks duration;
	
	//each can be null or zero if metric not used
	// Player *p;
	char name[USER_NAME_LEN];
	u32 IPAddress;
	u32 MachineID;
	u32 IPStart;
	u32 IPEnd;
	char creator[USER_NAME_LEN];
	char reason[250];
	
	Byte extradata[0];
} Ban;

typedef enum
{
	BA_CREATE,
	BA_LOAD,
	BA_UNLOAD,
	BA_DESTROY
} BanAction;

#define CB_BANACTION "banaction"
typedef void (*BanActionFunc)(Ban *b, BanAction action);

#define CB_LOADBAN "loadban"
typedef void (*LoadBanResponseFunc)(Ban *b, bool loaded);
typedef void (*BanFunc)(Ban *b, LoadBanResponseFunc f);

#define CB_BANPOINTER "banpointer"
typedef void (*BanPointerFunc)(Ban *b, bool isnew);

#define I_BANS "bans"

typedef struct Ibans
{
	INTERFACE_HEAD_DECL;

	Ban* (*NewBan)(void);
	void (*FreeBan)(Ban *b);
	Size (*AllocateBanData)(Size bytes);
	void (*FreeBanData)(int key);
	void (*Lock)(void);
	void (*Unlock)(void);
	
	Ban* (*FindBanByID)(int id);
	Ban* (*FindBanByName)(char *name);
	Ban* (*FindBanByMachine)(u32 MachineID);
	Ban* (*FindBanByIP)(u32 IPAddress);
	
	LinkedList banlist;
	
} Ibans;

#define FOR_EACH_BAN(b) \
	for( \
		link=LLGetHead(&bd->banlist); \
		link && ((b=link->data, link=link->next) || 1); )

#define FOR_EACH_BAN_DATA(b,d,key) \
	for( \
		link=LLGetHead(&bd->banlist); \
		link && ((b=link->data, \
		          d=GETDATA(b,key), \
		          link=link->next) || 1); )

#define FOR_EACH_BAN_REVERSE(b) \
	for( \
		link=LLGetTail(&bd->banlist); \
		link && ((b=link->data, link=link->prev) || 1); )

#endif
