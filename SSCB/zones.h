
#ifndef __ZONE_H
#define __ZONE_H

typedef struct Zone
{
	int id;
	int db_id;
	
	char name[SERVER_NAME_LEN];
	char password[SERVER_PW_LEN];
	int ServerID;
	int ScoreID;
	int GroupID;
	int reportedport;
	
	bool MultiCastChat;
	bool SupportDemographics;
	
	// int status;
	// bool authenticated;
	bool online;
	int connecttime;
	int lastdatatime;
	LinkedList connections;
	bool banned;
	WebInfo wi;
	
	Byte extradata[0];
} Zone;

#define z_ip wi->sin_addr.s_addr;
#define z_port wi->port;

typedef enum
{
	ZA_CREATE,
	ZA_CONNECT,
	ZA_DISCONNECT,
	ZA_DESTROY
} ZoneAction;

#define CB_ZONEACTION "zoneaction"
typedef void (*ZoneActionFunc)(Zone *p, ZoneAction action);

#define CB_LOADZONE "loadzone"
typedef void (*LoadZoneResponseFunc)(Zone* z, bool loaded);
typedef void (*LoadZoneFunc)(Zone *z, LoadZoneResponseFunc f);

#define CB_ZONEPOINTER "zonepointer"
typedef void (*ZonePointerFunc)(Zone *p, int isnew);

#define I_ZONES "zones"

typedef struct Izones
{
	INTERFACE_HEAD_DECL;

	Zone* (*NewZone)(void);
	void (*FreeZone)(Zone *p);
	Size (*AllocateZoneData)(Size bytes);
	void (*FreeZoneData)(int key);
	void (*Lock)(void);
	void (*Unlock)(void);
	
	Zone* (*FindZoneByID)(int id);
	Zone* (*FindZoneByName)(char *name);
	Zone* (*FindZoneBySID)(int sid);
	Zone* (*FindZoneByWI)(WebInfo* wi);
	void (*KickZone)(Zone *p);
	
	// LinkedList waitingforauth;
	LinkedList zonelist;
	
} Izones;

#define FOR_EACH_ZONE(z) \
	for( \
		link=LLGetHead(&zd->zonelist); \
		link && ((z=link->data, link=link->next) || 1); )

#define FOR_EACH_ZONE_DATA(z,d,key) \
	for( \
		link=LLGetHead(&zd->zonelist); \
		link && ((z=link->data, \
		          d=GETDATA(z,key), \
		          link=link->next) || 1); )

#define FOR_EACH_UNAUTHED_ZONE(z) \
	for( \
		link=LLGetHead(&zd->waitingforauth); \
		link && ((z=link->data, link=link->next) || 1); )

#endif
