
#ifndef __CMDMAN_H
#define __CMDMAN_H

typedef void (*CommandFunc)(Player *p, char *command, char *params);

typedef const char* helptext_t;

#define I_CMDMAN "cmdman"

typedef struct Icmdman
{
	INTERFACE_HEAD_DECL;
	
	void (*AddCommand)(char *cmdname, CommandFunc func, helptext_t ht);
	void (*RemoveCommand)(char *cmdname, CommandFunc func);
	void (*AddUnlogged)(char *cmdname);
	void (*RemoveUnlogged)(char *cmdname);
	void (*Command)(char *typedline, Player *p, int sound);
	helptext_t (*GetHelpText)(char *cmdname);
} Icmdman;

#endif

