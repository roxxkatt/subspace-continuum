
#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;
local Imainloop *ml;
local Isql *sql;
local Iconfig *cfg;
local Izones *zd;
local Iplayers *pd;
local Iscores *scd;
local Isquads *sqd;
local Ichats *cd;
local Ibans *bd;

typedef struct cbpass
{
	//TODO FIXME: unions + memory leaks
	Zone *z;
	LoadZoneResponseFunc zf;
	Player *p;
	int cid;
	LoadPlayerResponseFunc pf;
	Score *sc;
	LoadScoreResponseFunc scf;
	Squad *sq;
	LoadSquadResponseFunc sqf;
	Chat *c;
	LoadChatResponseFunc cf;
	Ban *b;
	LoadBanResponseFunc bf;
} cbpass;

local void loadzoneresult(int status, db_res* res, void* vp)
{
	if(status)
	{
		lm->Log(L_WARN,"<data_sql> error getting zone from database: %u",status);
		return;
	}
		
	cbpass *cbp=vp;
	
	int rows=sql->GetRowCount(res);
	int cols=sql->GetFieldCount(res);
	
	int num=0;
	int i=0;
	for(i=0; i < rows; i++)
	{
		db_row* row=sql->GetRow(res);
		
		//TODO FIXME: can only load 1 data module atm
		Zone* z=zd->NewZone(); //create actual zone list entry
		//populate data
		z->db_id=aatoi(sql->GetField(row,0));
		strncpy(z->name,sql->GetField(row,1),SERVER_NAME_LEN);
		strncpy(z->password,sql->GetField(row,2),SERVER_PW_LEN);
		z->ServerID=aatoi(sql->GetField(row,3));
		
		zd->Lock();
		LLAddLast(&zd->zonelist,z);
		zd->Unlock();
		
		num++;
	}
	lm->Log(L_DRIVEL,"<data_sql> got %u zone from database",num);
		
	cbp->zf(cbp->z,TRUE); //inform billing module there is something to check against now
}

local void LoadZone(Zone *z, LoadZoneResponseFunc f)
{
	if(!z) return;
	
	cbpass *cbp=amalloc(sizeof(cbpass));
	cbp->z=z;
	cbp->zf=f;
	sql->Query(loadzoneresult,cbp,true,"SELECT ZoneID, Name, Password, ServerID FROM Zones WHERE ServerID=# LIMIT 1",z->ServerID);
}

local void loadplayerresult(int status, db_res* res, void* vp)
{
	if(status)
	{
		lm->Log(L_WARN,"<data_sql> error getting zone from database: %u",status);
		return;
	}
	
	cbpass *cbp=vp;
	
	int rows=sql->GetRowCount(res);
	int cols=sql->GetFieldCount(res);
	
	int num=0;
	int i=0;
	for(i=0; i < rows; i++)
	{
		db_row* row=sql->GetRow(res);
		
		char buf[BANNER_SIZE*2+1];
		//TODO FIXME: can only load 1 data module atm
		Player *p=pd->NewPlayer(); //create actual zone list entry
		//TODO FIXME: populate data
		p->db_id=aatoi(sql->GetField(row,0));
		strncpy(p->name,sql->GetField(row,1),USER_NAME_LEN);
		strncpy(p->password,sql->GetField(row,2),USER_PW_LEN);
		strncpy(p->squad,sql->GetField(row,3),SQUAD_NAME_LEN);
		strncpy(buf,sql->GetField(row,4),sizeof(buf));
		HexStringToByteArray(buf,sizeof(buf),p->Banner,BANNER_SIZE);
		
		pd->Lock();
		LLAddLast(&pd->playerlist,p);
		pd->Unlock();
		
		num++;
	}
	lm->Log(L_DRIVEL,"<data_sql> got %u player from database",num);
	
	cbp->pf(cbp->p,cbp->cid,TRUE); //inform billing module there is something to check against now
}

local void LoadPlayer(Player *p, int cid, LoadPlayerResponseFunc f)
{
	if(!p) return;
	
	cbpass *cbp=amalloc(sizeof(cbpass));
	cbp->p=p;
	cbp->cid=cid;
	cbp->pf=f;
	sql->Query(loadplayerresult,cbp,true,"SELECT UserID, Name, Password, Squad, Banner FROM Users WHERE Name=? LIMIT 1",p->name);
}

local void loadscoreresult(int status, db_res* res, void* vp)
{
	if(status)
	{
		lm->Log(L_WARN,"<data_sql> error getting score from database: %u",status);
		return;
	}
	
	cbpass *cbp=vp;
	
	int rows=sql->GetRowCount(res);
	int cols=sql->GetFieldCount(res);
	
	int num=0;
	int i=0;
	for(i=0; i < rows; i++)
	{
		db_row* row=sql->GetRow(res);
		
		Score *s=scd->NewScore();
		s->db_id=aatoi(sql->GetField(row,0));
		s->lastupdatetime=aatoi(sql->GetField(row,1));
		// int pid=aatoi(sql->GetField(row,2));
		// int sid=aatoi(sql->GetField(row,3));
		s->Score.Kills=aatoi(sql->GetField(row,4));
		s->Score.Deaths=aatoi(sql->GetField(row,5));
		s->Score.Flags=aatoi(sql->GetField(row,6));
		s->Score.Score=aatoi(sql->GetField(row,7));
		s->Score.FlagScore=aatoi(sql->GetField(row,8));
		
		// s->p=zd->FindZoneBySID(sid);
		// s->z=zd->FindZoneBySID(sid);
		s->p=cbp->sc->p;
		s->z=cbp->sc->z;
		s->lastupdatetime=current_ticks();
		
		scd->Lock();
		LLAddLast(&scd->scorelist,s);
		scd->Unlock();
		
		num++;
	}
	lm->Log(L_DRIVEL,"<data_sql> got %u score from database",num);
	
	cbp->scf(cbp->sc,TRUE); //inform module there is something to check against now
}

local void LoadScore(Score *s, LoadScoreResponseFunc f)
{
	if(!s) return;
	
	cbpass *cbp=amalloc(sizeof(cbpass));
	cbp->sc=s;
	cbp->scf=f;
	sql->Query(loadscoreresult,cbp,true,"SELECT ScoreID, Time, UserID, ScoresID, Kills, Deaths, Flags, Score, FlagScore FROM Scores WHERE UserID=# AND ScoresID=# LIMIT 1",s->p->db_id,s->z->ServerID);
}

local void loadsquadresult(int status, db_res* res, void* vp)
{
	if(status)
	{
		lm->Log(L_WARN,"<data_sql> error getting squad from database: %u",status);
		return;
	}
	
	cbpass *cbp=vp;
	
	int rows=sql->GetRowCount(res);
	int cols=sql->GetFieldCount(res);
	
	int num=0;
	int i=0;
	for(i=0; i < rows; i++)
	{
		db_row* row=sql->GetRow(res);
		
		Squad *s=sqd->NewSquad();
		s->db_id=aatoi(sql->GetField(row,0));
		strncpy(s->name,sql->GetField(row,1),SQUAD_NAME_LEN);
		strncpy(s->owner,sql->GetField(row,2),USER_NAME_LEN);
		s->created=aatoi(sql->GetField(row,3));
				
		sqd->Lock();
		LLAddLast(&sqd->squadlist,s);
		sqd->Unlock();
		
		num++;
	}
	lm->Log(L_DRIVEL,"<data_sql> got %u squad from database",num);
	
	cbp->sqf(cbp->sq,TRUE); //inform module there is something to check against now
}

local void LoadSquad(Squad *s, LoadSquadResponseFunc f)
{
	if(!s) return;
	
	cbpass *cbp=amalloc(sizeof(cbpass));
	cbp->sq=s;
	cbp->sqf=f;
	sql->Query(loadsquadresult,cbp,true,"SELECT SquadID, Name, Owner, Created FROM Squads WHERE Name=?  LIMIT 1",s->name);
}

local void loadchatresult(int status, db_res* res, void* vp)
{
	if(status)
	{
		lm->Log(L_WARN,"<data_sql> error getting chat from database: %u",status);
		return;
	}
	
	cbpass *cbp=vp;
	
	int rows=sql->GetRowCount(res);
	int cols=sql->GetFieldCount(res);
	
	int num=0;
	int i=0;
	for(i=0; i < rows; i++)
	{
		db_row* row=sql->GetRow(res);
		
		Chat *c=cd->NewChat();
		c->db_id=aatoi(sql->GetField(row,0));
		strncpy(c->name,sql->GetField(row,1),CHAT_NAME_LEN);
		strncpy(c->owner,sql->GetField(row,2),USER_NAME_LEN);
		c->created=aatoi(sql->GetField(row,3));
		
		cd->Lock();
		LLAddLast(&cd->chatlist,c);
		cd->Unlock();
		
		num++;
	}
	lm->Log(L_DRIVEL,"<data_sql> got %u chat from database",num);
	
	cbp->cf(cbp->c,TRUE); //inform module there is something to check against now
}

local void LoadChat(Chat *c, LoadChatResponseFunc f)
{
	if(!c) return;
	
	cbpass *cbp=amalloc(sizeof(cbpass));
	cbp->c=c;
	cbp->cf=f;
	sql->Query(loadchatresult,cbp,true,"SELECT ChatID, Name, Owner, Created FROM Chats WHERE Name=? LIMIT 1",c->name);
}

local void loadbanresult(int status, db_res* res, void* vp)
{
	if(status)
	{
		lm->Log(L_WARN,"<data_sql> error getting ban from database: %u",status);
		return;
	}
	
	cbpass *cbp=vp;
	
	int rows=sql->GetRowCount(res);
	int cols=sql->GetFieldCount(res);
	
	int num=0;
	int i=0;
	for(i=0; i < rows; i++)
	{
		db_row* row=sql->GetRow(res);
		
		Ban *b=bd->NewBan();
		b->db_id=aatoi(sql->GetField(row,0));
		b->active=aatoi(sql->GetField(row,1));
		strncpy(b->reason,sql->GetField(row,2),250);
		b->created=aatoi(sql->GetField(row,3));
		b->duration=aatoi(sql->GetField(row,4));
		strncpy(b->name,sql->GetField(row,5),USER_NAME_LEN);
		strncpy(b->creator,sql->GetField(row,6),USER_NAME_LEN);
		b->IPAddress=aatoi(sql->GetField(row,7));
		b->MachineID=aatoi(sql->GetField(row,8));
		b->IPStart=aatoi(sql->GetField(row,9));
		b->IPEnd=aatoi(sql->GetField(row,10));
		bd->Lock();
		LLAddLast(&bd->banlist,b);
		bd->Unlock();
		
		num++;
	}
	lm->Log(L_DRIVEL,"<data_sql> got %u ban from database",num);
	
	cbp->bf(cbp->b,TRUE); //inform module there is something to check against now
}

local void LoadBan(Ban *b, LoadBanResponseFunc f)
{
	if(!b) return;
	
	cbpass *cbp=amalloc(sizeof(cbpass));
	cbp->b=b;
	cbp->bf=f;
	sql->Query(loadbanresult,cbp,true,"SELECT BanID, Active, Reason, Created, Ends, Banee, Issuer, IP, MachineID, IPStart, IPEnd FROM Bans WHERE Banee=? LIMIT 1",b->name);
}

local void dontcare(int status, db_res* res, void* vp)
{
	//herp a derp
}

local int autosave(void* vp)
{
	int num=0;
	int num2=0;
	int num3=0;
	int num4=0;
	int num5=0;
	int num6=0;
	
	Link *link;
	
	Zone *z;
	zd->Lock();
	FOR_EACH_ZONE(z)
	{
		sql->Query(dontcare,NULL,true,"UPDATE Zones SET Name=?, ScoreID=#, GroupID=#, LastSeen=NOW() WHERE ServerID=# AND Password=?",
			z->name,z->ScoreID,z->GroupID,z->ServerID,z->password);
		num++;
	}
	zd->Unlock();
	
	Player *p;
	pd->Lock();
	FOR_EACH_PLAYER(p)
	{
		char buf[200];
		ByteArrayToHexString(buf,sizeof(buf),p->Banner,BANNER_SIZE);
		sql->Query(dontcare,NULL,true,"UPDATE Users SET Squad=?, IP=#, MachineID=#, Timezone=#, LastSeen=NOW(), Banner=? WHERE Name=? AND Password=?",
			p->squad,p->IPAddress,p->MachineID,p->Timezone,buf,p->name,p->password);
		num2++;
	}
	pd->Unlock();
	
	Score *sc;
	scd->Lock();
	FOR_EACH_SCORE(sc)
	{
		sql->Query(dontcare,NULL,true,"UPDATE Scores SET Kills=?, Deaths=#, Flags=#, Score=#, FlagScore=# WHERE UserID=# AND ScoresID=#",
			sc->Score.Kills,sc->Score.Deaths,sc->Score.Flags,sc->Score.Score,sc->Score.FlagScore,
			sc->p->db_id,sc->z->ServerID);
		num3++;
	}
	scd->Unlock();
	
	Squad *sq;
	sqd->Lock();
	FOR_EACH_SQUAD(sq)
	{
		sql->Query(dontcare,NULL,true,"UPDATE Squads SET Owner=? WHERE Name=#",sq->owner,sq->name);
		num4++;
	}
	sqd->Unlock();
	
	Chat *c;
	cd->Lock();
	FOR_EACH_CHAT(c)
	{
		sql->Query(dontcare,NULL,true,"UPDATE Chats SET Owner=? WHERE Name=#",c->owner,c->name);
		num5++;
	}
	cd->Unlock();
	
	Ban *b;
	bd->Lock();
	FOR_EACH_BAN(b)
	{
		sql->Query(dontcare,NULL,true,"UPDATE Bans SET Active=#, Ends=#, IP=#, MachineID=#, IPStart=#, IPEnd=#, Banee=?, Issuer=?, Reason=? WHERE BanID=#",
			b->active,b->duration,b->IPAddress,b->MachineID,
			b->IPStart,b->IPEnd,b->name,b->creator,b->reason,b->db_id);
		num6++;
	}
	bd->Unlock();
	
	lm->Log(L_INFO,"<data_sql> autosave: %u zones, %u players, and %u scores to database",num,num2,num3);
	lm->Log(L_INFO,"<data_sql> autosave: %u squads, %u chats, and %u bans to database",num4,num5,num6);
	
	return true;
}

local void zoneaction(Zone *z, ZoneAction action)
{
	if(!z) return;
	
	if(action == ZA_CREATE)
	{
		sql->Query(dontcare,NULL,true,"INSERT INTO Zones SET Name=?, Password=?, ServerID=#, ScoreID=#, GroupID=#, LastSeen=NOW()",
			z->name,z->password,z->ServerID,z->ScoreID,z->GroupID);
		lm->Log(L_DRIVEL,"<data_sql> created zone: (%s) (%u)",z->name,z->ServerID);
	}
}

local void playeraction(Player *p, PlayerAction action)
{
	if(!p) return;
	
	if(action == PA_CREATE)
	{
		sql->Query(dontcare,NULL,true,"INSERT INTO Users SET Name=?, Password=?, Squad=?, IP=#, MachineID=#, Timezone=#, LastSeen=NOW()",
			p->name,p->password,p->squad,p->IPAddress,p->MachineID,p->Timezone);
		lm->Log(L_DRIVEL,"<data_sql> created player: (%s)",p->name);
	}
}

local void scoreaction(Score *s, ScoreAction action)
{
	if(!s) return;
	
	if(action == SCA_CREATE)
	{
		sql->Query(dontcare,NULL,true,"INSERT INTO Scores SET UserID=#, ScoresID=#, Time=NOW()",
			s->p->db_id,s->z->ServerID);
		lm->Log(L_DRIVEL,"<data_sql> created score: (%s:%u)",s->p->name,s->z->ServerID);
	}
}

local void squadaction(Squad *s, SquadAction action)
{
	if(!s) return;
	
	if(action == SQA_CREATE)
	{
		sql->Query(dontcare,NULL,true,"INSERT INTO Squads SET Name=?, Password=?, Owner=?",
			s->name,s->password,s->name);
		lm->Log(L_DRIVEL,"<data_sql> created squad: (%s:%s)",s->name,s->name);
	}
}

local void chataction(Chat *c, ChatAction action)
{
	if(!c) return;
	
	if(action == CA_CREATE)
	{
		sql->Query(dontcare,NULL,true,"INSERT INTO Chats SET Name=?, Password=?, Owner=?",
			c->name,c->password,c->owner);
		lm->Log(L_DRIVEL,"<data_sql> created chat: (%s)",c->name);
	}
}

local void banaction(Ban *b, BanAction action)
{
	if(!b) return;
	
	if(action == BA_CREATE)
	{
		sql->Query(dontcare,NULL,true,"INSERT INTO Bans SET Active=#, Reason=?, Banee=?, Issuer=?, IP=#, MachineID=#, IPStart=#, IPEnd=#",
			b->active,b->reason,b->name,b->creator,b->IPAddress,b->MachineID,b->IPStart,b->IPEnd);
		lm->Log(L_DRIVEL,"<data_sql> created ban: (%s:%s:%s)",b->creator,b->name,b->reason);
	}
}

EXPORT char* info_data_sql=
"SQL Data Module\n"
"by Cheese\n"
"Allows modules to access a SQL database.";

EXPORT ModReturn MM_data_sql(Imodman *mm2, ModAction action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		ml=mm->GetInterface(I_MAINLOOP);
		cfg=mm->GetInterface(I_CONFIG);
		sql=mm->GetInterface(I_SQL);
		zd=mm->GetInterface(I_ZONES);
		pd=mm->GetInterface(I_PLAYERS);
		scd=mm->GetInterface(I_SCORES);
		sqd=mm->GetInterface(I_SQUADS);
		cd=mm->GetInterface(I_CHATS);
		bd=mm->GetInterface(I_BANS);
		if(!lm || !ml || !cfg || !sql || !zd || !pd
			|| !scd || !sqd || !cd || !bd) return MM_FAIL;
		
		mm->RegCallback(CB_ZONEACTION,zoneaction);
		mm->RegCallback(CB_LOADZONE,LoadZone);
		mm->RegCallback(CB_PLAYERACTION,playeraction);
		mm->RegCallback(CB_LOADPLAYER,LoadPlayer);
		mm->RegCallback(CB_SCOREACTION,scoreaction);
		mm->RegCallback(CB_LOADSCORE,LoadScore);
		mm->RegCallback(CB_SQUADACTION,scoreaction);
		mm->RegCallback(CB_LOADSQUAD,LoadScore);
		mm->RegCallback(CB_CHATACTION,chataction);
		mm->RegCallback(CB_LOADCHAT,LoadScore);
		mm->RegCallback(CB_BANACTION,scoreaction);
		mm->RegCallback(CB_LOADBAN,LoadScore);
		
		int savedelaymins=cfg->GetInt(GLOBAL,"SQL","WriteDelay",60);
		int savedelay=100*60*savedelaymins;
		ml->SetTimer(autosave,savedelay,savedelay,NULL,NULL);
		
		//ultimately a bad idea
		// sql->Query(initzones,NULL,true,"SELECT ZoneID, Name, Password, ServerID FROM Zones");
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		ml->ClearTimer(autosave,NULL);
		
		mm->UnregCallback(CB_ZONEACTION,zoneaction);
		mm->UnregCallback(CB_LOADZONE,LoadZone);
		mm->UnregCallback(CB_PLAYERACTION,playeraction);
		mm->UnregCallback(CB_LOADPLAYER,LoadPlayer);
		mm->UnregCallback(CB_SCOREACTION,scoreaction);
		mm->UnregCallback(CB_LOADSCORE,LoadScore);
		mm->UnregCallback(CB_SCOREACTION,scoreaction);
		mm->UnregCallback(CB_LOADSCORE,LoadScore);
		mm->UnregCallback(CB_SCOREACTION,scoreaction);
		mm->UnregCallback(CB_LOADSCORE,LoadScore);
		mm->UnregCallback(CB_SCOREACTION,scoreaction);
		mm->UnregCallback(CB_LOADSCORE,LoadScore);
		
		mm->ReleaseInterface(bd);
		mm->ReleaseInterface(cd);
		mm->ReleaseInterface(sqd);
		mm->ReleaseInterface(scd);
		mm->ReleaseInterface(pd);
		mm->ReleaseInterface(zd);
		mm->ReleaseInterface(cfg);
		mm->ReleaseInterface(sql);
		mm->ReleaseInterface(ml);
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}

