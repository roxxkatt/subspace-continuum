#ifndef __TEXT_H
#define __TEXT_H

//because things are fucky
#undef CC_NONE

//color text will send a S2C_CHAT disguised as B2S_USER_PACKET
#define S2C_CHAT 0x07
typedef struct ChatPacket
{
	u8 type; //S2C_CHAT
	u8 kind; //ColorCode
	u8 sound; //will probably work
	i16 pid; //0
	char text[0];
} ChatPacket;

// #define ALLZONES 0

typedef enum
{
	CC_NONE		=0,
	CC_BLUE		=2,
	CC_YELLOW	=3,
	CC_GREEN	=5,
	CC_RED		=8,
	CC_ORANGE	=9,
	CC_GREY		=33,
	CC_PINK		=79
} ColorCode;

#define I_TEXT "text-1"

typedef struct Itext
{
	INTERFACE_HEAD_DECL;

	//cmd response
	void (*SendCommandResponse)(Player* p, const char *format, ...);
	//priv
	void (*SendPrivate)(Player* p, int sound, const char *format, ...);
	//chat
	void (*SendChat)(Player* p, int chnum, const char *format, ...);
	
	//send colored message to a player
	void (*SendColorMsgP)(Player* p, ColorCode color, const char *format, ...);
	//send colored message to the target zone
	void (*SendColorMsgZ)(Zone* z, ColorCode color, const char *format, ...);
	
	//broadcast
	void (*SendBroadcast)(Zone* z, int sound, const char *format, ...);
	
} Itext;

#endif
