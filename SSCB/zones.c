
#include "ssc.h"

#define ID_REUSE_DELAY 10

local Imodman *mm;
local int dummykey;
local int magickey;

local pthread_mutex_t lockmtx;
#define LOCK() pthread_mutex_lock(&lockmtx)
#define UNLOCK() pthread_mutex_unlock(&lockmtx)

local struct
{
	Zone *z;
	int next;
	time_t available;
} *idmap;

local int firstfreeid;
local int idmapsize;
local int perunitspace;
local pthread_mutexattr_t recmtxattr;

local LinkedList blocks;
local pthread_mutex_t blockmtx;
struct block
{
	int start;
	int len;
};

local Izones zdint;
#define zd (&zdint)

local void Lock(void)
{
	LOCK();
}

local void Unlock(void)
{
	UNLOCK();
}

local Zone* alloc_zone(void)
{
	Zone *z=amalloc(sizeof(*z)+perunitspace);
	*(unsigned*)GETDATA(z,magickey)=MODMAN_MAGIC;
	return z;
}

local Zone* NewZone(void)
{
	LOCK();
	time_t now=time(NULL);

	//find a free id that's available
	int *ptr=&firstfreeid;
	int id=firstfreeid;
	while(id != -1 && idmap[id].available > now)
	{
		ptr=&idmap[id].next;
		id=idmap[id].next;
	}

	if(id == -1)
	{
		//no available ids
		int newsize=idmapsize*2;
		idmap=arealloc(idmap,newsize* sizeof(idmap[0]));
		for (id=idmapsize; id < newsize; id++)
		{
			idmap[id].z=NULL;
			idmap[id].next=id + 1;
			idmap[id].available=0;
		}
		idmap[newsize-1].next=firstfreeid;
		firstfreeid=idmapsize;
		ptr=&firstfreeid;
		id=firstfreeid;
		idmapsize=newsize;
	}

	*ptr=idmap[id].next;

	Zone *z=idmap[id].z;
	if(!z) z=idmap[id].z=alloc_zone();

	z->id=id;
	z->connecttime=current_ticks();
	LLInit(&(z->connections));
	//do not add to zone list until after auth
	UNLOCK();

	DO_CBS(CB_ZONEPOINTER,ZonePointerFunc,(z,TRUE));

	return z;
}

local void FreeZone(Zone *z)
{
	DO_CBS(CB_ZONEPOINTER,ZonePointerFunc,(z,FALSE));

	LOCK();
	LLRemove(&zd->zonelist,z);
	idmap[z->id].z=NULL;
	idmap[z->id].available=time(NULL)+ID_REUSE_DELAY;
	idmap[z->id].next=firstfreeid;
	firstfreeid=z->id;
	UNLOCK();

	afree(z);
}

local Size AllocateZoneData(Size bytes)
{
	struct block *b, *nb;
	int current=0;

	//round up to next multiple of word size
	bytes=(bytes+(sizeof(int)-1)) & (~(sizeof(int)-1));

	pthread_mutex_lock(&blockmtx);

	//first try before between two blocks (or at the beginning)
	Link *link;
	Link *last=NULL;
	for(link=LLGetHead(&blocks); link; link=link->next)
	{
		b=link->data;
		if((b->start - current) >= (int)bytes) goto found;
		else current=b->start + b->len;
		last=link;
	}

	//if we couldn't get in between two blocks,try at the end
	if((perunitspace-current) >= (int)bytes) goto found;

	//no more space.
	pthread_mutex_unlock(&blockmtx);
	return -1;

found:
	nb=amalloc(sizeof(*nb));
	nb->start=current;
	nb->len=bytes;
	//if last == NULL,this will put it in front of the list
	LLInsertAfter(&blocks,last,nb);

	//clear all newly allocated space
	Zone *z;
	void *data;
	LOCK();
	FOR_EACH_ZONE_DATA(z,data,current) memset(data,0,bytes);
	UNLOCK();

	pthread_mutex_unlock(&blockmtx);

	return current;
}

local void FreeZoneData(int key)
{
	Link *l;
	pthread_mutex_lock(&blockmtx);
	for(l=LLGetHead(&blocks); l; l=l->next)
	{
		struct block *b=l->data;
		if(b->start == key)
		{
			LLRemove(&blocks,b);
			afree(b);
			break;
		}
	}
	pthread_mutex_unlock(&blockmtx);
}

local Zone* FindZoneByID(int id)
{
	LOCK();
	if(id >= 0 && id < idmapsize)
	{
		Zone *z=idmap[id].z;
		UNLOCK();
		return z;
	}
	UNLOCK();
	return NULL;
}

local Zone* FindZoneByName(char *name)
{
	Link *link;
	Zone *z;

	LOCK();
	
	FOR_EACH_ZONE(z)
		if(strcasecmp(name,z->name) == 0)
		{
			UNLOCK();
			return z;
		}
		
//	FOR_EACH_UNAUTHED_ZONE(z)
//		if(strcasecmp(name,z->name) == 0)
//		{
//			UNLOCK();
//			return z;
//		}
		
	UNLOCK();
	
	return NULL;
}

local Zone* FindZoneBySID(int sid)
{
	Link *link;
	Zone *z;

	LOCK();
	
	FOR_EACH_ZONE(z)
		if(z->ServerID == sid)
		{
			UNLOCK();
			return z;
		}
		
//	FOR_EACH_UNAUTHED_ZONE(z)
//		if(z->ServerID == sid)
//		{
//			UNLOCK();
//			return z;
//		}
		
	UNLOCK();
	
	return NULL;
}

local Zone* FindZoneByWI(WebInfo* wi)
{
	Zone *z;
	Link *link;
	LOCK();
	FOR_EACH_ZONE(z)
	{
// printf("<zones> comparing (%lu,%hu) to (%lu,%hu)\n",
// (long)wi->sin_addr.S_un.S_addr,wi->sin_port,
// (long)z->wi.sin_addr.S_un.S_addr,z->wi.sin_port);

#if PLATFORM == PLATFORM_WINDOWS
//looks like microsoft rewrote the struct to make it more powerful
		if(((long)z->wi.sin_addr.S_un.S_addr == (long)wi->sin_addr.S_un.S_addr)
			 && (z->wi.sin_port == wi->sin_port))
#else
		if(((long)z->wi.sin_addr.s_addr == (long)wi->sin_addr.s_addr)
			 && (z->wi.sin_port == wi->sin_port))
#endif
		{
			UNLOCK();
			return z;
		}
	}
	UNLOCK();
	
	return NULL;
}

local void KickZone(Zone *z)
{
	Lock();
	Unlock();
}

local Izones zdint=
{
	INTERFACE_HEAD_INIT(I_ZONES,"zones"),
	NewZone,FreeZone,
	AllocateZoneData,FreeZoneData,
	Lock,Unlock,
	FindZoneByID,FindZoneByName,FindZoneBySID,FindZoneByWI,
	KickZone
};

EXPORT char* info_zones=
"Zones Module\n"
"by Cheese\n"
"Allows modules to access zone data.";

EXPORT ModReturn MM_zones(Imodman *mm2, ModAction action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;

		pthread_mutexattr_init(&recmtxattr);
		pthread_mutexattr_settype(&recmtxattr,PTHREAD_MUTEX_RECURSIVE);
		pthread_mutexattr_settype(&recmtxattr,PTHREAD_MUTEX_RECURSIVE);
		pthread_mutex_init(&lockmtx,&recmtxattr);
		pthread_mutex_init(&blockmtx,NULL);

		idmapsize=256;
		idmap=amalloc(idmapsize*sizeof(idmap[0]));
		int i;
		for(i=0; i < idmapsize; i++)
		{
			idmap[i].z=NULL;
			idmap[i].next=i + 1;
			idmap[i].available=0;
		}
		idmap[idmapsize-1].next=-1;
		firstfreeid=0;

		LLInit(&zd->zonelist);
		LLInit(&blocks);

		Iconfig *cfg=mm->GetInterface(I_CONFIG);
		perunitspace=cfg ? cfg->GetInt(GLOBAL,"General","PerZoneBytes",4000) : 4000;
		mm->ReleaseInterface(cfg);

		dummykey=AllocateZoneData(sizeof(unsigned));
		magickey=AllocateZoneData(sizeof(unsigned));

		mm->RegInterface(&zdint);

		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&zdint)) return MM_FAIL;

		pthread_mutexattr_destroy(&recmtxattr);

		FreeZoneData(dummykey);
		FreeZoneData(magickey);

		afree(idmap);
		LLEnum(&zd->zonelist,afree);
		LLEmpty(&zd->zonelist);
		
		return MM_OK;
	}
	
	return MM_FAIL;
}
